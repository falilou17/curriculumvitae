<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProverbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proverbs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('portfolio');
            $table->text('competence');
            $table->text('strength');
            $table->text('resume');
            $table->text('reference');
            $table->text('skill');
            $table->text('knowledge');
            $table->text('award');
            $table->text('achievement');
            $table->text('workProcess');
            $table->text('contact');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proverbs');
    }
}
