<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWallpapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallpapers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('strength', 255)->default('strength-wallpaper.jpg');
            $table->string('reference', 255)->default('reference-wallpaper.jpg');
            $table->string('achievement', 255)->default('achievement-wallpaper.jpg');
            $table->string('portfolio', 255)->default('portfolio-wallpaper.jpg');
            $table->string('contact', 255)->default('contact-wallpaper.jpg');
            $table->string('service', 255)->default('service-wallpaper.jpg');
            $table->string('profile', 255)->default('profile-wallpaper.jpg');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallpapers');
    }
}
