<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastName');
            $table->string('firstName');
            $table->string('email', 255);
            $table->text('descBrief');
            $table->text('descLong');
            $table->text('coverDesc');
            $table->string('profilePicture', 255);
            $table->string('coverPicture', 255);
            $table->text('job');
            $table->string('cv', 255);
            $table->text('address');
            $table->text('webSite');
            $table->string('phone');
            $table->boolean('hideContact')->default(false);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('template_id')->unsigned()->default('1');
            $table->foreign('template_id')->references('id')->on('templates');
            $table->text('siteTitle')->nullable();
            $table->text('siteDescription')->nullable();
            $table->text('siteKeywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
