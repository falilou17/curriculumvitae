<?php

use Illuminate\Database\Seeder;

class WallpaperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wallpapers')->insert([
            'strength'           => 'strength-wallpaper.jpg',
            'achievement'          => 'achievement-wallpaper.jpg',
            'reference'             => 'reference-wallpaper.jpg',
            'portfolio'             => 'portfolio-wallpaper.jpg',
            'contact'             => 'contact-wallpaper.jpg',
            'service'             => 'service-wallpaper.jpg',
            'profile'             => 'profile-wallpaper.jpg',
            'user_id'           => '1'
        ]);

        DB::table('wallpapers')->insert([
            'strength'           => 'strength-wallpaper.jpg',
            'achievement'          => 'achievement-wallpaper.jpg',
            'reference'             => 'reference-wallpaper.jpg',
            'portfolio'             => 'portfolio-wallpaper.jpg',
            'contact'             => 'contact-wallpaper.jpg',
            'service'             => 'service-wallpaper.jpg',
            'profile'             => 'profile-wallpaper.jpg',
            'user_id'           => '2'
        ]);

        $fas = ['fa-compass','fa-expand','fa-eur','fa-gbp','fa-usd'];

        foreach ($fas as $k => $fa)
            DB::table('competences')->insert([
                'id' => $k+1,
                'name' => $fa,
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'icon' => $fa,
                'status' => true,
                'user_id' => 2
            ]);
    }
}
