<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(TemplateSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(ProverbSeeder::class);
        $this->call(WallpaperSeeder::class);
//        $this->call(CompetenceSeeder::class);

        Model::reguard();
    }
}
