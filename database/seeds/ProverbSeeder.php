<?php

use Illuminate\Database\Seeder;

class ProverbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proverbs')->insert([
            'portfolio'           => 'I\'m looking to expand my portfolio while I\'m on top and while I\'m young.',
            'competence'          => 'I love what I do. I take great pride in what I do. And I can\'t do something halfway, three-quarters, nine-tenths. If I\'m going to do something, I go all the way.',
            'strength'            => 'Failure will never overtake me if my determination to succeed is strong enough.',
            'resume'              => 'If you call failures experiments, you can put them in your resume and claim them as achievements.',
            'reference'           => 'Testimonials are enough to convince people for now.',
            'skill'               => 'A winner is someone who recognizes his God-given talents, works his tail off to develop them into skills, and uses these skills to accomplish his goals.',
            'knowledge'           => 'Human behavior flows from three main sources: desire, emotion, and knowledge.',
            'award'               => 'The accolades, just like the scrapes and bruises, fade in the end, and all you\'re left with is your ambition.',
            'achievement'         => 'My path has not been determined. I shall have more experiences and pass many more milestones.',
            'workProcess'         => 'As a human being, I\'m work in process.',
            'contact'             => 'Have a project you\'d like to discuss?',
            'user_id'           => '1'
        ]);

        DB::table('proverbs')->insert([
            'portfolio'           => 'I\'m looking to expand my portfolio while I\'m on top and while I\'m young.',
            'competence'          => 'I love what I do. I take great pride in what I do. And I can\'t do something halfway, three-quarters, nine-tenths. If I\'m going to do something, I go all the way.',
            'strength'            => 'Failure will never overtake me if my determination to succeed is strong enough.',
            'resume'              => 'If you call failures experiments, you can put them in your resume and claim them as achievements.',
            'reference'           => 'Testimonials are enough to convince people for now.',
            'skill'               => 'A winner is someone who recognizes his God-given talents, works his tail off to develop them into skills, and uses these skills to accomplish his goals.',
            'knowledge'           => 'Human behavior flows from three main sources: desire, emotion, and knowledge.',
            'award'               => 'The accolades, just like the scrapes and bruises, fade in the end, and all you\'re left with is your ambition.',
            'achievement'         => 'My path has not been determined. I shall have more experiences and pass many more milestones.',
            'workProcess'         => 'As a human being, I\'m work in process.',
            'contact'             => 'Have a project you\'d like to discuss?',
            'user_id'           => '2'
        ]);
    }
}
