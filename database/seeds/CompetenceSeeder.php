<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompetenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fas = ['fa-compass','fa-collapse','fa-collapse-top','fa-expand','fa-eur','fa-euro (alias)','fa-gbp','fa-usd'];
        $faker = new Faker\Generator();
        foreach ($fas as $k => $fa)
            DB::table('competences')->insert([
                'id' => $k+1,
                'name' => $faker->title,
                'description' => $faker->sentence,
                'icon' => $fa,
                'active' => true
            ]);
    }
}
