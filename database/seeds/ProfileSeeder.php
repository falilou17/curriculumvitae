<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'profilePicture'     => 'profilePicture.jpg',
            'coverPicture'       => 'coverPicture.jpg',
            'user_id'            => '1',
            'template_id'        => '1',
            'siteTitle'          => 'Currucullum Vitae',
            'siteDescription'    => 'Currucullum Vitae',
            'siteKeywords'       => 'Currucullum Vitae',
            'hideContact'        => false
        ]);

        DB::table('profiles')->insert([
            'firstName'           => 'Global Network Consulting & Services',
            'lastName'           => 'GNCS',
            'email'              => 'gncs.sarl@gncs-sarl.com',
            'phone'              => '+221 77 590 48 28 / +221 76 419 04 65',
            'address'           => '12 Ouest Foire<br> Cité LONASE<br> Dakar Sénégal',
            'profilePicture'     => 'profilePicture.jpg',
            'coverPicture'       => 'coverPicture.jpg',
            'user_id'            => '2',
            'template_id'        => '4',
            'siteTitle'          => 'Currucullum Vitae',
            'siteDescription'    => 'Currucullum Vitae',
            'siteKeywords'       => 'Currucullum Vitae',
            'hideContact'        => false,
            'coverDesc'          => 'cover Desc',
            'descBrief'          => 'descBrief',
            'descLong'          => 'desc Long',
        ]);
    }
}
