<?php

use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('templates')->insert([
            'id' => 1,
            'title' => 'cv',
            'name' => 'template1',
            'sidebar' => '*',
            'active' => true
        ]);

        DB::table('templates')->insert([
            'id' => 2,
            'title' => 'dev',
            'name' => 'template2',
            'sidebar' => 'portfolio|service|social|achievement|strength|reference|wallpaper|design',
            'active' => true
        ]);

        DB::table('templates')->insert([
            'id' => 3,
            'title' => 'colorlib',
            'name' => 'templateColorLib',
            'sidebar' => 'portfolio|service|social|skill|reference|wallpaper|design',
            'active' => true
        ]);

        DB::table('templates')->insert([
            'id' => 4,
            'title' => 'company',
            'name' => 'templateCompany',
            'sidebar' => 'portfolio|service|social|reference|design',
            'active' => true
        ]);
    }
}

//award|experience|knowledge|portfolio|school|service|skill|social|knowledge|achievement|strength|reference|wallpaper|design
