<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designs')->insert([
            'background'        => '#0d0c0d',
            'backgroundDiv'     => '#171717',
            'faBackground'      => '#2e2e2e',
            'title'             => '#ffffff',
            'description'       => '#a1a1a1',
            'element'           => '#c80a48',
            'status'            => '1',
            'user_id'           => '1'
        ]);

        DB::table('designs')->insert([
            'background'        => '#0d0c0d',
            'backgroundDiv'     => '#171717',
            'faBackground'      => '#2e2e2e',
            'title'             => '#ffffff',
            'description'       => '#a1a1a1',
            'element'           => '#c80a48',
            'status'            => '1',
            'user_id'           => '2'
        ]);
    }
}
