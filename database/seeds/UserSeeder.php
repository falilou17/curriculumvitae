<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'login'           => 'administrator',
            'password'        => Hash::make('administrator'),
            'isAdmin'         => 1
        ]);

        DB::table('users')->insert([
            'login'           => 'falilou17',
            'password'        => Hash::make('administrator'),
            'createdBy'       => 1,
            'isAdmin'         => 0
        ]);
    }
}
