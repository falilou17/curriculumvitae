<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 03/02/2016
 * Time: 14:44
 */
return [
    'back-pagination' => 20,
    'front-pagination' => 20,
    'defaultMail' => 'noreply',
    'picture-width' => '430',
    'picture-height' => null,
    'cover-width' => '1300',
    'picture-portfolio-width' => '800',
    'picture-reference-width' => '200',
    'icon-width-achievement' => '60',
    'icon-height-achievement' => '60',
    'icon-width-award' => '60',
    'icon-height-award' => '60',
    'icon-width-competence' => '40',
    'icon-height-competence' => '40',
];