<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'socials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'link', 'icon', 'user_id'];
}
