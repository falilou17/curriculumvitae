<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'experiences';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'profession', 'dateStart', 'dateEnd', 'user_id'];
}
