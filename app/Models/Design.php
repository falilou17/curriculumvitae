<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Design extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'designs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['background', 'backgroundDiv', 'title', 'description', 'element', 'faBackground', 'user_id'];
}
