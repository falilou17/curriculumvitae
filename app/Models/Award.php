<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Award extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'awards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'icon', 'picture', 'user_id'];
}
