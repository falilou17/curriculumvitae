<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'skills';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'pourcentage', 'user_id'];
}
