<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Achievement extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'achievements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'number', 'icon', 'picture', 'user_id'];
}
