<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proverb extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proverbs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['portfolio', 'competence', 'strength', 'resume', 'reference', 'skill', 'knowledge', 'award', 'achievement', 'workProcess', 'contact', 'user_id'];
}
