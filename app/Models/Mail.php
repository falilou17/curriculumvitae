<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['object', 'sender', 'receiver', 'content', 'read', 'status', 'user_id'];
}
