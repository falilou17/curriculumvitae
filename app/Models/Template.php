<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'name', 'active', 'sidebar'];

    function showMenu($menu){
        if($this->sidebar == '*' )
            return true;
        else {
            $arrMenu = explode('|', $this->sidebar);
            return in_array($menu, $arrMenu);
        }
    }
}
