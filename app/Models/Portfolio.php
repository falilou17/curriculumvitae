<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'technologies', 'client', 'testimonial', 'datePublish', 'user_id'];

    public function medias()
    {
        return $this->hasMany('App\Models\Media')->orderBy('position');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
