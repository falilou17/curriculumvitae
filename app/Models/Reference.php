<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reference extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'references';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['enterprise', 'description', 'fullName', 'profession', 'picture', 'user_id'];
}
