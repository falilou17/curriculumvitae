<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Competence extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competences';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'icon', 'picture', 'user_id'];
}
