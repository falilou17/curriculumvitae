<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Strength extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'strengths';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'pourcentage', 'user_id'];
}
