<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schools';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'diploma', 'dateStart', 'dateEnd', 'user_id'];
}
