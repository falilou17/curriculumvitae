<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 04/01/2016
 * Time: 20:08
 */

namespace app\Models;


use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}