<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends ItemModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lastName', 'firstName', 'email', 'descBrief', 'descLong', 'coverDesc', 'profilePicture', 'coverPicture', 'job', 'cv', 'address', 'webSite', 'phone', 'user_id', 'template_id', 'siteTitle', 'siteDescription', 'siteKeywords', 'hideContact'];

    function template(){
        return $this->hasOne('App\Models\Template', 'id', 'template_id');
    }

}
