<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['login', 'password', 'domain'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function achievements()
    {
        return $this->hasMany('App\Models\Achievement');
    }

    public function awards()
    {
        return $this->hasMany('App\Models\Award');
    }

    public function competences()
    {
        return $this->hasMany('App\Models\Competence');
    }

    public function designs()
    {
        return $this->hasMany('App\Models\Design');
    }

    public function experiences()
    {
        return $this->hasMany('App\Models\Experience');
    }

    public function knowledges()
    {
        return $this->hasMany('App\Models\Knowledge');
    }

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function proverb()
    {
        return $this->hasMany('App\Models\Proverb');
    }

    public function references()
    {
        return $this->hasMany('App\Models\Reference');
    }

    public function schools()
    {
        return $this->hasMany('App\Models\School');
    }

    public function skills()
    {
        return $this->hasMany('App\Models\Skill');
    }

    public function socials()
    {
        return $this->hasMany('App\Models\Social');
    }

    public function strengths()
    {
        return $this->hasMany('App\Models\Strength');
    }

    public function wallpaper()
    {
        return $this->hasOne('App\Models\Wallpaper');
    }
}
