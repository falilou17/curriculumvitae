<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallpaper extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wallpapers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['strength', 'reference', 'achievement', 'user_id'];
}
