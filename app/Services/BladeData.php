<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 04/01/2016
 * Time: 19:32
 */

namespace app\Services;


use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class BladeData
{
    function getAuthUser(){
        return Auth::user()->profile()->first();
    }

    function getAuthUserDomain(){
        return Auth::user()->domain;
    }
}