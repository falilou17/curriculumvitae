<?php

namespace App\Http\Controllers;

use App\Models\Achievement;
use App\Models\Award;
use App\Models\Competence;
use App\Models\Design;
use App\Models\Experience;
use App\Models\Knowledge;
use App\Models\Media;
use App\Models\Portfolio;
use App\Models\Profile;
use App\Models\Proverb;
use App\Models\Reference;
use App\Models\School;
use App\Models\Skill;
use App\Models\Social;
use App\Models\Strength;
use App\Models\User;
use App\Models\Wallpaper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Symfony\Component\VarDumper\VarDumper;

class indexController extends Controller
{
    /**
     * Display welcome page.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     * Display index page.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('isActive', '=', 1)->where('id','!=',1)->first();
        if(empty($user) || !$user->isActive)
            $user = User::where('isAdmin','=',1)->first();
        $id = $user['id'];

        $lang = $request->get('lang') ?? 'fr';
        App::setLocale($lang);

        $references = Reference::where('status','=','1')->where('user_id','=',$id)->get();
        $competences = Competence::where('status','=','1')->where('user_id','=',$id)->get();
        $achievements = Achievement::where('status','=','1')->where('user_id','=',$id)->limit(8)->get();
        $strengths = Strength::where('status','=','1')->where('user_id','=',$id)->limit(5)->get();
        $knowledges = Knowledge::where('status','=','1')->where('user_id','=',$id)->get();
        $awards = Award::where('status','=','1')->where('user_id','=',$id)->get();
        $skills = Skill::where('status','=','1')->where('user_id','=',$id)->get();
        $socials = Social::where('status','=','1')->where('user_id','=',$id)->get();
        $profile = Profile::where('user_id','=',$id)->first();
        $proverbs = Proverb::where('user_id','=',$id)->first();
        $designs = Design::where('status','=','1')->where('user_id','=',$id)->first();
        $portfolios = Portfolio::where('status','=','1')->where('user_id','=',$id)->get();
        $schools = School::where('status','=','1')->where('user_id','=',$id)->orderBy('dateStart', 'Desc')->get();
        $experiences = Experience::where('status','=','1')->where('user_id','=',$id)->orderBy('dateStart', 'Desc')->get();
        $wallpaper = Wallpaper::where('user_id','=',$id)->first();

        return view('front.'.$profile->template->name.'.pages.index', compact('designs', 'skills', 'proverbs', 'awards', 'profile', 'schools', 'experiences', 'strengths', 'knowledges', 'achievements', 'competences', 'references', 'portfolios', 'socials','wallpaper', 'lang'));
    }

    /**
     * Display a specific portfolio page.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function portfolio(Request $request, $id)
    {
        $user = User::where('isActive', '=', 1)->where('id','!=',1)->first();
        $profile = Profile::where('user_id','=',$user->id)->first();
        if(empty($user) || !$user->isActive)
            $user = User::where('isAdmin','=',1)->first();

        $lang = $request->get('lang') ?? 'fr';
        App::setLocale($lang);

        $portfolio = Portfolio::find($id);
        $medias = Media::where('portfolio_id','=', $id)->orderBy('position')->get();
        $designs = Design::where('status','=','1')->where('user_id','=',$user['id'])->first();
        $profile = Profile::where('user_id','=',$user['id'])->first();
        $socials = Social::where('status','=','1')->where('user_id','=',$id)->get();

        if(!is_null($portfolio) && !is_null($designs) && !is_null($medias) )
            return view('front.'.$profile->template->name.'.pages/portfolio', compact('portfolio','designs','medias', 'profile', 'lang', 'socials'));
        else
            return redirect()->route('index','administrator');
    }


}
