<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminAwardRequest;
use App\Models\Award;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AdminAwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $awards = Auth::user()->awards()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/award', compact('home', 'awards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/award');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminAwardRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminAwardRequest $request)
    {
        $newAward = new Award();
        $newAward->fill($request->all());
        $newAward->save();
        if ($request->file('picture')) {
            $imageName = $newAward->id . '.' . $request->file('picture')->getClientOriginalExtension();
            if(!file_exists(public_path('img/icons/awards')))
                mkdir(public_path('img/icons/awards'));
            Image::make($request->file('picture'))->resize(config('cvrules.icon-width-award'),config('cvrules.icon-height-award'))->save('img/icons/awards/'.$imageName);
            $newAward['picture'] = $imageName;
            $newAward->update();
        }

        return redirect()->route('admin-awards');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $award = Award::find($id);
        if(is_null($award) || $award['user_id'] != Auth::user()->id)
                return redirect()->route('admin-awards')
                    ->with('state', 'The Award does not exist or is not your !!!')
                    ->with('type', 'error');
        else
            return view('admin/pages/award', compact('award'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminAwardRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminAwardRequest $request, $id)
    {
        try{
            $award = Award::find($id);
            if(is_null($award) || $award['user_id'] != Auth::user()->id)
                return redirect()->route('admin-awards')
                    ->with('state', 'The Award does not exist or is not your !!!')
                    ->with('type', 'error');
            $award->fill($request->except('delete_picture'));
            if ($request->file('picture')) {
                $imageName = $award->id . '.png';
                if(!file_exists(public_path('img/icons/awards')))
                    mkdir(public_path('img/icons/awards'));
                Image::make($request->file('picture'))->resize(config('cvrules.icon-width-award'),config('cvrules.icon-height-award'))->save('img/icons/awards/'.$imageName);
                $award['picture'] = $imageName;
            }elseif (!empty($request->get('delete_picture'))) {
                unlink(base_path() . '/public/img/icons/awards/' . $award->id . '.png');
                $award['picture'] = null;
            }
            $award->update();
            return redirect()->route('admin-awards')
                ->with('state', 'Update Award Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Award Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $award = Award::find($id);
            if(is_null($award) || $award['user_id'] != Auth::user()->id)
                return redirect()->route('admin-awards')
                    ->with('state', 'The Award does not exist or is not your !!!')
                    ->with('type', 'error');
            if(!empty($award->picture)){
                if(File::exists(public_path().'/img/icons/awards/'.$award->picture))
                    unlink(public_path().'/img/icons/awards/'.$award->picture);
            }
            $award->delete();
            return redirect()->back()
                ->with('state', 'Delete Award Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Award Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $award = Award::find($id);
            if(is_null($award) || $award['user_id'] != Auth::user()->id)
                return redirect()->route('admin-awards')
                    ->with('state', 'The Award does not exist or is not your !!!')
                    ->with('type', 'error');
            $award->status = 1;
            $award->update();
            return redirect()->back()
                ->with('state', 'Active Award Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Award Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $award = Award::find($id);
            if(is_null($award) || $award['user_id'] != Auth::user()->id)
                return redirect()->route('admin-awards')
                    ->with('state', 'The Award does not exist or is not your !!!')
                    ->with('type', 'error');
            $award->status = 0;
            $award->update();
            return redirect()->back()
                ->with('state', 'Deactive Award Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Award Failed')
                ->with('type', 'error');
        }
    }
}
