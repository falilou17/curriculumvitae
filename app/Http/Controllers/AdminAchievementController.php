<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminAchievementRequest;
use App\Models\Achievement;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AdminAchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $achievements = Auth::user()->achievements()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/achievement', compact('home', 'achievements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/achievement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminAchievementRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminAchievementRequest $request)
    {
        $newAchievement = new Achievement();
        $newAchievement->fill($request->all());
        $newAchievement['score'] = $request->input('score');
        $newAchievement->save();
        if ($request->file('picture')) {
            $imageName = $newAchievement->id . '.' . $request->file('picture')->getClientOriginalExtension();
            if(!file_exists(public_path('img/icons/achievements')))
                mkdir(public_path('img/icons/achievements'));
            Image::make($request->file('picture'))->resize(config('cvrules.icon-width-achievement'),config('cvrules.icon-height-achievement'))->save('img/icons/achievements/'.$imageName);
            $newAchievement['picture'] = $imageName;
            $newAchievement->update();
        }

        return redirect()->route('admin-achievements');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $achievement = Achievement::find($id);
        if(is_null($achievement) || $achievement['user_id'] != Auth::user()->id)
                return redirect()->route('admin-achievements')
                    ->with('state', 'The Stat does not exist or is not your !!!')
                    ->with('type', 'error');
        else
            return view('admin/pages/achievement', compact('achievement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminAchievementRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminAchievementRequest $request, $id)
    {
        try{
            $achievement = Achievement::find($id);
            if(is_null($achievement) || $achievement['user_id'] != Auth::user()->id)
                return redirect()->route('admin-achievements')
                    ->with('state', 'The Stat does not exist or is not your !!!')
                    ->with('type', 'error');
            $achievement->fill($request->except('delete_picture'));
            $achievement['score'] = $request->input('score');
            if ($request->file('picture')) {
                $imageName = $achievement->id . '.png';
                if(!file_exists(public_path('img/icons/achievements')))
                    mkdir(public_path('img/icons/achievements'));
                Image::make($request->file('picture'))->resize(config('cvrules.icon-width-achievement'),config('cvrules.icon-height-achievement'))->save('img/icons/achievements/'.$imageName);
                $achievement['picture'] = $imageName;
            }elseif (!empty($request->get('delete_picture'))) {
                unlink(base_path() . '/public/img/icons/achievements/' . $achievement->id . '.png');
                $achievement['picture'] = null;
            }
            $achievement->update();
            return redirect()->route('admin-achievements')
                ->with('state', 'Update Stat Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Stat Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $achievement = Achievement::find($id);
            if(is_null($achievement) || $achievement['user_id'] != Auth::user()->id)
                return redirect()->route('admin-achievements')
                    ->with('state', 'The Stat does not exist or is not your !!!')
                    ->with('type', 'error');
            if(!empty($achievement->picture)){
                if(File::exists(public_path().'/img/icons/achievements/'.$achievement->picture))
                    unlink(public_path().'/img/icons/achievements/'.$achievement->picture);
            }
            $achievement->delete();
            return redirect()->back()
                ->with('state', 'Delete Stat Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Stat Failed '.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $achievement = Achievement::find($id);
            if(is_null($achievement) || $achievement['user_id'] != Auth::user()->id)
                return redirect()->route('admin-achievements')
                    ->with('state', 'The Stat does not exist or is not your !!!')
                    ->with('type', 'error');
            $achievement->status = 1;
            $achievement->update();
            return redirect()->back()
                ->with('state', 'Active Stat Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Stat Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $home="home";
            $achievement = Achievement::find($id);
            if(is_null($achievement) || $achievement['user_id'] != Auth::user()->id)
                return redirect()->route('admin-achievements')
                    ->with('state', 'The Stat does not exist or is not your !!!')
                    ->with('type', 'error');
            $achievement->status = 0;
            $achievement->update();
            $achievements = Auth::user()->achievements()->get();
            return redirect()->back()
                ->with('state', 'Deactive Stat Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Stat Failed')
                ->with('type', 'error');
        }
    }
}
