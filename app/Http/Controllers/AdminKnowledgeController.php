<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminKnowledgeRequest;
use App\Models\Knowledge;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminKnowledgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $knowledges = Auth::user()->knowledges()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/knowledge', compact('home', 'knowledges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/knowledge');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminKnowledgeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminKnowledgeRequest $request)
    {
        $newKnowledge = new Knowledge();
        $newKnowledge->fill($request->all());
        $newKnowledge->save();

        return redirect()->route('admin-knowledges');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $knowledge = Knowledge::find($id);
        if(is_null($knowledge) || $knowledge['user_id'] != Auth::user()->id)
            return redirect()->route('admin-knowledges')
                ->with('state', 'The Knowledge does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/knowledge', compact('knowledge'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminKnowledgeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminKnowledgeRequest $request, $id)
    {
        try{
            $knowledge = Knowledge::find($id);
            if(is_null($knowledge) || $knowledge['user_id'] != Auth::user()->id)
                return redirect()->route('admin-knowledges')
                    ->with('state', 'The Knowledge does not exist or is not your !!!')
                    ->with('type', 'error');
            $knowledge->fill($request->all());
            $knowledge->update();
            return redirect()->route('admin-knowledges')
                ->with('state', 'Update Knowledge Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Knowledge Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $knowledge = Knowledge::find($id);
            if(is_null($knowledge) || $knowledge['user_id'] != Auth::user()->id)
                return redirect()->route('admin-knowledges')
                    ->with('state', 'The Knowledge does not exist or is not your !!!')
                    ->with('type', 'error');
            $knowledge->delete();
            return redirect()->back()
                ->with('state', 'Delete Knowledge Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Knowledge Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $home="home";
            $knowledge = Knowledge::find($id);
            if(is_null($knowledge) || $knowledge['user_id'] != Auth::user()->id)
                return redirect()->route('admin-knowledges')
                    ->with('state', 'The Knowledge does not exist or is not your !!!')
                    ->with('type', 'error');
            $knowledge->status = 1;
            $knowledge->update();
            return redirect()->back()
                ->with('state', 'Active Knowledge succeeded')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Knowledge Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $home="home";
            $knowledge = Knowledge::find($id);
            if(is_null($knowledge) || $knowledge['user_id'] != Auth::user()->id)
                return redirect()->route('admin-knowledges')
                    ->with('state', 'The Knowledge does not exist or is not your !!!')
                    ->with('type', 'error');
            $knowledge->status = 0;
            $knowledge->update();
            return redirect()->back()
                ->with('state', 'Deactive Knowledge Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Knowledge Failed')
                ->with('type', 'error');
        }
    }
}
