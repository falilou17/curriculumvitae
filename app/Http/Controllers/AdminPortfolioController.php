<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminPortfolioRequest;
use App\Models\Media;
use App\Models\Portfolio;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use League\Flysystem\File;

class AdminPortfolioController extends Controller
{
    /*
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $home="home";
        $portfolios = Auth::user()->portfolios()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/portfolio', compact('home', 'portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/portfolio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminPortfolioRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminPortfolioRequest $request)
    {
        DB::beginTransaction();
        try{
            $newPortfolio = new Portfolio();
            $newPortfolio->fill($request->all());
            $newPortfolio['testimonial'] = $request->input('testimonial');
            $newPortfolio->save();
            $cpt = 1;
            foreach($request->file('media') as $image) {
                if(!empty($image)){
                    $imageName = md5(bcrypt(Carbon::now()->toDateTimeString())).$newPortfolio['id'].'-portfolio.jpg';
                    $myMedia = new Media();
                    $myMedia['path'] = $imageName;
                    $myMedia['position'] = $cpt;
                    $myMedia['portfolio_id'] = $newPortfolio['id'];
                    if(!file_exists(public_path('img/portfolio')))
                        mkdir(public_path('img/portfolio'));
                    Image::make($image)->resize(config('cvrules.picture-portfolio-width'), null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save('img/portfolio/'.$imageName);
                    $myMedia->save();
                    echo $myMedia;
                    $cpt++;
                }
            }

            DB::commit();
            return redirect()->route('admin-portfolios');

        }catch(Exception $e){
            return redirect()->back();
            DB::rollback();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        $medias = Media::where('portfolio_id','=', $id)->orderBy('position')->get();
        if(is_null($portfolio) || $portfolio['user_id'] != Auth::user()->id)
            return redirect()->route('admin-portfolios')
                ->with('state', 'The Portfolio does not exist or is not your !!!')
                ->with('type', 'danger');
        else
            return view('admin/pages/portfolio', compact('portfolio','medias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminPortfolioRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminPortfolioRequest $request, $id)
    {
        DB::beginTransaction();
        $portfolio = Portfolio::find($id);
        if(is_null($portfolio) || $portfolio['user_id'] != Auth::user()->id)
            return redirect()->route('admin-portfolios')
                ->with('state', 'The Portfolio does not exist or is not your !!!')
                ->with('type', 'danger');
        try{
            $portfolio->fill($request->all());
            $portfolio['testimonial'] = $request->input('testimonial');
            $portfolio['datePublish'] = $request->input('datePublish');
            $portfolio->update();

            for($i=0; $i<5; $i++){
                if(!empty($request->file('media')[$i])){
                    if($request->input('FNMedia')[$i] == "null"){
                        $imageName = md5(bcrypt(Carbon::now()->toDateTimeString())).$portfolio['id'].'-portfolio.jpg';
                        $myMedia = new Media();
                        $myMedia['path'] = $imageName;
                        $myMedia['portfolio_id'] = $portfolio['id'];
                        $myMedia['position'] = 1;
                        if(!file_exists(public_path('img/portfolio')))
                            mkdir(public_path('img/portfolio'));
                        Image::make($request->file('media')[$i])->resize(config('cvrules.picture-portfolio-width'), null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save('img/portfolio/'.$imageName);
                        $myMedia->save();
                        $request->input('FNMedia')[$i] = $imageName;
                    }else{
                        $myMedia = Media::where('path','=', $request->input('FNMedia')[$i])->where('portfolio_id','=',$portfolio['id'])->first();
                        unlink(base_path().'/public/img/portfolio/'.$myMedia['path']);
                        Image::make($request->file('media')[$i])->resize(config('cvrules.picture-portfolio-width'), null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save('img/portfolio/'.$myMedia['path']);
                        $myMedia->update();
                    }
                }
            }
            $cpt=1;
            for($i=0; $i<5; $i++){
                if($request->input('FNMedia')[$i] != "null") {
                    $myMedia = Media::where('path','=', $request->input('FNMedia')[$i])->first();
                    if(!is_null($myMedia)){
                        $myMedia['position'] = $cpt;
                        $myMedia->update();
                        $cpt++;
                    }
                }
            }

            DB::commit();
            return redirect()->route('admin-portfolios')
                ->with('state', 'Update Portfolio Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()
                ->with('state', 'Update Portfolio Failed')
                ->with('type', 'danger');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPicture($id)
    {
        try{
            $media = Media::find($id);
                unlink(base_path() . '/public/img/portfolio/' . $media['path']);
                $media->delete();
                return redirect()->back()
                    ->with('state', 'Delete Portfolio picture Succeed')
                    ->with('type', 'success');
        }catch (\Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Portfolio picture Failed')
                ->with('type', 'danger');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $portfolio = Portfolio::find($id);
            if(is_null($portfolio) || $portfolio['user_id'] != Auth::user()->id)
                return redirect()->route('admin-portfolios')
                    ->with('state', 'The Portfolio does not exist or is not your !!!')
                    ->with('type', 'danger');
            foreach($portfolio->medias as $image) {
                unlink(base_path().'/public/img/portfolio/'.$image->path);
                $image->delete();
            }
            $portfolio->delete();
            DB::commit();
            return redirect()->back()
                ->with('state', 'Delete Portfolio Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()
                ->with('state', 'Delete Portfolio Failed')
                ->with('type', 'danger');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $portfolio = Portfolio::find($id);
            if(is_null($portfolio) || $portfolio['user_id'] != Auth::user()->id)
                return redirect()->route('admin-portfolios')
                    ->with('state', 'The Portfolio does not exist or is not your !!!')
                    ->with('type', 'danger');
            $portfolio->status = 1;
            $portfolio->update();
            return redirect()->back()
                ->with('state', 'Active Portfolio succeeded')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Portfolio Failed')
                ->with('type', 'danger');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $portfolio = Portfolio::find($id);
            if(is_null($portfolio) || $portfolio['user_id'] != Auth::user()->id)
                return redirect()->route('admin-portfolios')
                    ->with('state', 'The Portfolio does not exist or is not your !!!')
                    ->with('type', 'danger');
            $portfolio->status = 0;
            $portfolio->update();
            return redirect()->back()
                ->with('state', 'Deactive Portfolio Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Portfolio Failed')
                ->with('type', 'danger');
        }
    }
}
