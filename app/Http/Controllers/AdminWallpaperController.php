<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminWallpaperRequest;
use App\Models\Profile;
use App\Models\Template;
use App\Models\User;
use App\Models\Wallpaper;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AdminWallpaperController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $page = "Wallpaper";
        $user = User::where('isActive', '=', 1)->where('id','!=',1)->first();
        if(empty($user) || !$user->isActive)
            $user = User::where('isAdmin','=',1)->first();
        $id = $user['id'];
        $profile = Profile::where('user_id','=',$id)->first();
        $wallpaper = Wallpaper::where('user_id', Auth::user()->id)->first();
        $template = $profile->template;
        if(is_null($wallpaper) || $wallpaper['user_id'] != Auth::user()->id)
            return redirect()->back()
                ->with('state', 'The resource does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/wallpaper', compact('page', 'wallpaper', 'template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminWallpaperRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(AdminWallpaperRequest $request)
    {
        $wallpaper = Auth::user()->wallpaper()->first();
        try {
            if ($request->file('strength')) {
                if($wallpaper->strength != "strength-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/strengths/'.$wallpaper->strength))
                    unlink(public_path().'/img/wallpapers/strengths/'.$wallpaper->strength);
                $imageName = $wallpaper->user_id . '-strength.' . $request->file('strength')->getClientOriginalExtension();
                Image::make($request->file('strength'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/strengths/'.$imageName);
                $wallpaper['strength'] = $imageName;
            }
            if ($request->file('achievement')) {
                if($wallpaper->achievement != "achievement-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/achievements/'.$wallpaper->achievement))
                    unlink(public_path().'/img/wallpapers/achievements/'.$wallpaper->achievement);
                $imageName = $wallpaper->user_id . '-achievement.' . $request->file('achievement')->getClientOriginalExtension();
                Image::make($request->file('achievement'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/achievements/'.$imageName);
                $wallpaper['achievement'] = $imageName;
            }
            if ($request->file('reference')) {
                if($wallpaper->reference != "reference-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/references/'.$wallpaper->reference))
                    unlink(public_path().'/img/wallpapers/references/'.$wallpaper->reference);
                $imageName = $wallpaper->user_id . '-reference.' . $request->file('reference')->getClientOriginalExtension();
                Image::make($request->file('reference'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/references/'.$imageName);
                $wallpaper['reference'] = $imageName;
            }
            if ($request->file('portfolio')) {
                if($wallpaper->reference != "portfolio-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/portfolios/'.$wallpaper->portfolio))
                    unlink(public_path().'/img/wallpapers/portfolios/'.$wallpaper->portfolio);
                $imageName = $wallpaper->user_id . '-portfolio.' . $request->file('portfolio')->getClientOriginalExtension();
                Image::make($request->file('portfolio'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/portfolios/'.$imageName);
                $wallpaper['portfolio'] = $imageName;
            }
            if ($request->file('contact')) {
                if($wallpaper->reference != "contact-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/contacts/'.$wallpaper->contact))
                    unlink(public_path().'/img/wallpapers/contacts/'.$wallpaper->contact);
                $imageName = $wallpaper->user_id . '-contact.' . $request->file('contact')->getClientOriginalExtension();
                Image::make($request->file('contact'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/contacts/'.$imageName);
                $wallpaper['contact'] = $imageName;
            }
            if ($request->file('service')) {
                if($wallpaper->reference != "service-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/services/'.$wallpaper->service))
                    unlink(public_path().'/img/wallpapers/services/'.$wallpaper->service);
                $imageName = $wallpaper->user_id . '-service.' . $request->file('service')->getClientOriginalExtension();
                Image::make($request->file('service'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/services/'.$imageName);
                $wallpaper['service'] = $imageName;
            }
            if ($request->file('profile')) {
                if($wallpaper->reference != "profile-wallpaper.jpg" && File::exists(public_path().'/img/wallpapers/profiles/'.$wallpaper->profile))
                    unlink(public_path().'/img/wallpapers/profiles/'.$wallpaper->profile);
                $imageName = $wallpaper->user_id . '-profile.' . $request->file('profile')->getClientOriginalExtension();
                Image::make($request->file('profile'))->resize(config('cvrules.cover-width'),null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('img/wallpapers/profiles/'.$imageName);
                $wallpaper['profile'] = $imageName;
            }

            $wallpaper->update();
            return redirect()->back()
                ->with('state', 'Update Wallpaper Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Wallpaper Failed '.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
