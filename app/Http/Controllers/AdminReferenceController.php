<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminReferenceRequest;
use App\Models\Reference;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AdminReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $references = Auth::user()->references()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/reference', compact('home', 'references'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/reference');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdminReferenceRequest $request)
    {
        $newReference = new Reference();
        $newReference->fill($request->all());
        $newReference['description'] = $request->input('description');
        $newReference->save();
        if ($request->file('picture')) {
            $imageName = $newReference['id'] . '-referencePicture.' . $request->file('picture')->getClientOriginalExtension();
            Image::make($request->file('picture'))->resize(config('cvrules.picture-reference-width'), null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save('img/reference/'.$imageName);
            $newReference['picture'] = $imageName;
        }
        $newReference->save();
        return redirect()->route('admin-references');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reference = Reference::find($id);
        if(is_null($reference) || $reference['user_id'] != Auth::user()->id)
            return redirect()->route('admin-references')
                ->with('state', 'The Testimonial does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/reference', compact('reference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminReferenceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminReferenceRequest $request, $id)
    {
        try{
            $reference = Reference::find($id);
            if(is_null($reference) || $reference['user_id'] != Auth::user()->id)
                return redirect()->route('admin-references')
                    ->with('state', 'The Testimonial does not exist or is not your !!!')
                    ->with('type', 'error');
            $reference->fill($request->all());
            $reference['description'] = $request->input('description');
            $reference->update();
            if ($request->file('picture')) {
                $imageName = $reference['id'] . '-referencePicture.' . $request->file('picture')->getClientOriginalExtension();
                Image::make($request->file('picture'))->resize(config('cvrules.picture-reference-width'), null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save('img/reference/'.$imageName);
                $reference['picture'] = $imageName;
            }
            $reference->update();
            return redirect()->route('admin-references')
                ->with('state', 'Update Testimonial Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Testimonial Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $reference = Reference::find($id);
            if(is_null($reference) || $reference['user_id'] != Auth::user()->id)
                return redirect()->route('admin-references')
                    ->with('state', 'The Testimonial does not exist or is not your !!!')
                    ->with('type', 'error');
            if(!empty($reference->picture) && $reference->picture != "reference.png" && File::exists(public_path().'/img/reference/'.$reference->picture))
                unlink(public_path().'/img/reference/'.$reference->picture);
            $reference->delete();
            return redirect()->back()
                ->with('state', 'Delete Testimonial Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Testimonial Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $reference = Reference::find($id);
            if(is_null($reference) || $reference['user_id'] != Auth::user()->id)
                return redirect()->route('admin-references')
                    ->with('state', 'The Testimonial does not exist or is not your !!!')
                    ->with('type', 'error');
            $reference->status = 1;
            $reference->update();
            return redirect()->back()
                ->with('state', 'Active Testimonial Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Testimonial Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $reference = Reference::find($id);
            if(is_null($reference) || $reference['user_id'] != Auth::user()->id)
                return redirect()->route('admin-references')
                    ->with('state', 'The Testimonial does not exist or is not your !!!')
                    ->with('type', 'error');
            $reference->status = 0;
            $reference->update();
            return redirect()->back()
                ->with('state', 'Deactive Testimonial Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Testimonial Failed')
                ->with('type', 'error');
        }
    }
}
