<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminStrengthRequest;
use App\Models\Strength;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminStrengthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $strengths = Auth::user()->strengths()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/strength', compact('home', 'strengths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/strength');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminStrengthRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStrengthRequest $request)
    {
        $newStrength = new Strength();
        $newStrength->fill($request->all());
        $newStrength->save();

        return redirect()->route('admin-strengths');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $strength = Strength::find($id);
        if(is_null($strength) || $strength['user_id'] != Auth::user()->id)
            return redirect()->route('admin-strengths')
                ->with('state', 'The Strength does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/strength', compact('strength'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminStrengthRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminStrengthRequest $request, $id)
    {
        try{
            $strength = Strength::find($id);
            if(is_null($strength) || $strength['user_id'] != Auth::user()->id)
                return redirect()->route('admin-strengths')
                    ->with('state', 'The Strength does not exist or is not your !!!')
                    ->with('type', 'error');
            $strength->fill($request->all());
            $strength->update();
            return redirect()->route('admin-strengths')
                ->with('state', 'Update Strength Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Strength Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $home="home";
            $strength = Strength::find($id);
            if(is_null($strength) || $strength['user_id'] != Auth::user()->id)
                return redirect()->route('admin-strengths')
                    ->with('state', 'The Strength does not exist or is not your !!!')
                    ->with('type', 'error');
            $strength->delete();
            return redirect()->back()
                ->with('state', 'Delete Strength Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Strength Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $home="home";
            $strength = Strength::find($id);
            if(is_null($strength) || $strength['user_id'] != Auth::user()->id)
                return redirect()->route('admin-strengths')
                    ->with('state', 'The Strength does not exist or is not your !!!')
                    ->with('type', 'error');
            $strength->status = 1;
            $strength->update();
            return redirect()->back()
                ->with('state', 'Active Strength Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Strength Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $strength = Strength::find($id);
            if(is_null($strength) || $strength['user_id'] != Auth::user()->id)
                return redirect()->route('admin-strengths')
                    ->with('state', 'The Strength does not exist or is not your !!!')
                    ->with('type', 'error');
            $strength->status = 0;
            $strength->update();
            return redirect()->back()
                ->with('state', 'Deactive Strength Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Strength Failed')
                ->with('type', 'error');
        }
    }
}
