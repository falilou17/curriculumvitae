<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminCompetenceRequest;
use App\Models\Competence;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AdminCompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $competences = Auth::user()->competences()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/competence', compact('home', 'competences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/competence');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminCompetenceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminCompetenceRequest $request)
    {
        $newComp = new Competence();
        $newComp->fill($request->all());
        $newComp->save();
        if ($request->file('picture')) {
            $imageName = $newComp->id . '.' . $request->file('picture')->getClientOriginalExtension();
            if(!file_exists(public_path('img/icons/competences')))
                mkdir(public_path('img/icons/competences'));
            Image::make($request->file('picture'))->resize(config('cvrules.icon-width-competence'),config('cvrules.icon-height-competence'))->save('img/icons/competences/'.$imageName);
            $newComp['picture'] = $imageName;
            $newComp->update();
        }

        return redirect()->route('admin-competences');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competence = Competence::find($id);
        if(is_null($competence) || $competence['user_id'] != Auth::user()->id)
                return redirect()->route('admin-competences')
                    ->with('state', 'The Service does not exist or is nor your !!!')
                    ->with('type', 'error');
        else
            return view('admin/pages/competence', compact('competence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminCompetenceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminCompetenceRequest $request, $id)
    {
        try{
            $competence = Competence::find($id);
            if(is_null($competence) || $competence['user_id'] != Auth::user()->id)
                return redirect()->route('admin-competences')
                    ->with('state', 'The Service does not exist or is nor your !!!')
                    ->with('type', 'error');
            $competence->fill($request->except('delete_picture'));
            if ($request->file('picture')) {
                $imageName = $competence->id . '.png';
                if(!file_exists(public_path('img/icons/competences')))
                    mkdir(public_path('img/icons/competences'));
                Image::make($request->file('picture'))->resize(config('cvrules.icon-width-competence'),config('cvrules.icon-height-competence'))->save('img/icons/competences/'.$imageName);
                $competence['picture'] = $imageName;
            }elseif (!empty($request->get('delete_picture'))) {
                unlink(base_path() . '/public/img/icons/competences/' . $competence->id . '.png');
                $competence['picture'] = null;
            }
            $competence->update();
            return redirect()->route('admin-competences')
                ->with('state', 'Update Service Succeed')
                ->with('type', 'success');
            }catch(Exception $e){
                return redirect()->back()
                ->with('state', 'Update Service Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $competence = Competence::find($id);
            if(is_null($competence) || $competence['user_id'] != Auth::user()->id)
                return redirect()->route('admin-competences')
                    ->with('state', 'The Service does not exist or is nor your !!!')
                    ->with('type', 'error');
            if(!empty($competence->picture)){
                if(File::exists(public_path().'/img/icons/competences/'.$competence->picture))
                    unlink(public_path().'/img/icons/competences/'.$competence->picture);
            }
            $competence->delete();
            return redirect()->back()
                ->with('state', 'Delete Service Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Service Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $competence = Competence::find($id);
            if(is_null($competence) || $competence['user_id'] != Auth::user()->id)
                return redirect()->route('admin-competences')
                    ->with('state', 'The Service does not exist or is nor your !!!')
                    ->with('type', 'error');
            $competence->status = 1;
            $competence->update();
            return redirect()->back()
                ->with('state', 'Active Service Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Service Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $competence = Competence::find($id);
            if(is_null($competence) || $competence['user_id'] != Auth::user()->id)
                return redirect()->route('admin-competences')
                    ->with('state', 'The Service does not exist or is nor your !!!')
                    ->with('type', 'error');
            $competence->status = 0;
            $competence->update();
            return redirect()->back()
                ->with('state', 'Deactive Service Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Service Failed')
                ->with('type', 'error');
        }
    }
}
