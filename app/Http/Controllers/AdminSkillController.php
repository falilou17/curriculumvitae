<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminSkillRequest;
use App\Models\Skill;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $skills = Auth::user()->skills()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/skill', compact('home', 'skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/skill');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminSkillRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSkillRequest $request)
    {
        $newSkill = new Skill();
        $newSkill->fill($request->all());
        $newSkill->save();

        return redirect()->route('admin-skills');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skill = Skill::find($id);
        if(is_null($skill) || $skill['user_id'] != Auth::user()->id)
            return redirect()->route('admin-skills')
                ->with('state', 'The Skill does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/skill', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminSkillRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSkillRequest $request, $id)
    {
        try{
            $skill = Skill::find($id);
            if(is_null($skill) || $skill['user_id'] != Auth::user()->id)
                return redirect()->route('admin-skills')
                    ->with('state', 'The Skill does not exist or is not your !!!')
                    ->with('type', 'error');
            $skill->fill($request->all());
            $skill->update();
            return redirect()->route('admin-skills')
                ->with('state', 'Update Skill Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Skill Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $home="home";
            $skill = Skill::find($id);
            if(is_null($skill) || $skill['user_id'] != Auth::user()->id)
                return redirect()->route('admin-skills')
                    ->with('state', 'The Skill does not exist or is not your !!!')
                    ->with('type', 'error');
            $skill->delete();
            $skills = Auth::user()->skills()->get();
            return redirect()->back()
                ->with('state', 'Delete Skill Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Skill Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $skill = Skill::find($id);
            if(is_null($skill) || $skill['user_id'] != Auth::user()->id)
                return redirect()->route('admin-skills')
                    ->with('state', 'The Skill does not exist or is not your !!!')
                    ->with('type', 'error');
            $skill->status = 1;
            $skill->update();
            return redirect()->back()
                ->with('state', 'Active Skill Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Skill Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $skill = Skill::find($id);
            if(is_null($skill) || $skill['user_id'] != Auth::user()->id)
                return redirect()->route('admin-skills')
                    ->with('state', 'The Skill does not exist or is not your !!!')
                    ->with('type', 'error');
            $skill->status = 0;
            $skill->update();
            return redirect()->back()
                ->with('state', 'Deactive Skill Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Skill Failed')
                ->with('type', 'error');
        }
    }
}
