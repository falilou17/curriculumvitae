<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\Proverb;
use App\Models\User;
use App\Models\Wallpaper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminProverbController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proverb = Auth::user()->proverb()->first();
        $user = User::where('isActive', '=', 1)->where('id','!=',1)->first();
        if(empty($user) || !$user->isActive)
            $user = User::where('isAdmin','=',1)->first();
        $id = $user['id'];
        $profile = Profile::where('user_id','=',$id)->first();
        $template = $profile->template;
        if(is_null($proverb)  || $proverb['user_id'] != Auth::user()->id)
            return redirect()->route('admin-proverbs')
                ->with('state', 'The Proverb does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/proverb', compact('proverb', 'template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $proverb = Auth::user()->proverb()->first();
        if(is_null($proverb)  || $proverb['user_id'] != Auth::user()->id)
            return redirect()->route('admin-proverbs')
                ->with('state', 'The Proverb does not exist or is not your !!!')
                ->with('type', 'error');
        try {
            $proverb->fill($request->all());
            $proverb->update();
            return redirect()->back()
                ->with('state', 'Update Proverbs Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Proverbs Failed')
                ->with('type', 'error');
        }
    }
}
