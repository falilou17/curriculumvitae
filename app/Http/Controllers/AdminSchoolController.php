<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminSchoolRequest;
use App\Models\School;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $schools = Auth::user()->schools()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/school', compact('home', 'schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/school');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminSchoolRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSchoolRequest $request)
    {
        $newSchool = new School();
        $newSchool->fill($request->all());
        $newSchool['description'] = $request->input('description');
        $newSchool->save();

        return redirect()->route('admin-schools');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);
        if(is_null($school) || $school['user_id'] != Auth::user()->id)
            return redirect()->route('admin-schools')
                ->with('state', 'The School does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/school', compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminSchoolRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSchoolRequest $request, $id)
    {
        try{
            $school = School::find($id);
            if(is_null($school) || $school['user_id'] != Auth::user()->id)
                return redirect()->route('admin-schools')
                    ->with('state', 'The School does not exist or is not your !!!')
                    ->with('type', 'error');
            $school->fill($request->all());
            $school['description'] = $request->input('description');
            $school->update();
            return redirect()->route('admin-schools')
                ->with('state', 'Update School Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update School Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $school = School::find($id);
            if(is_null($school) || $school['user_id'] != Auth::user()->id)
                return redirect()->route('admin-schools')
                    ->with('state', 'The School does not exist or is not your !!!')
                    ->with('type', 'error');
            $school->delete();
            return redirect()->back()
                ->with('state', 'Delete School Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete School Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $school = School::find($id);
            if(is_null($school) || $school['user_id'] != Auth::user()->id)
                return redirect()->route('admin-schools')
                    ->with('state', 'The School does not exist or is not your !!!')
                    ->with('type', 'error');
            $school->status = 1;
            $school->update();
            return redirect()->back()
                ->with('state', 'Active School Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active School Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $school = School::find($id);
            if(is_null($school) || $school['user_id'] != Auth::user()->id)
                return redirect()->route('admin-schools')
                    ->with('state', 'The School does not exist or is not your !!!')
                    ->with('type', 'error');
            $school->status = 0;
            $school->update();
            return redirect()->back()
                ->with('state', 'Deactive School Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive School Failed')
                ->with('type', 'error');
        }
    }
}
