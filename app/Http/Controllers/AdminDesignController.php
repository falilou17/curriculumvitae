<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminDesignRequest;
use App\Models\Design;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminDesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $designs = Auth::user()->designs()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/design', compact('home', 'designs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/design');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminDesignRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminDesignRequest $request)
    {
        $newDesign = new Design();
        $newDesign->fill($request->all());
        $newDesign['description'] = $request->input('description');
        $newDesign->save();

        return redirect()->route('admin-designs');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $design = Design::find($id);
        if(is_null($design) || $design['user_id'] != Auth::user()->id)
            return redirect()->route('admin-designs')
                ->with('state', 'The Design does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/design', compact('design'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminDesignRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminDesignRequest $request, $id)
    {
        try{
            $design = Design::find($id);
            if(is_null($design) || $design['user_id'] != Auth::user()->id)
                return redirect()->route('admin-designs')
                    ->with('state', 'The Design does not exist or is not your !!!')
                    ->with('type', 'error');
            $design->fill($request->all());
            $design['description'] = $request->input('description');
            $design->update();
            return redirect()->route('admin-designs')
                ->with('state', 'Update Design Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Design Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $design = Design::find($id);
            if(is_null($design) || $design['user_id'] != Auth::user()->id)
                return redirect()->route('admin-designs')
                    ->with('state', 'The Design does not exist or is not your !!!')
                    ->with('type', 'error');
            if($design->status == 0){
                $design->delete();
                return redirect()->back()
                    ->with('state', 'Delete Design Succeed')
                    ->with('type', 'success');
            }else{
                return redirect()->back()
                    ->with('state', 'You Can\'t Delete Active Design')
                    ->with('type', 'error');
            }

        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Design Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $design = Design::find($id);
            if(is_null($design) || $design['user_id'] != Auth::user()->id)
                return redirect()->route('admin-designs')
                    ->with('state', 'The Design does not exist or is not your !!!')
                    ->with('type', 'error');
            $designs = Auth::user()->designs()->get();
            foreach($designs as $des){
                $des->status = 0;
                $des->update();
            }
            $design->status = 1;
            $design->update();
            return redirect()->back()
                ->with('state', 'Active Design Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Design Failed')
                ->with('type', 'error');
        }
    }
}
