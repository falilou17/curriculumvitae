<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\AdminSetupRequest;
use App\Http\Requests\AdminUpdateCredentialRequest;
use App\Models\Achievement;
use App\Models\Award;
use App\Models\Competence;
use App\Models\Design;
use App\Models\Experience;
use App\Models\Knowledge;
use App\Models\Mail;
use App\Models\Portfolio;
use App\Models\Profile;
use App\Models\Proverb;
use App\Models\Reference;
use App\Models\School;
use App\Models\Skill;
use App\Models\Social;
use App\Models\Strength;
use App\Models\User;
use App\Models\Wallpaper;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display login Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/pages/login');
    }

    /**
     * Display Home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('admin/pages/home');
    }

    /**
     * Login admin.
     *
     * @param AdminLoginRequest $request
     * @return \Illuminate\Http\Response
     */
    public function login(AdminLoginRequest $request)
    {
        try{
            $login = $request->input('login');
            $password = $request->input('password');
            if(Auth::attempt(['login' => $login, 'password' => $password, 'isActive' => true ])){
                return redirect()->route('admin-home')
                    ->with('state', 'Hi '.$login.' welcome to your admin panel')
                    ->with('type', 'success');
            }else{
                return redirect()->back()
                    ->with('state', 'Bad Username / Password')
                    ->with('type', 'danger');
            }
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Login failed')
                ->with('type', 'danger');
        }
    }

    /**
     *  logout admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin-login');
    }



    /**
     * Show the form for editing admin credential.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('admin/pages/update');
    }

    /**
     * Update admin credential.
     *
     * @param AdminUpdateCredentialRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateCredentialRequest $request)
    {
        try{
            $user = User::where('id','=',Auth::user()->id)->first();

            if(is_null($user) || $user['id'] != Auth::user()->id)
                return redirect()->back()
                    ->with('state', 'The User does not exist !!!')
                    ->with('type', 'error');
            $user['password'] = Hash::make($request->input('password'));
            $user->update();
            return redirect()->route('admin-home')
                ->with('state', 'Update credential succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update credential Failed')
                ->with('type', 'error')
                ->withInput();
        }
    }

    /**
     * Active all item.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeAll()
    {
        try{
            $portfolios = Portfolio::where('user_id','=',Auth::user()->id)->get();
            $competences = Competence::where('user_id','=',Auth::user()->id)->get();
            $strengths = Strength::where('user_id','=',Auth::user()->id)->get();
            $schools = School::where('user_id','=',Auth::user()->id)->get();
            $experiences = Experience::where('user_id','=',Auth::user()->id)->get();
            $references = Reference::where('user_id','=',Auth::user()->id)->get();
            $skills = Skill::where('user_id','=',Auth::user()->id)->get();
            $knowledges = Knowledge::where('user_id','=',Auth::user()->id)->get();
            $awards = Award::where('user_id','=',Auth::user()->id)->get();
            $achievements = Achievement::where('user_id','=',Auth::user()->id)->get();
            $socials = Social::where('user_id','=',Auth::user()->id)->get();

            foreach($portfolios as $portfolio){
                $portfolio['status'] = 1;
                $portfolio->update();
            }
            foreach($competences as $competence){
                $competence['status'] = 1;
                $competence->update();
            }
            foreach($strengths as $strength){
                $strength['status'] = 1;
                $strength->update();
            }
            foreach($schools as $school){
                $school['status'] = 1;
                $school->update();
            }
            foreach($experiences as $experience){
                $experience['status'] = 1;
                $experience->update();
            }
            foreach($references as $reference){
                $reference['status'] = 1;
                $reference->update();
            }
            foreach($skills as $skill){
                $skill['status'] = 1;
                $skill->update();
            }
            foreach($knowledges as $knowledge){
                $knowledge['status'] = 1;
                $knowledge->update();
            }
            foreach($awards as $award){
                $award['status'] = 1;
                $award->update();
            }
            foreach($achievements as $achievement){
                $achievement['status'] = 1;
                $achievement->update();
            }
            foreach($socials as $social){
                $social['status'] = 1;
                $social->update();
            }

            return redirect()->back()
                ->with('state', 'Active all item Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active all item Failed')
                ->with('type', 'error');
        }
    }


    /**
     * Desactive all item.
     *
     * @return \Illuminate\Http\Response
     */
    public function desactiveAll()
    {
        try{
            $portfolios = Portfolio::where('user_id','=',Auth::user()->id)->get();
            $competences = Competence::where('user_id','=',Auth::user()->id)->get();
            $strengths = Strength::where('user_id','=',Auth::user()->id)->get();
            $schools = School::where('user_id','=',Auth::user()->id)->get();
            $experiences = Experience::where('user_id','=',Auth::user()->id)->get();
            $references = Reference::where('user_id','=',Auth::user()->id)->get();
            $skills = Skill::where('user_id','=',Auth::user()->id)->get();
            $knowledges = Knowledge::where('user_id','=',Auth::user()->id)->get();
            $awards = Award::where('user_id','=',Auth::user()->id)->get();
            $achievements = Achievement::where('user_id','=',Auth::user()->id)->get();
            $socials = Social::where('user_id','=',Auth::user()->id)->get();

            foreach($portfolios as $portfolio){
                $portfolio['status'] = 0;
                $portfolio->update();
            }
            foreach($competences as $competence){
                $competence['status'] = 0;
                $competence->update();
            }
            foreach($strengths as $strength){
                $strength['status'] = 0;
                $strength->update();
            }
            foreach($schools as $school){
                $school['status'] = 0;
                $school->update();
            }
            foreach($experiences as $experience){
                $experience['status'] = 0;
                $experience->update();
            }
            foreach($references as $reference){
                $reference['status'] = 0;
                $reference->update();
            }
            foreach($skills as $skill){
                $skill['status'] = 0;
                $skill->update();
            }
            foreach($knowledges as $knowledge){
                $knowledge['status'] = 0;
                $knowledge->update();
            }
            foreach($awards as $award){
                $award['status'] = 0;
                $award->update();
            }
            foreach($achievements as $achievement){
                $achievement['status'] = 0;
                $achievement->update();
            }
            foreach($socials as $social){
                $social['status'] = 0;
                $social->update();
            }

            return redirect()->back()
                ->with('state', 'Desactive all item Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Desactive all item Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Delete all item.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset()
    {
        try{
            $profile = Profile::where('user_id','=',Auth::user()->id)->first();
            $portfolios = Portfolio::where('user_id','=',Auth::user()->id)->get();
            $competences = Competence::where('user_id','=',Auth::user()->id)->get();
            $strengths = Strength::where('user_id','=',Auth::user()->id)->get();
            $schools = School::where('user_id','=',Auth::user()->id)->get();
            $experiences = Experience::where('user_id','=',Auth::user()->id)->get();
            $references = Reference::where('user_id','=',Auth::user()->id)->get();
            $skills = Skill::where('user_id','=',Auth::user()->id)->get();
            $knowledges = Knowledge::where('user_id','=',Auth::user()->id)->get();
            $awards = Award::where('user_id','=',Auth::user()->id)->get();
            $achievements = Achievement::where('user_id','=',Auth::user()->id)->get();
            $socials = Social::where('user_id','=',Auth::user()->id)->get();
            $designs = Design::where('user_id','=',Auth::user()->id)->Where('status','=',0)->get();
            $proverbs = Proverb::where('user_id','=',Auth::user()->id)->first();
            $wallpaper = Wallpaper::where('user_id','=',Auth::user()->id)->get();

            $profile['firstName'] = '';
            $profile['lastName'] = '';
            $profile['email'] = '';
            $profile['descBrief'] = '';
            $profile['descLong'] = '';
            $profile['coverDesc'] = '';
            $profile['job'] = '';
            if($profile['cv'] != "cv.jpg" && !empty($profile['cv']))
                unlink(public_path().'/img/cv/'.$profile['cv']);
            $profile['cv'] = '';
            if($profile['profilePicture'] != 'profilePicture.jpg')
                unlink(public_path().'/img/'.$profile['profilePicture']);
            $profile['profilePicture'] = 'profilePicture.jpg';
            if($profile['coverPicture'] != 'coverPicture.jpg')
                unlink(public_path().'/img/'.$profile['coverPicture']);
            $profile['coverPicture'] = 'coverPicture.jpg';
            $profile['address'] = '';
            $profile['website'] = '';
            $profile['phone'] = '';
            $profile->update();

            foreach($portfolios as $portfolio){
                foreach($portfolio->medias as $image) {
                    if(File::exists(public_path().'/img/portfolio/'.$image->path))
                        unlink(public_path().'/img/portfolio/'.$image->path);
                    $image->delete();
                }
                $portfolio->delete();
            }
            foreach($competences as $competence){
                if(!empty($competence->picture) && File::exists(public_path().'/img/icons/competences/'.$competence->picture))
                    unlink(public_path().'/img/icons/competences/'.$competence->picture);
                $competence->delete();
            }
            foreach($strengths as $strength){
                $strength->delete();
            }
            foreach($schools as $school){
                $school->delete();
            }
            foreach($experiences as $experience){
                $experience->delete();
            }
            foreach($references as $reference){
                if($reference->picture != "reference.png")
                    unlink(base_path().'/public/img/reference/'.$reference->picture);
                $reference->delete();
            }
            foreach($skills as $skill){
                $skill->delete();
            }
            foreach($knowledges as $knowledge){
                $knowledge->delete();
            }
            foreach($awards as $award){
                if(!empty($award->picture) && File::exists(public_path().'/img/icons/awards/'.$award->picture))
                    unlink(public_path().'/img/icons/awards/'.$award->picture);
                $award->delete();
            }
            foreach($achievements as $achievement){
                if(!empty($achievement->picture) && File::exists(public_path().'/img/icons/achievements/'.$achievement->picture))
                    unlink(public_path().'/img/icons/achievements/'.$achievement->picture);
                $achievement->delete();
            }
            foreach($socials as $social){
                $social->delete();
            }
            foreach($designs as $design){
                $design->delete();
            }

            if($wallpaper->strength != 'strength-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/strengths/'.$strength->picture)){
                unlink(public_path().'/img/wallpapers/strengths/'.$strength->picture);
                $wallpaper->strength = 'strength-wallpaper.jpg';
            }
            if($wallpaper->reference != 'reference-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/references/'.$reference->picture)){
                unlink(public_path().'/img/wallpapers/references/'.$reference->picture);
                $wallpaper->reference = 'reference-wallpaper.jpg';
            }
            if($wallpaper->achievement != 'achievement-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/achievements/'.$achievement->picture)){
                unlink(public_path().'/img/wallpapers/achievements/'.$achievement->picture);
                $wallpaper->achievement = 'achievement-wallpaper.jpg';
            }
            $wallpaper->update();

            $proverbs['portfolio'] = '';
            $proverbs['competence'] = '';
            $proverbs['strength'] = '';
            $proverbs['resume'] = '';
            $proverbs['reference'] = '';
            $proverbs['skill'] = '';
            $proverbs['knowledge'] = '';
            $proverbs['award'] = '';
            $proverbs['achievement'] = '';
            $proverbs['contact'] = '';
            $proverbs->update();

            return redirect()->route('admin-home')
                ->with('state', 'Reset Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->route('admin-home')
                ->with('state', 'Reset Failed')
                ->with('type', 'error');
        }
    }
}
