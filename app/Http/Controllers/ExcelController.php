<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminExportRequest;
use App\Models\Achievement;
use App\Models\Award;
use App\Models\Competence;
use App\Models\Design;
use App\Models\Experience;
use App\Models\Knowledge;
use App\Models\Media;
use App\Models\Portfolio;
use App\Models\Profile;
use App\Models\Proverb;
use App\Models\Reference;
use App\Models\School;
use App\Models\Skill;
use App\Models\Social;
use App\Models\Strength;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    /**
     * Export User information on Excel File
     *
     * @param AdminExportRequest $request
     * @return \Illuminate\Http\Response
     */
    public function exportXLSX(AdminExportRequest $request)
    {
        try{
            $profile = Profile::where('user_id','=',Auth::user()->id)->first();
            $portfolios = Portfolio::where('user_id','=',Auth::user()->id)->get();
            $competences = Competence::where('user_id','=',Auth::user()->id)->get();
            $strengths = Strength::where('user_id','=',Auth::user()->id)->get();
            $schools = School::where('user_id','=',Auth::user()->id)->orderBy('dateStart', 'Desc')->get();
            $experiences = Experience::where('user_id','=',Auth::user()->id)->orderBy('dateStart', 'Desc')->get();
            $references = Reference::where('user_id','=', Auth::user()->id)->get();
            $skills = Skill::where('user_id','=',Auth::user()->id)->get();
            $knowledges = Knowledge::where('user_id','=',Auth::user()->id)->get();
            $awards = Award::where('user_id','=',Auth::user()->id)->get();
            $achievements = Achievement::where('user_id','=',Auth::user()->id)->get();
            $socials = Social::where('user_id','=',Auth::user()->id)->get();
            $proverbs = Proverb::where('user_id','=',Auth::user()->id)->first();
            $designs = Design::where('user_id','=',Auth::user()->id)->get();

            Excel::create('Cv-Informations-export', function($excel)
            use($request, $profile, $portfolios, $competences, $strengths, $schools, $experiences, $references, $skills, $knowledges, $awards, $achievements, $socials, $proverbs, $designs) {
                $excel->setTitle('Cv-Informations');
                $excel->setTitle('Cv-Informations');
                $excel->setCreator('CurriculumVitae.dev')->setCompany('CurriculumVitae.dev');
                $excel->setDescription('My Cv Informations');

                if(!empty($profile) && $request->input('profile')){
                    $excel->sheet('Profile', function($sheet) use($profile){
                        $sheet->fromArray(array(
                            array('First Name', $profile['firstName']),
                            array('Last Name', $profile['lastName']),
                            array('Email', $profile['email']),
                            array('Description Brief', $profile['descBrief']),
                            array('Description Long', $profile['descLong']),
                            array('Cover Description', $profile['coverDesc']),
                            array('Job', $profile['job']),
                            array('Address', $profile['address']),
                            array('Website', $profile['webSite']),
                            array('Phone Number', $profile['phone']),
                        ), null, 'A1', false);
                    });
                }
                if(!empty($portfolios) && $request->input('portfolio')){
                    $excel->sheet('Portfolios', function($sheet) use($portfolios) {
                        $data = array();
                        foreach($portfolios as $portfolio){
                            $data = array_merge($data, array(
                                    array('Name', $portfolio['name']),
                                    array('Description', $portfolio['description']),
                                    array('Technologies', $portfolio['technologies']),
                                    array('Client', $portfolio['client']),
                                    array('Testimonial', $portfolio['testimonial']),
                                    array('Publish Date', $portfolio['datePublish']),
                                    array('Status', $portfolio['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($competences) && $request->input('competence')){
                    $excel->sheet('Services', function($sheet) use($competences) {
                        $data = array();
                        foreach($competences as $competence){
                            $data = array_merge($data, array(
                                    array('Name', $competence['name']),
                                    array('Description', $competence['description']),
                                    array('Icon', $competence['icon']),
                                    array('Picture', !empty($competence['picture']) ? $this->imageTobase64(public_path().'/img/icons/competences/'.$competence['picture']) : ''),
                                    array('Status', $competence['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($strengths) && $request->input('strength')){
                    $excel->sheet('Strengths', function($sheet) use($strengths) {
                        $data = array();
                        foreach($strengths as $strength){
                            $data = array_merge($data, array(
                                    array('Name', $strength['name']),
                                    array('Pourcentage', $strength['pourcentage']),
                                    array('Status', $strength['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($schools) && $request->input('school')){
                    $excel->sheet('Schools', function($sheet) use($schools) {
                        $data = array();
                        foreach($schools as $school){
                            $data = array_merge($data, array(
                                    array('Name', $school['name']),
                                    array('Diploma', $school['diploma']),
                                    array('Description', $school['description']),
                                    array('Start Date', $school['dateStart']),
                                    array('End Date', $school['dateEnd']),
                                    array('Status', $school['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($experiences) && $request->input('experience')){
                    $excel->sheet('Experiences', function($sheet) use($experiences) {
                        $data = array();
                        foreach($experiences as $experience){
                            $data = array_merge($data, array(
                                    array('Name', $experience['name']),
                                    array('Profession', $experience['profession']),
                                    array('Description', $experience['description']),
                                    array('Start Date', $experience['dateStart']),
                                    array('End Date', $experience['dateEnd']),
                                    array('Status', $experience['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($references) && $request->input('reference')){
                    $excel->sheet('Testimonials', function($sheet) use($references) {
                        $data = array();
                        foreach($references as $reference){
                            $data = array_merge($data, array(
                                    array('Full Name', $reference['fullName']),
                                    array('Enterprise', $reference['enterprise']),
                                    array('Profession', $reference['profession']),
                                    array('Description', $reference['description']),
                                    array('Status', $reference['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($skills) && $request->input('skill')){
                    $excel->sheet('Skills', function($sheet) use($skills) {
                        $data = array();
                        foreach($skills as $skill){
                            $data = array_merge($data, array(
                                    array('Name', $skill['name']),
                                    array('Pourcentage', $skill['pourcentage']),
                                    array('Status', $skill['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($knowledges) && $request->input('knowledge')){
                    $excel->sheet('Knowledges', function($sheet) use($knowledges) {
                        $data = array();
                        foreach($knowledges as $knowledge){
                            $data = array_merge($data, array(
                                    array('Name', $knowledge['name']),
                                    array('Pourcentage', $knowledge['pourcentage']),
                                    array('Status', $knowledge['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($awards) && $request->input('award')){
                    $excel->sheet('Awards', function($sheet) use($awards) {
                        $data = array();
                        foreach($awards as $award){
                            $data = array_merge($data, array(
                                    array('Title', $award['title']),
                                    array('Description', $award['description']),
                                    array('Icon', $award['icon']),
                                    array('Picture', !empty($award['picture']) ? $this->imageTobase64(public_path().'/img/icons/awards/'.$award['picture']) : ''),
                                    array('Status', $award['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($achievements) && $request->input('achievement')){
                    $excel->sheet('Stats', function($sheet) use($achievements) {
                        $data = array();
                        foreach($achievements as $achievement){
                            $data = array_merge($data, array(
                                    array('Title', $achievement['title']),
                                    array('Score', $achievement['score']),
                                    array('Icon', $achievement['icon']),
                                    array('Picture', !empty($achievement['picture']) ? $this->imageTobase64(public_path().'/img/icons/achievements/'.$achievement['picture']) : ''),
                                    array('Status', $achievement['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($socials) && $request->input('social')){
                    $excel->sheet('Socials', function($sheet) use($socials) {
                        $data = array();
                        foreach($socials as $social){
                            $data = array_merge($data, array(
                                    array('Title', $social['title']),
                                    array('Link', $social['link']),
                                    array('Icon', $social['icon']),
                                    array('Status', $social['status']==1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }
                if(!empty($proverb) && $request->input('proverb')){
                    $excel->sheet('Proverbs', function($sheet) use($proverbs) {
                        $sheet->fromArray(array(
                            array('Portfolio', $proverbs['portfolio']),
                            array('Competence', $proverbs['competence']),
                            array('Strength', $proverbs['strength']),
                            array('Resume', $proverbs['resume']),
                            array('Reference', $proverbs['reference']),
                            array('Skill', $proverbs['skill']),
                            array('Knowledge', $proverbs['knowledge']),
                            array('Award', $proverbs['award']),
                            array('Achievement', $proverbs['achievement']),
                            array('Contact', $proverbs['contact']),
                        ), null, 'A1', false);
                    });
                }
                if(!empty($designs) && $request->input('design')){
                    $excel->sheet('Designs', function($sheet) use($designs) {
                        $data = array();
                        foreach($designs as $design){
                            $data = array_merge($data, array(
                                    array('Background 1', $design['background']),
                                    array('Background 2', $design['backgroundDiv']),
                                    array('Background 3', $design['faBackground']),
                                    array('Title', $design['title']),
                                    array('Description', $design['description']),
                                    array('Element', $design['element']),
                                    array('Status', $design['status'] == 1 ? 'active' : 'desactive'),
                                    array('', ''))
                            );
                        }
                        $sheet->fromArray($data, null, 'A1', false);
                    });
                }

            })->download('xlsx');
            return redirect()->back()
                ->with('state', 'Export data succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Export data failed !!!'.'\n'.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Download Empty Excel Template
     *
     * @param AdminExportRequest $request
     * @return \Illuminate\Http\Response
     */
    public function exportEmpty(AdminExportRequest $request)
    {
        try{
            Excel::create('Cv-Informations-empty', function($excel) use($request) {
                $excel->setTitle('Cv-Informations');
                $excel->setTitle('Cv-Informations');
                $excel->setCreator('CurriculumVitae.dev')->setCompany('CurriculumVitae.dev');
                $excel->setDescription('My Cv Informations');

                if($request->input('profile')){
                    $excel->sheet('Profile', function($sheet) use($request){
                            $sheet->fromArray(array(
                                array('First Name',''),
                                array('Last Name',''),
                                array('Email',''),
                                array('Description Brief',''),
                                array('Description Long',''),
                                array('Cover Description',''),
                                array('Job',''),
                                array('Address',''),
                                array('Website',''),
                                array('Phone Number',''),
                            ), null, 'A1', false);
                    });
                }
                $excel->sheet('Portfolios', function($sheet) use($request){
                    if($request->input('portfolio')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Description',''),
                            array('Technologies',''),
                            array('Client',''),
                            array('Testimonial',''),
                            array('Publish Date',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Services', function($sheet) use($request){
                    if($request->input('competence')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Description',''),
                            array('Icon',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Strengths', function($sheet) use($request){
                    if($request->input('strength')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Pourcentage',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Schools', function($sheet) use($request){
                    if($request->input('school')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Diploma',''),
                            array('Description',''),
                            array('Start Date',''),
                            array('End Date',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Experiences', function($sheet) use($request){
                    if($request->input('experience')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Profession',''),
                            array('Description',''),
                            array('Start Date',''),
                            array('End Date',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Testimonials', function($sheet) use($request){
                    if($request->input('reference')){
                        $sheet->fromArray(array(
                            array('Full Name',''),
                            array('Enterprise',''),
                            array('Profession',''),
                            array('Description',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Skills', function($sheet) use($request){
                    if($request->input('skill')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Pourcentage',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Knowledges', function($sheet) use($request){
                    if($request->input('knowledge')){
                        $sheet->fromArray(array(
                            array('Name',''),
                            array('Pourcentage',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Awards', function($sheet) use($request){
                    if($request->input('award')){
                        $sheet->fromArray(array(
                            array('Title',''),
                            array('Description',''),
                            array('Icon',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Stats', function($sheet) use($request){
                    if($request->input('achievement')){
                        $sheet->fromArray(array(
                            array('Title',''),
                            array('Score',''),
                            array('Icon',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                $excel->sheet('Socials', function($sheet) use($request){
                    if($request->input('social')){
                        $sheet->fromArray(array(
                            array('Title',''),
                            array('Link',''),
                            array('Icon',''),
                            array('Status','active'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });
                if($request->input('proverb')){
                    $excel->sheet('Proverbs', function($sheet) use($request){
                            $sheet->fromArray(array(
                                array('Portfolio',''),
                                array('Competence',''),
                                array('Strength',''),
                                array('Resume',''),
                                array('Reference',''),
                                array('Skill',''),
                                array('Knowledge',''),
                                array('Award',''),
                                array('Achievement',''),
                                array('Contact',''),
                                array('', '')
                            ), null, 'A1', false);
                    });
                }
                $excel->sheet('Designs', function($sheet) use($request){
                    if($request->input('design')){
                        $sheet->fromArray(array(
                            array('Background 1', '#'),
                            array('Background 2', '#'),
                            array('Background 3', '#'),
                            array('Title', '#'),
                            array('Description', '#'),
                            array('Element', '#'),
                            array('', '')
                        ), null, 'A1', false);
                    }
                });

            })->download('xlsx');
            return redirect()->back()
                ->with('state', 'Get empty file succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Get empty file failed !!!'.'\n'.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Import User information from Excel File
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $profile = Profile::where('user_id','=',Auth::user()->id)->first();
        $proverb = Proverb::where('user_id','=',Auth::user()->id)->first();

        try{
            if(!empty($request->file("importExcel"))){
                $infoXlsx = 'Cv-Informations-'.$profile['id'].'.xlsx';
                $request->file("importExcel")->move(
                    base_path().'/public/excel/', $infoXlsx
                );
            }else
                return redirect()->route('admin-home')
                    ->with('state', 'Excel file required !!!')
                    ->with('type', 'error');

            Excel::load('public/excel/Cv-Informations-'.$profile['id'].'.xlsx', function($reader)
            use($profile, $proverb){
                DB::beginTransaction();
                foreach ($reader->all() as $sheet) {
                    if($sheet->getTitle() == "Profile") {
                        for($i=0; $i<10; $i++){
                            switch($sheet[$i][0]){
                                case "First Name" :
                                    $profile['firstName'] = $sheet[$i][1];
                                    break;
                                case "Last Name" :
                                    $profile['lastName'] = $sheet[$i][1];
                                    break;
                                case "Email" :
                                    $profile['email'] = $sheet[$i][1];
                                    break;
                                case "Description Brief" :
                                    $profile['descBrief'] = $sheet[$i][1];
                                    break;
                                case "Description Long" :
                                    $profile['descLong'] = $sheet[$i][1];
                                    break;
                                case "Cover Description" :
                                    $profile['coverDesc'] = $sheet[$i][1];
                                    break;
                                case "Job" :
                                    $profile['job'] = $sheet[$i][1];
                                    break;
                                case "Address" :
                                    $profile['address'] = $sheet[$i][1];
                                    break;
                                case "Website" :
                                    $profile['website'] = $sheet[$i][1];
                                    break;
                                case "Phone Number" :
                                    $profile['phone'] = $sheet[$i][1];
                                    break;
                            }
                        }
                        $profile->update();
                    }
                    elseif ($sheet->getTitle() == "Portfolios"){
                        for($i=0; $i<count($sheet); $i+=8){
                            $portfolio = new Portfolio();
                            for($j=$i; $j<$i+7; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $portfolio["name"] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $portfolio["description"] = $sheet[$j][1];
                                        break;
                                    case "Technologies":
                                        $portfolio["technologies"] = $sheet[$j][1];
                                        break;
                                    case "Client":
                                        $portfolio["Client"] = $sheet[$j][1];
                                        break;
                                    case "Testimonial":
                                        $portfolio["testimonial"] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                        break;
                                    case "Publish Date":
                                        $date = new DateTime($sheet[$j][1]);
                                        $portfolio["datePublish"] = $date->format('m/d/Y');
                                        break;
                                    case "Status":
                                        $portfolio["status"] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $portfolio["user_id"] = Auth::user()->id;
                            $portfolio->save();

                            $media = New Media();
                            $imagePath = md5(Carbon::now()->toDateTimeString()).$portfolio['id'].'-portfolio.jpg';
                            copy ( base_path().'/public/img/portfolio/projet.png' , base_path().'/public/img/portfolio/'.$imagePath);
                            $media['position'] = 1;
                            $media['path'] = $imagePath;
                            $media['portfolio_id'] = $portfolio["id"];
                            $media->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Services"){
                        for($i=0; $i<count($sheet); $i+=6){
                            $competence = new Competence();
                            for($j=$i; $j<$i+5; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $competence['name'] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $competence['description'] = $sheet[$j][1];
                                        break;
                                    case "Icon":
                                        $competence['icon'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : '';
                                        break;
                                    case "Status":
                                        $competence['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                    case "Picture":
                                        if(!empty($sheet[$j][1])){
                                            $competence['user_id'] = Auth::user()->id;
                                            $competence->save();
                                            if($this->base64ToImage($sheet[$j][1], 'competence', $competence['id'])){
                                                $competence['picture'] = $competence['id'].'.png';
                                            }
                                        }
                                        break;
                                }
                            }
                            $competence['user_id'] = Auth::user()->id;
                            $competence->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Strengths"){
                        for($i=0; $i<count($sheet); $i+=4){
                            $strength = new Strength();
                            for($j=$i; $j<$i+3; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $strength['name'] = $sheet[$j][1];
                                        break;
                                    case "Pourcentage":
                                        $strength['pourcentage'] = $sheet[$j][1] >= 0 && $sheet[$j][1] <= 100 && !empty($sheet[$j][1]) ? $sheet[$j][1] : 50 ;
                                        break;
                                    case "Status":
                                        $strength['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $strength['user_id'] = Auth::user()->id;
                            $strength->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Schools") {
                        for($i=0; $i<count($sheet); $i+=7){
                            $school = new School();
                            for($j=$i; $j<$i+6; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $school['name'] = $sheet[$j][1];
                                        break;
                                    case "Diploma":
                                        $school['diploma'] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $school['description'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : '';
                                        break;
                                    case "Start Date":
                                        $school['dateStart'] = $sheet[$j][1];$sheet[$j][1];
                                        break;
                                    case "End Date":
                                        $school['dateEnd'] = $sheet[$j][1] < $sheet[$j][1] ? $sheet[$j][1] : $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $school['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $school['user_id'] = Auth::user()->id;
                            $school->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Experiences"){
                        for($i=0; $i<count($sheet); $i+=7){
                            $experience = new Experience();
                            for($j=$i; $j<$i+6; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $experience['name'] = $sheet[$j][1];
                                        break;
                                    case "Profession":
                                        $experience['profession'] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $experience['description'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : '';
                                        break;
                                    case "Start Date":
                                        $experience['dateStart'] = $sheet[$j][1];$sheet[$j][1];
                                        break;
                                    case "End Date":
                                        $experience['dateEnd'] = $sheet[$j][1] < $sheet[$j][1] ? $sheet[$j][1] : $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $experience['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $experience['user_id'] = Auth::user()->id;
                            $experience->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Testimonials"){
                        for($i=0; $i<count($sheet); $i+=6){
                            $reference = new Reference();
                            for($j=$i; $j<$i+5; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Full Name":
                                        $reference['fullName'] = $sheet[$j][1];
                                        break;
                                    case "Enterprise":
                                        $reference['enterprise'] = $sheet[$j][1];
                                        break;
                                    case "Profession":
                                        $reference['profession'] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $reference['description'] = $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $reference['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $reference['user_id'] = Auth::user()->id;
                            $reference->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Skills") {
                        for($i=0; $i<count($sheet); $i+=4){
                            $skill = new Skill();
                            for($j=$i; $j<$i+3; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $skill['name'] = $sheet[$j][1];
                                        break;
                                    case "Pourcentage":
                                        $skill['pourcentage'] = $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $skill['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $skill['user_id'] = Auth::user()->id;
                            $skill->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Knowledges"){
                        for($i=0; $i<count($sheet); $i+=4){
                            $knowledge = new Knowledge();
                            for($j=$i; $j<$i+3; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Name":
                                        $knowledge['name'] = $sheet[$j][1];
                                        break;
                                    case "Pourcentage":
                                        $knowledge['pourcentage'] = $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $knowledge['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $knowledge['user_id'] = Auth::user()->id;
                            $knowledge->save();
                        }
                    }
                    elseif($sheet->getTitle() == "Awards") {
                        for($i=0; $i<count($sheet); $i+=6){
                            echo count($sheet).'<br>';
                            $award = new Award();
                            for($j=$i; $j<$i+5; $j+=1){
                                echo 'j='.$j.'<br>';
                                echo  $sheet[$j].'<br>';
                                switch($sheet[$j][0]){
                                    case "Title":
                                        $award['title'] = $sheet[$j][1];
                                        break;
                                    case "Description":
                                        $award['description'] = $sheet[$j][1];
                                        break;
                                    case "Icon":
                                        $award['icon'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : '';
                                        break;
                                    case "Status":
                                        $award['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                    case "Picture":
                                        if(!empty($sheet[$j][1])){
                                            $award['user_id'] = Auth::user()->id;
                                            $award->save();
                                            if($this->base64ToImage($sheet[$j][1], 'award', $award['id'])){
                                                $award['picture'] = $award['id'].'.png';
                                            }
                                        }
                                        break;
                                }
                            }
                            $award['user_id'] = Auth::user()->id;
                            $award->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Stats"){
                        for($i=0; $i<count($sheet); $i+=6){
                            $achievement = new Achievement();
                            for($j=$i; $j<$i+5; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Title":
                                        $achievement['title'] = $sheet[$j][1];
                                        break;
                                    case "Score":
                                        $achievement['score'] = $sheet[$j][1];
                                        break;
                                    case "Icon":
                                        $achievement['icon'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : '';
                                        break;
                                    case "Status":
                                        $achievement['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                    case "Picture":
                                        if(!empty($sheet[$j][1])){
                                            $achievement['user_id'] = Auth::user()->id;
                                            $achievement->save();
                                            if($this->base64ToImage($sheet[$j][1], 'achievement', $achievement['id'])){
                                                $achievement['picture'] = $achievement['id'].'.png';
                                            }
                                        }
                                        break;
                                }
                            }
                            $achievement['user_id'] = Auth::user()->id;
                            $achievement->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Socials") {
                        for($i=0; $i<count($sheet); $i+=5){
                            $social = new Social();
                            for($j=$i; $j<$i+4; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Title":
                                        $social['title'] = $sheet[$j][1];
                                        break;
                                    case "Link":
                                        $social['link'] = $sheet[$j][1];
                                        break;
                                    case "Icon":
                                        $social['icon'] = $sheet[$j][1];
                                        break;
                                    case "Status":
                                        $social['status'] = $sheet[$j][1] == 'active' ? 1 : 0;
                                        break;
                                }
                            }
                            $social['user_id'] = Auth::user()->id;
                            $social->save();
                        }
                    }
                    elseif ($sheet->getTitle() == "Proverbs") {
                        for($j=0; $j<10; $j+=1){
                            switch($sheet[$j][0]){
                                case "Portfolio":
                                    $proverb['portfolio'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Competence":
                                    $proverb['competence'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Strength":
                                    $proverb['strength'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Resume":
                                    $proverb['resume'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Reference":
                                    $proverb['reference'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Skill":
                                    $proverb['skill'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Knowledge":
                                    $proverb['knowledge'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Award":
                                    $proverb['award'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Achievement":
                                    $proverb['achievement'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                                case "Contact":
                                    $proverb['contact'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "";
                                    break;
                            }
                        }
                        $proverb->update();
                    }
                    elseif ($sheet->getTitle() == "Designs") {
                        for($i=0; $i<count($sheet); $i+=8){
                            $design = new Design();
                            for($j=$i; $j<$i+7; $j+=1){
                                switch($sheet[$j][0]){
                                    case "Background 1":
                                        $design['background'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#000000";
                                        break;
                                    case "Background 2":
                                        $design['backgroundDiv'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#000000";
                                        break;
                                    case "Background 3":
                                        $design['faBackground'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#000000";
                                        break;
                                    case "Title":
                                        $design['title'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#ffffff";
                                        break;
                                    case "Description":
                                        $design['description'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#ffffff";
                                        break;
                                    case "Element":
                                        $design['element'] = !empty($sheet[$j][1]) ? $sheet[$j][1] : "#000000";
                                        break;
                                }
                            }
                            $design['user_id'] = Auth::user()->id;
                            $design->save();
                        }
                    }
                }
            })->get();
            DB::commit();
            return redirect()->back()
                ->with('state', 'Import Succeeded')
                ->with('type', 'success');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()
                ->with('state', 'Import Failed '.'\n'.'please contact admin : '.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Converter Picture to base 64
     *
     * @return \Illuminate\Http\Response
     */
    public function imageTobase64($path){
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    /**
     * Converter base 64 to Picture
     *
     * @param $base64String
     * @param $category
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function base64ToImage($base64String, $category, $id){
        try{
            switch ($category){
                case "competence" :
                    $imageName = $id . '.png';
                    Image::make($base64String)->resize(config('cvrules.icon-width-competence'),config('cvrules.icon-height-competence'))->save('img/icons/competences/'.$imageName);
                    return true;
                    break;
                case "award" :
                    $imageName = $id . '.png';
                    Image::make($base64String)->resize(config('cvrules.icon-width-award'),config('cvrules.icon-height-award'))->save('img/icons/awards/'.$imageName);
                    return true;
                    break;
                case "achievement" :
                    $imageName = $id . '.png';
                    Image::make($base64String)->resize(config('cvrules.icon-width-achievement'),config('cvrules.icon-height-achievement'))->save('img/icons/achievements/'.$imageName);
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
        }catch (Exception $e){
            return false;
        }
    }

}