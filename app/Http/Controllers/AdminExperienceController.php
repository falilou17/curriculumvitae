<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminExperienceRequest;
use App\Models\Experience;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $experiences = Auth::user()->experiences()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/experience', compact('home', 'experiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/experience');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdminExperienceRequest $request)
    {
        $newExperience = new Experience();
        $newExperience->fill($request->all());
        $newExperience['description'] = $request->input('description');
        $newExperience->save();

        return redirect()->route('admin-experiences');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $experience = Experience::find($id);
        if(is_null($experience) || $experience['user_id'] != Auth::user()->id)
            return redirect()->route('admin-home')
                ->with('state', 'The Experience does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/experience', compact('experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminExperienceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminExperienceRequest $request, $id)
    {
        try{
            $experience = Experience::find($id);
            if(is_null($experience) || $experience['user_id'] != Auth::user()->id)
                return redirect()->route('admin-experiences')
                    ->with('state', 'The Experience does not exist or is not your !!!')
                    ->with('type', 'error');
            $experience->fill($request->all());
            $experience['description'] = $request->input('description');
            $experience->update();
            return redirect()->route('admin-experiences')
                ->with('state', 'Update Experience Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Experience Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $experience = Experience::find($id);
            if(is_null($experience) || $experience['user_id'] != Auth::user()->id)
                return redirect()->route('admin-experiences')
                    ->with('state', 'The Experience does not exist or is not your !!!')
                    ->with('type', 'error');
            $experience->delete();
            return redirect()->back()
                ->with('state', 'Delete Experience Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Experience Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $experience = Experience::find($id);
            if(is_null($experience) || $experience['user_id'] != Auth::user()->id)
                return redirect()->route('admin-experiences')
                    ->with('state', 'The Experience does not exist or is not your !!!')
                    ->with('type', 'error');
            $experience->status = 1;
            $experience->update();
            return redirect()->back()
                ->with('state', 'Active Experience succeeded')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Experience Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $home="home";
            $experience = Experience::find($id);
            if(is_null($experience) || $experience['user_id'] != Auth::user()->id)
                return redirect()->route('admin-experiences')
                    ->with('state', 'The Experience does not exist or is not your !!!')
                    ->with('type', 'error');
            $experience->status = 0;
            $experience->update();
            return redirect()->back()
                ->with('state', 'Deactive Experience Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Experience Failed')
                ->with('type', 'error');
        }
    }
}
