<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminUserRequest;
use App\Http\Requests\AdminUserUpdateRequest;
use App\Models\Achievement;
use App\Models\Award;
use App\Models\Competence;
use App\Models\Design;
use App\Models\Experience;
use App\Models\Knowledge;
use App\Models\Portfolio;
use App\Models\Proverb;
use App\Models\Profile;
use App\Models\Reference;
use App\Models\School;
use App\Models\Skill;
use App\Models\Social;
use App\Models\Strength;
use App\Models\User;
use App\Models\Wallpaper;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $users = null;
        if(Auth::user()->id==1)
            $users = User::paginate(config('cvrules.back-pagination'));
        else
            $users = User::where('createdBy',Auth::user()->id)->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/user', compact('home', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserRequest $request)
    {
        DB::beginTransaction();
        try{
            $user = new User();
            $user->fill($request->all());
            $user['createdBy'] = Auth::user()->id;
            $user['password'] = bcrypt($request->input('password'));
            if($request->input('isAdmin'))
                $user['isAdmin'] = 1;
            $user->save();

            $profile = new Profile();
            $profile['coverPicture'] = 'coverPicture.jpg';
            $profile['profilePicture'] = 'profilePicture.jpg';
            $profile['user_id'] = $user['id'];
            $profile->save();

            $design = new Design();
            $design['background'] = '#0d0c0d';
            $design['backgroundDiv'] = '#171717';
            $design['faBackground'] = '#2e2e2e';
            $design['title'] = '#ffffff';
            $design['description'] = '#a1a1a1';
            $design['element'] = '#c80a48';
            $design['status'] = '1';
            $design['user_id'] = $user['id'];
            $design->save();

            $proverb = new Proverb();
            $proverb['user_id'] = $user['id'];
            $proverb->save();

            $wallpaper = new Wallpaper();
            $wallpaper['user_id'] = $user['id'];
            $wallpaper->save();

            DB::commit();
            return redirect()->route('admin-users')
                ->with('state', 'Create User Succeed !!!')
                ->with('type', 'success');

        }catch(Exception $e){
            DB::rollback();
            return redirect()->route('admin-users')
                ->with('state', 'Create User Failed !!! \n'.$e->getMessage())
                ->with('type', 'error');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(is_null($user))
            return redirect()->route('admin-users')
                ->with('state', 'The resource does not exist !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/user', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminUserUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUserUpdateRequest $request, $id)
    {
        try{
            $user = User::find($id);
            if(is_null($user))
                return redirect()->route('admin-users')
                    ->with('state', 'The resource does not exist !!!')
                    ->with('type', 'error');

            $user['login'] = $request->input('login');
            $user['domain'] = $request->input('domain');
            if(!empty($request->input('password')) && $request->input('password') != "")
                $user['password'] = bcrypt($request->input('password'));
            $user['isAdmin'] = !empty($request->input('isAdmin')) ? 1 : 0;

            $user->update();

            return redirect()->route('admin-users')
                ->with('state', 'Update User Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update User Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Deactivate a the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        $user = User::find($id);
        if(is_null($user))
            return redirect()->route('admin-users')
                ->with('state', 'The resource does not exist !!!')
                ->with('type', 'error');
        elseif(Auth::user()->id != 1){
            if($user['createdBy'] != Auth::user()->id)
                return redirect()->route('admin-users')
                    ->with('state', 'You can\'t disable this account !!!')
                    ->with('type', 'error');
        }elseif($user->id == 1)
            return redirect()->route('admin-users')
                ->with('state', 'You can\'t disable the Super Admin account !!!')
                ->with('type', 'error');
        $user->isActive = false;
        $user->update();

        return redirect()->route('admin-users')
            ->with('state', 'Deactivation Succeed')
            ->with('type', 'success');
    }

    /**
     * Deactivate a the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $user = User::find($id);
        if(is_null($user))
            return redirect()->route('admin-users')
                ->with('state', 'The resource does not exist !!!')
                ->with('type', 'error');
        elseif(Auth::user()->id != 1){
            if($user['createdBy'] != Auth::user()->id)
                return redirect()->route('admin-users')
                    ->with('state', 'You can\'t delete this account !!!')
                    ->with('type', 'error');
        }
        $user->isActive = true;
        $user->update();

        return redirect()->route('admin-users')
            ->with('state', 'Activation Succeed')
            ->with('type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $user = User::find($id);
            if(is_null($user))
                return redirect()->route('admin-users')
                    ->with('state', 'The user does not exist !!!')
                    ->with('type', 'error');
            elseif(Auth::user()->id != 1){
                if($user['createdBy'] != Auth::user()->id)
                    return redirect()->route('admin-users')
                        ->with('state', 'You can\'t delete this account !!!')
                        ->with('type', 'error');
            }
            elseif($user['id'] == Auth::user()->id)
                return redirect()->route('admin-users')
                    ->with('state', 'You can\'t delete your account !!!')
                    ->with('type', 'error');
            elseif($user['id'] == 1)
                return redirect()->route('admin-users')
                    ->with('state', 'You can\'t delete the Super Admin !!!')
                    ->with('type', 'error');


            $profile = Profile::where('user_id','=',$id)->first();
            $portfolios = Portfolio::where('user_id','=',$id)->get();
            $competences = Competence::where('user_id','=',$id)->get();
            $strengths = Strength::where('user_id','=',$id)->get();
            $schools = School::where('user_id','=',$id)->get();
            $experiences = Experience::where('user_id','=',$id)->get();
            $references = Reference::where('user_id','=',$id)->get();
            $skills = Skill::where('user_id','=',$id)->get();
            $knowledges = Knowledge::where('user_id','=',$id)->get();
            $awards = Award::where('user_id','=',$id)->get();
            $achievements = Achievement::where('user_id','=',$id)->get();
            $socials = Social::where('user_id','=',$id)->get();
            $designs = Design::where('user_id','=',$id)->get();
            $proverbs = Proverb::where('user_id','=',$id)->first();
            $wallpaper = Wallpaper::where('user_id','=',$id)->first();

            foreach($strengths as $strength){
                $strength->delete();
            }
            foreach($schools as $school){
                $school->delete();
            }
            foreach($experiences as $experience){
                $experience->delete();
            }
            foreach($skills as $skill){
                $skill->delete();
            }
            foreach($knowledges as $knowledge){
                $knowledge->delete();
            }
            foreach($socials as $social){
                $social->delete();
            }
            foreach($designs as $design){
                $design->delete();
            }

            $proverbs->delete();

            foreach($competences as $competence){
                if(!empty($competence->picture) && File::exists(public_path().'/img/icons/competences/'.$competence->picture))
                    unlink(public_path().'/img/icons/competences/'.$competence->picture);
                $competence->delete();
            }
            foreach($awards as $award){
                if(!empty($award->picture) && File::exists(public_path().'/img/icons/awards/'.$award->picture))
                    unlink(public_path().'/img/icons/awards/'.$award->picture);
                $award->delete();
            }
            foreach($achievements as $achievement){
                if(!empty($achievement->picture) && File::exists(public_path().'/img/icons/achievements/'.$achievement->picture))
                    unlink(public_path().'/img/icons/achievements/'.$achievement->picture);
                $achievement->delete();
            }

            if($profile->profilePicture != "profilePicture.jpg")
                unlink(base_path().'/public/img/'.$profile->profilePicture);

            if($profile->coverPicture != "coverPicture.jpg")
                unlink(base_path().'/public/img/'.$profile->coverPicture);
            $profile->delete();

            foreach($references as $reference){
                if($reference->picture != "reference.png")
                    unlink(base_path().'/public/img/reference/'.$reference->picture);
                $reference->delete();
            }
            foreach($portfolios as $portfolio){
                foreach($portfolio->medias as $image) {
                    unlink(base_path().'/public/img/portfolio/'.$image->path);
                    $image->delete();
                }
                $portfolio->delete();
            }
            if($wallpaper->strength != 'strength-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/strengths/'.$strength->picture)){
                unlink(public_path().'/img/wallpapers/strengths/'.$strength->picture);
            }
            if($wallpaper->reference != 'reference-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/references/'.$reference->picture)){
                unlink(public_path().'/img/wallpapers/references/'.$reference->picture);
            }
            if($wallpaper->achievement != 'achievement-wallpaper.jpg' && File::exists(public_path().'/img/wallpapers/achievements/'.$achievement->picture)){
                unlink(public_path().'/img/wallpapers/achievements/'.$achievement->picture);
            }
            $wallpaper->delete();

            $user->delete();


            DB::commit();
            return redirect()->back()
                ->with('state', 'Delete User Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()
                ->with('state', 'Delete User Failed \n'.$e->getMessage())
                ->with('type', 'error');
        }
    }
}
