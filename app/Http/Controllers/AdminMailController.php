<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminMailRequest;
use App\Models\Mail;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

class AdminMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $page
     * @return \Illuminate\Http\Response
     */
    public function index($page = null)
    {
        if (empty($page) || ($page != 'Inbox' && $page != 'Send'))
            $page = 'Inbox';
        switch ($page) {
            case 'Inbox':
                $mails = Mail::where('receiver', '=', 'me')->where('user_id', '=', Auth::user()->id)->paginate(config('cvrules.back-pagination'));
                break;
            case 'Send':
                $mails = Mail::where('sender', '=', 'me')->where('user_id', '=', Auth::user()->id)->paginate(config('cvrules.back-pagination'));
                break;
        }
        $home = $page;
        return view('admin/pages/mail', compact('home', 'mails'));
    }

    /**
     * Show a specific mail.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mail = Mail::where('id', '=', $id)->where('user_id', '=', Auth::user()->id)->first();
        if (!empty($mail)) {
            $mail['read'] = $mail['read'] == 0 ? 1 : $mail['read'];
            $mail->update();
            return view('admin/pages/mail', compact('mail'));
        }
        return redirect()->route('admin-mails', 'Inbox')
            ->with('state', 'The Mail does not exist or is not your !!!')
            ->with('type', 'error');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/mail');
    }

    /**
     * Send a newly created resource .
     *
     * @param AdminMailRequest $request
     * @return \Illuminate\Http\Response
     */
    public function send(AdminMailRequest $request)
    {
        try {
            $mail = new Mail();
            $mail->fill($request->input());
            $mail->save();
            \Illuminate\Support\Facades\Mail::send('mail.sendMail', ['mail' => $mail],
                function ($message) use ($mail, $request){
                    $message->from($mail->sender, $request->name);
                    $message->to($mail->receiver);
                    $message->subject($mail->object);
                });
            if($request->has('ajax'))
                return new Response(['success' => 1]);
            return redirect()->back()
                ->with('state', 'Send Mail Succeed !!!')
                ->with('type', 'success');
        } catch (Exception $e) {
            if($request->has('ajax'))
                return new Response(['success' => 0, 'error' => $e->getMessage()]);
            return redirect()->back()
                ->with('state', 'Send Mail Failed !!!')
                ->with('type', 'error');
        }
    }

    /**
     * Receive a newly created resource .
     *
     * @param AdminMailRequest $request
     * @return \Illuminate\Http\Response
     */
    public function receive(AdminMailRequest $request)
    {
        try {
            $mail = new Mail();
            $mail->fill($request->input());
            $mail['receiver'] = 'me';
            $mail['read'] = 0;
            $mail['status'] = 1;
            $mail->save();
            return redirect()->back()
                ->with('state', 'Send Mail Succeed !!!')
                ->with('type', 'success');
        } catch (Exception $e) {
            return redirect()->back()
                ->with('state', 'Send Mail Failed !!!')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mail = Mail::find($id);
            if (is_null($mail) || $mail['user_id'] != Auth::user()->id)
                return redirect()->route('admin-mails')
                    ->with('state', 'The Mail does not exist or is not your !!!')
                    ->with('type', 'error');
            $mail->delete();
            return redirect()->route('admin-mails', 'Inbox')
                ->with('state', 'Delete Mail Succeed')
                ->with('type', 'success');
        } catch (Exception $e) {
            return redirect()->back()
                ->with('state', 'Delete Mail Failed')
                ->with('type', 'error');
        }
    }

    /**
     * mark read a specified mail.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        try {
            $mail = Mail::find($id);
            if (is_null($mail) || $mail['user_id'] != Auth::user()->id)
                return redirect()->route('admin-mails')
                    ->with('state', 'The Mail does not exist or is not your !!!')
                    ->with('type', 'error');
            $mail->read = 1;
            $mail->update();
            return redirect()->back()
                ->with('state', 'Mark as read succeeded')
                ->with('type', 'success');
        } catch (Exception $e) {
            return redirect()->back()
                ->with('state', 'Mark as read Failed')
                ->with('type', 'error');
        }
    }

    /**
     * mark unread a specified mail.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function unread($id)
    {
        try {
            $mail = Mail::find($id);
            if (is_null($mail) || $mail['user_id'] != Auth::user()->id)
                return redirect()->route('admin-mails')
                    ->with('state', 'The Mail does not exist or is not your !!!')
                    ->with('type', 'error');
            $mail->read = 0;
            $mail->update();
            return redirect()->back()
                ->with('state', 'Mark as unread Succeed')
                ->with('type', 'success');
        } catch (Exception $e) {
            return redirect()->back()
                ->with('state', 'Mark as unread Failed')
                ->with('type', 'error');
        }
    }
}
