<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminSocialRequest;
use App\Models\Social;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminSocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home="home";
        $socials = Auth::user()->socials()->paginate(config('cvrules.back-pagination'));
        return view('admin/pages/social', compact('home', 'socials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/social');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminSocialRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSocialRequest $request)
    {
        $newSocial = new Social();
        $newSocial->fill($request->all());
        $newSocial->save();

        return redirect()->route('admin-socials');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = Social::find($id);
        if(is_null($social) || $social['user_id'] != Auth::user()->id)
            return redirect()->route('admin-socials')
                ->with('state', 'The resource does not exist or is not your !!!')
                ->with('type', 'error');
        else
            return view('admin/pages/social', compact('social'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminSocialRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSocialRequest $request, $id)
    {
        try{
            $social = Social::find($id);
            if(is_null($social) || $social['user_id'] != Auth::user()->id)
                return redirect()->route('admin-socials')
                    ->with('state', 'The resource does not exist or is not your !!!')
                    ->with('type', 'error');
            $social->fill($request->all());
            $social->update();
            return redirect()->route('admin-socials')
                ->with('state', 'Update Social Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Update Social Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $home="home";
            $social = Social::find($id);
            if(is_null($social) || $social['user_id'] != Auth::user()->id)
                return redirect()->route('admin-socials')
                    ->with('state', 'The resource does not exist or is not your !!!')
                    ->with('type', 'error');
            $social->delete();
            $socials = Auth::user()->socials()->get();
            return redirect()->back()
                ->with('state', 'Delete Social Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Delete Social Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try{
            $social = Social::find($id);
            if(is_null($social) || $social['user_id'] != Auth::user()->id)
                return redirect()->route('admin-socials')
                    ->with('state', 'The resource does not exist or is not your !!!')
                    ->with('type', 'error');
            $social->status = 1;
            $social->update();
            return redirect()->back()
                ->with('state', 'Active Social Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Active Social Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Disactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        try{
            $social = Social::find($id);
            if(is_null($social) || $social['user_id'] != Auth::user()->id)
                return redirect()->route('admin-socials')
                    ->with('state', 'The resource does not exist or is not your !!!')
                    ->with('type', 'error');
            $social->status = 0;
            $social->update();
            return redirect()->back()
                ->with('state', 'Deactive Social Succeed')
                ->with('type', 'success');
        }catch(Exception $e){
            return redirect()->back()
                ->with('state', 'Deactive Social Failed')
                ->with('type', 'error');
        }
    }
}
