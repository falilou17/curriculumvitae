<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminProfileJobRequest;
use App\Http\Requests\AdminProfileMeRequest;
use App\Http\Requests\AdminProfileContactRequest;
use App\Http\Requests\AdminProfilePictureRequest;
use App\Http\Requests\AdminProfileSettingRequest;
use App\Models\Profile;
use App\Models\Template;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\VarDumper\VarDumper;

class AdminProfileController extends Controller
{

    /**
     * Display a form for personnal information.
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        $page = "Me";
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-contact')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/profile',compact('page','myProfile'));
    }

    /**
     * Update personnal information.
     *
     * @param AdminProfileMeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update_me(AdminProfileMeRequest $request)
    {
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-contact')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        try {
            $myProfile->fill($request->all());
            $myProfile->update();
            return redirect()->back()
                ->with('state', 'Update Personnal Informations Succeed')
                ->with('type', 'success');
        }catch(\Exception $e){
            return redirect()->back()
                ->with('state', 'Update Personnal Informations Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $page = "Contact";
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-contact')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/profile', compact('page', 'myProfile'));
    }

    /**
     * Update personnal information.
     *
     * @param AdminProfileContactRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update_contact(AdminProfileContactRequest $request)
    {
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-contact')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
//        try {
            $myProfile->fill($request->all());
            $myProfile->update();
            return redirect()->back()
                ->with('state', 'Update Contact Informations Succeed')
                ->with('type', 'success');
//        }catch(\Exception $e){
//            return redirect()->back()
//                ->with('state', 'Update Contact Informations Failed')
//                ->with('type', 'error');
//        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myjob()
    {
        $page = "Job";
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-myjob')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/profile', compact('page', 'myProfile'));
    }

    /**
     * Update professional information.
     *
     * @param AdminProfileJobRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update_myjob(AdminProfileJobRequest $request)
    {
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-myjob')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        try {
            $myProfile['job'] = $request->input("job");

            if ($request->file('cv')) {
                if(!empty($myProfile->cv) && File::exists(public_path().'/img/cv/'.$myProfile["cv"]))
                    unlink(public_path().'/img/cv/'.$myProfile->cv);
                $imageName = $myProfile->id . '-cv.' . $request->file('cv')->getClientOriginalExtension();
                Image::make($request->file('cv'))->save('img/cv/'.$imageName);
                $myProfile['cv'] = $imageName;
            }

            $myProfile->update();
            return redirect()->back()
                ->with('state', 'Update Professional Informations Succeed')
                ->with('type', 'success');
        }catch(\Exception $e){
            return redirect()->back()
                ->with('state', 'Update Professional Informations Failed')
                ->with('type', 'error');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletecv()
    {
        try {
            $myProfile = Auth::user()->profile()->first();
            if(!empty($myProfile)){
               if(!empty($myProfile["cv"]) && File::exists(public_path().'/img/cv/'.$myProfile["cv"])){
                   unlink(public_path().'/img/cv/'.$myProfile["cv"]);
                   $myProfile["cv"]=null;
                   $myProfile->update();
                   return redirect()->route('admin-profile-myjob')
                       ->with('state', 'Delete Cv Succeed !!!')
                       ->with('type', 'success');
               }
                else
                    return redirect()->route('admin-profile-myjob')
                        ->with('state', 'Delete Cv Failed !!!')
                        ->with('type', 'error');

            }
            return redirect()->route('admin-profile-myjob')
                ->with('state', 'Delete Cv Failed !!!')
                ->with('type', 'error');
        }
        catch(\Exception $e){
            return redirect()->route('admin-profile-myjob')
                ->with('state', 'Delete Cv Failed !!!')
                ->with('type', 'error');

        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function picture()
    {
        $page = "Picture";
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-picture')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        return view('admin/pages/profile', compact('page', 'myProfile'));
    }

    public function update_picture(AdminProfilePictureRequest $request)
    {
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-picture')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        try {
            if ($request->file('profilePicture')) {
                if($myProfile->profilePicture != "profilePicture.jpg" && File::exists(public_path().'/img/'.$myProfile->profilePicture))
                    unlink(base_path().'/public/img/'.$myProfile->profilePicture);
                $imageName = $myProfile->id . '-profilePicture.' . $request->file('profilePicture')->getClientOriginalExtension();
                Image::make($request->file('profilePicture'))->resize(config('cvrules.picture-width'), null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save('img/'.$imageName);
                $myProfile['profilePicture'] = $imageName;
            }
            if (!empty($request->file('coverPicture'))) {
                foreach (glob(base_path().'/public/img/*-coverPicture.*') as $cp)
                    unlink($cp);

                foreach ($request->file('coverPicture') as $key => $cp){
                    $imageName = $key . '-coverPicture.png'; //. $request->file('coverPicture')[$key]->getClientOriginalExtension();
                    Image::make($request->file('coverPicture')[$key])->resize(config('cvrules.cover-width'), null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save('img/'.$imageName);
                }
                $myProfile['coverPicture'] = $imageName;
            }

            $myProfile->update();
            return redirect()->back()
                ->with('state', 'Update Pictures Succeed')
                ->with('type', 'success');
        }catch(\Exception $e){
            return redirect()->back()
                ->with('state', 'Update Pictures Failed : '.$e->getMessage())
                ->with('type', 'error');
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        $page = "Setting";
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-setting')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
        $templates = Template::all();
        return view('admin/pages/profile', compact('page', 'myProfile', 'templates'));
    }

    /**
     * Update personnal information.
     *
     * @param AdminProfileMeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update_setting(AdminProfileSettingRequest $request)
    {
        $myProfile = Auth::user()->profile()->first();
        if(is_null($myProfile))
            return redirect()->route('admin-profile-setting')
                ->with('state', 'The profile does not exist or is not your !!!')
                ->with('type', 'error');
//        try {
            $myProfile->fill($request->all());
            $myProfile->update();
            return redirect()->back()
                ->with('state', 'Update Settings Succeed')
                ->with('type', 'success');
//        }catch(\Exception $e){
//            return redirect()->back()
//                ->with('state', 'Update Settings Failed')
//                ->with('type', 'error');
//        }
    }
}

