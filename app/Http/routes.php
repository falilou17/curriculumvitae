<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Common Users Page
//Route::get('/',['as'=>'admin-index','uses'=>'AdminController@index']);
Route::get('/',['as'=>'index','uses'=>'indexController@index']);
//Route::get('/',['as'=>'index', function(){
//    return view('errors.503');
//}]);
Route::get('/portfolio/{id}',['as'=>'portfolio-show','uses'=>'indexController@portfolio']);
Route::get('/login',['as'=>'admin-login','uses'=>'AdminController@index']);
Route::post('/login',['uses'=>'AdminController@login']);
Route::get('/portfolio/{id}',['as'=>'portfolio-show','uses'=>'indexController@portfolio']);
Route::post('/mail',['as'=>'mail','uses'=>'AdminMailController@send']);


// Administrator Routes
Route::group(['prefix'=>'admin', 'middleware'=>'auth'],function(){
    Route::get('/',['as'=>'admin-home','uses'=>'AdminController@home']);
    Route::get('/logout',['as'=>'admin-logout','uses'=>'AdminController@logout']);
    Route::get('/edit',['as'=>'admin-edit','uses'=>'AdminController@edit']);
    Route::post('/update',['as'=>'admin-update','uses'=>'AdminController@update']);
    Route::get('/desactive',['as'=>'desactive-all','uses'=>'AdminController@desactiveAll']);
    Route::get('/active',['as'=>'active-all','uses'=>'AdminController@activeAll']);
    Route::get('/reset',['as'=>'reset','uses'=>'AdminController@reset']);
    Route::post('/export/excel',['as'=>'export-excel','uses'=>'ExcelController@exportXLSX']);
    Route::post('/export/excel/empty',['as'=>'export-excel-empty','uses'=>'ExcelController@exportEmpty']);
    Route::post('/import/excel',['as'=>'import-excel','uses'=>'ExcelController@import']);

    Route::group(['prefix'=>'profile'],function(){
        Route::get('/me',['as'=>'admin-profile-me','uses'=>'AdminProfileController@me']);
        Route::post('/me',['as'=>'admin-profile-me-save','uses'=>'AdminProfileController@update_me']);
        Route::get('/myjob',['as'=>'admin-profile-myjob','uses'=>'AdminProfileController@myjob']);
        Route::post('/myjob',['as'=>'admin-profile-myjob-save','uses'=>'AdminProfileController@update_myjob']);
        Route::get('/deletecv',['as'=>'admin-profile-myjob-deletecv','uses'=>'AdminProfileController@deletecv']);
        Route::get('/contact',['as'=>'admin-profile-contact','uses'=>'AdminProfileController@contact']);
        Route::post('/contact',['as'=>'admin-profile-contact-save','uses'=>'AdminProfileController@update_contact']);
        Route::get('/picture',['as'=>'admin-profile-picture','uses'=>'AdminProfileController@picture']);
        Route::post('/picture',['as'=>'admin-profile-picture-save','uses'=>'AdminProfileController@update_picture']);
        Route::get('/setting',['as'=>'admin-profile-setting','uses'=>'AdminProfileController@setting']);
        Route::post('/setting',['as'=>'admin-profile-setting-save','uses'=>'AdminProfileController@update_setting']);
    });

    Route::group(['prefix'=>'portfolio'],function(){
        Route::get('/',['as'=>'admin-portfolios','uses'=>'AdminPortfolioController@index']);
        Route::get('/create',['as'=>'admin-portfolio-create','uses'=>'AdminPortfolioController@create']);
        Route::post('/store',['as'=>'admin-portfolio-store','uses'=>'AdminPortfolioController@store']);
        Route::get('/edit/{id}',['as'=>'admin-portfolio-edit','uses'=>'AdminPortfolioController@edit']);
        Route::post('/update/{id}',['as'=>'admin-portfolio-update','uses'=>'AdminPortfolioController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-portfolio-destroy','uses'=>'AdminPortfolioController@destroy']);
        Route::get('/destroyPicture/{id}',['as'=>'admin-portfolio-destroy-picture','uses'=>'AdminPortfolioController@destroyPicture']);
        Route::get('/approve/{id}',['as'=>'admin-portfolio-approve','uses'=>'AdminPortfolioController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-portfolio-disapprove','uses'=>'AdminPortfolioController@disapprove']);
    });

    Route::group(['prefix'=>'competence'],function(){
        Route::get('/',['as'=>'admin-competences','uses'=>'AdminCompetenceController@index']);
        Route::get('/create',['as'=>'admin-competence-create','uses'=>'AdminCompetenceController@create']);
        Route::post('/store',['as'=>'admin-competence-store','uses'=>'AdminCompetenceController@store']);
        Route::get('/edit/{id}',['as'=>'admin-competence-edit','uses'=>'AdminCompetenceController@edit']);
        Route::post('/update/{id}',['as'=>'admin-competence-update','uses'=>'AdminCompetenceController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-competence-destroy','uses'=>'AdminCompetenceController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-competence-approve','uses'=>'AdminCompetenceController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-competence-disapprove','uses'=>'AdminCompetenceController@disapprove']);
    });

    Route::group(['prefix'=>'strength'],function(){
        Route::get('/',['as'=>'admin-strengths','uses'=>'AdminStrengthController@index']);
        Route::get('/create',['as'=>'admin-strength-create','uses'=>'AdminStrengthController@create']);
        Route::post('/store',['as'=>'admin-strength-store','uses'=>'AdminStrengthController@store']);
        Route::get('/edit/{id}',['as'=>'admin-strength-edit','uses'=>'AdminStrengthController@edit']);
        Route::post('/update/{id}',['as'=>'admin-strength-update','uses'=>'AdminStrengthController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-strength-destroy','uses'=>'AdminStrengthController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-strength-approve','uses'=>'AdminStrengthController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-strength-disapprove','uses'=>'AdminStrengthController@disapprove']);
    });

    Route::group(['prefix'=>'school'],function(){
        Route::get('/',['as'=>'admin-schools','uses'=>'AdminSchoolController@index']);
        Route::get('/create',['as'=>'admin-school-create','uses'=>'AdminSchoolController@create']);
        Route::post('/store',['as'=>'admin-school-store','uses'=>'AdminSchoolController@store']);
        Route::get('/edit/{id}',['as'=>'admin-school-edit','uses'=>'AdminSchoolController@edit']);
        Route::post('/update/{id}',['as'=>'admin-school-update','uses'=>'AdminSchoolController@update']);
        Route:get('/destroy/{id}',['as'=>'admin-school-destroy','uses'=>'AdminSchoolController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-school-approve','uses'=>'AdminSchoolController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-school-disapprove','uses'=>'AdminSchoolController@disapprove']);
    });

    Route::group(['prefix'=>'experience'],function(){
        Route::get('/',['as'=>'admin-experiences','uses'=>'AdminExperienceController@index']);
        Route::get('/create',['as'=>'admin-experience-create','uses'=>'AdminExperienceController@create']);
        Route::post('/store',['as'=>'admin-experience-store','uses'=>'AdminExperienceController@store']);
        Route::get('/edit/{id}',['as'=>'admin-experience-edit','uses'=>'AdminExperienceController@edit']);
        Route::post('/update/{id}',['as'=>'admin-experience-update','uses'=>'AdminExperienceController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-experience-destroy','uses'=>'AdminExperienceController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-experience-approve','uses'=>'AdminExperienceController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-experience-disapprove','uses'=>'AdminExperienceController@disapprove']);
    });

    Route::group(['prefix'=>'reference'],function(){
        Route::get('/',['as'=>'admin-references','uses'=>'AdminReferenceController@index']);
        Route::get('/create',['as'=>'admin-reference-create','uses'=>'AdminReferenceController@create']);
        Route::post('/store',['as'=>'admin-reference-store','uses'=>'AdminReferenceController@store']);
        Route::get('/edit/{id}',['as'=>'admin-reference-edit','uses'=>'AdminReferenceController@edit']);
        Route::post('/update/{id}',['as'=>'admin-reference-update','uses'=>'AdminReferenceController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-reference-destroy','uses'=>'AdminReferenceController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-reference-approve','uses'=>'AdminReferenceController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-reference-disapprove','uses'=>'AdminReferenceController@disapprove']);
    });

    Route::group(['prefix'=>'skill'],function(){
        Route::get('/',['as'=>'admin-skills','uses'=>'AdminSkillController@index']);
        Route::get('/create',['as'=>'admin-skill-create','uses'=>'AdminSkillController@create']);
        Route::post('/store',['as'=>'admin-skill-store','uses'=>'AdminSkillController@store']);
        Route::get('/edit/{id}',['as'=>'admin-skill-edit','uses'=>'AdminSkillController@edit']);
        Route::post('/update/{id}',['as'=>'admin-skill-update','uses'=>'AdminSkillController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-skill-destroy','uses'=>'AdminSkillController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-skill-approve','uses'=>'AdminSkillController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-skill-disapprove','uses'=>'AdminSkillController@disapprove']);
    });

    Route::group(['prefix'=>'knowledge'],function(){
        Route::get('/',['as'=>'admin-knowledges','uses'=>'AdminKnowledgeController@index']);
        Route::get('/create',['as'=>'admin-knowledge-create','uses'=>'AdminKnowledgeController@create']);
        Route::post('/store',['as'=>'admin-knowledge-store','uses'=>'AdminKnowledgeController@store']);
        Route::get('/edit/{id}',['as'=>'admin-knowledge-edit','uses'=>'AdminKnowledgeController@edit']);
        Route::post('/update/{id}',['as'=>'admin-knowledge-update','uses'=>'AdminKnowledgeController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-knowledge-destroy','uses'=>'AdminKnowledgeController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-knowledge-approve','uses'=>'AdminKnowledgeController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-knowledge-disapprove','uses'=>'AdminKnowledgeController@disapprove']);
    });

    Route::group(['prefix'=>'award'],function(){
        Route::get('/',['as'=>'admin-awards','uses'=>'AdminAwardController@index']);
        Route::get('/create',['as'=>'admin-award-create','uses'=>'AdminAwardController@create']);
        Route::post('/store',['as'=>'admin-award-store','uses'=>'AdminAwardController@store']);
        Route::get('/edit/{id}',['as'=>'admin-award-edit','uses'=>'AdminAwardController@edit']);
        Route::post('/update/{id}',['as'=>'admin-award-update','uses'=>'AdminAwardController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-award-destroy','uses'=>'AdminAwardController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-award-approve','uses'=>'AdminAwardController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-award-disapprove','uses'=>'AdminAwardController@disapprove']);
    });

    Route::group(['prefix'=>'achievement'],function(){
        Route::get('/',['as'=>'admin-achievements','uses'=>'AdminAchievementController@index']);
        Route::get('/create',['as'=>'admin-achievement-create','uses'=>'AdminAchievementController@create']);
        Route::post('/store',['as'=>'admin-achievement-store','uses'=>'AdminAchievementController@store']);
        Route::get('/edit/{id}',['as'=>'admin-achievement-edit','uses'=>'AdminAchievementController@edit']);
        Route::post('/update/{id}',['as'=>'admin-achievement-update','uses'=>'AdminAchievementController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-achievement-destroy','uses'=>'AdminAchievementController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-achievement-approve','uses'=>'AdminAchievementController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-achievement-disapprove','uses'=>'AdminAchievementController@disapprove']);
    });

    Route::group(['prefix'=>'social'],function(){
        Route::get('/',['as'=>'admin-socials','uses'=>'AdminSocialController@index']);
        Route::get('/create',['as'=>'admin-social-create','uses'=>'AdminSocialController@create']);
        Route::post('/store',['as'=>'admin-social-store','uses'=>'AdminSocialController@store']);
        Route::get('/edit/{id}',['as'=>'admin-social-edit','uses'=>'AdminSocialController@edit']);
        Route::post('/update/{id}',['as'=>'admin-social-update','uses'=>'AdminSocialController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-social-destroy','uses'=>'AdminSocialController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-social-approve','uses'=>'AdminSocialController@approve']);
        Route::get('/disapprove/{id}',['as'=>'admin-social-disapprove','uses'=>'AdminSocialController@disapprove']);
    });

    Route::group(['prefix'=>'proverb'],function(){
        Route::get('/',['as'=>'admin-proverbs','uses'=>'AdminProverbController@create']);
        Route::post('/update',['as'=>'admin-proverb-update','uses'=>'AdminProverbController@update']);
    });

    Route::group(['prefix'=>'wallpaper'],function(){
        Route::get('/',['as'=>'admin-wallpapers','uses'=>'AdminWallpaperController@edit']);
        Route::post('/update',['as'=>'admin-wallpapers-update','uses'=>'AdminWallpaperController@update']);
    });

    Route::group(['prefix'=>'design'],function(){
        Route::get('/',['as'=>'admin-designs','uses'=>'AdminDesignController@index']);
        Route::get('/create',['as'=>'admin-design-create','uses'=>'AdminDesignController@create']);
        Route::post('/store',['as'=>'admin-design-store','uses'=>'AdminDesignController@store']);
        Route::get('/edit/{id}',['as'=>'admin-design-edit','uses'=>'AdminDesignController@edit']);
        Route::post('/update/{id}',['as'=>'admin-design-update','uses'=>'AdminDesignController@update']);
        Route::get('/destroy/{id}',['as'=>'admin-design-destroy','uses'=>'AdminDesignController@destroy']);
        Route::get('/approve/{id}',['as'=>'admin-design-approve','uses'=>'AdminDesignController@approve']);
    });

//    Route::group(['prefix'=>'mail'],function(){
//        Route::get('/{page}',['as'=>'admin-mails','uses'=>'AdminMailController@index']);
//        Route::get('/create',['as'=>'admin-mail-create','uses'=>'AdminMailController@create']);
//        Route::post('/send',['as'=>'admin-mail-send','uses'=>'AdminMailController@send']);
//        Route::get('/show/{id}',['as'=>'admin-mail-show','uses'=>'AdminMailController@show']);
//        Route::get('/destroy/{id}',['as'=>'admin-mail-destroy','uses'=>'AdminMailController@destroy']);
//        Route::get('/read/{id}',['as'=>'admin-mail-read','uses'=>'AdminMailController@read']);
//        Route::get('/unread/{id}',['as'=>'admin-mail-unread','uses'=>'AdminMailController@unread']);
//    });
    Route::group(['prefix'=>'user', 'middleware'=>'SAdmin'],function(){
        Route::get('/',['as'=>'admin-users','uses'=>'AdminUserController@index']);
        Route::get('/create',['as'=>'admin-user-create','uses'=>'AdminUserController@create']);
        Route::post('/store',['as'=>'admin-user-store','uses'=>'AdminUserController@store']);
        Route::get('/edit/{id}',['as'=>'admin-user-edit','uses'=>'AdminUserController@edit']);
        Route::post('/update/{id}',['as'=>'admin-user-update','uses'=>'AdminUserController@update']);
        Route::get('/activate/{id}',['as'=>'admin-user-activate','uses'=>'AdminUserController@activate']);
        Route::get('/deactivate/{id}',['as'=>'admin-user-deactivate','uses'=>'AdminUserController@deactivate']);
        Route::get('/destroy/{id}',['as'=>'admin-user-destroy','uses'=>'AdminUserController@destroy']);
    });
});