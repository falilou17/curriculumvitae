<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminDesignRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'background'=> 'required|min:7',
            'backgroundDiv'=> 'required|min:7',
            'faBackground'=> 'required|min:7',
            'title'=> 'required|min:7',
            'description'=> 'required|min:7',
            'element'=> 'required|min:7'
        ];
    }
}
