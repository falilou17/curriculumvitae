<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminExportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile'=> 'required_without_all:portfolio,competence,strength,school,reference,skill,knowledge,award,achievement,social,design,proverb',
            'portfolio',
            'competence',
            'strength',
            'school',
            'reference',
            'skill',
            'knowledge',
            'award',
            'achievement',
            'social',
            'design',
            'proverb'
        ];
    }
}
