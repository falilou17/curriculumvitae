<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminReferenceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise'=> 'required_without:fullName',
            'fullName'=> 'required_without:enterprise',
            'profession'=> 'required_with:fullName',
        ];
    }
}
