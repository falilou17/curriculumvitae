<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminWallpaperRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'strength' => 'mimes:jpg,png,jpeg',
            'achievement' => 'mimes:jpg,png,jpeg',
            'reference' => 'mimes:jpg,png,jpeg',
            'portfolio' => 'mimes:jpg,png,jpeg',
            'profile' => 'mimes:jpg,png,jpeg',
            'service' => 'mimes:jpg,png,jpeg',
            'contact' => 'mimes:jpg,png,jpeg',
        ];
    }
}
