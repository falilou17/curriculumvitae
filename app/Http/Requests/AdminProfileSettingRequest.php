<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminProfileSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'template_id' => 'required',
            'siteTitle' => 'string|required|max:255',
            'siteDescription' => 'string|required',
            'siteKeywords' => 'string|required'
        ];
    }
}
