<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminProfileMeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName'=> 'required_without:lastName|min:3',
            'lastName'=> 'required_without:firstName|min:2'
        ];
    }
}
