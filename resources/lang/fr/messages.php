<?php

return [
    // template 1
    'knowmebetter' => 'mieux nous connaître',
    'home' => 'accueil',
    'profile' => 'profil',
    'services' => 'services',
    'ourservices' => 'nos services',
    'strengths' => 'points forts',
    'mystrengths' => 'nos points forts',
    'portfolio' => 'portfolio',
    'resume' => 'CV',
    'testimonials' => 'Témoignages',
    'skills' => 'compétences',
    'contact' => 'contact',
    'stats' => 'stats',
    'accolades' => 'accolades',
    'contactme' => 'nous contacter',
    'workexperience' => 'expérience professionnelle',
    'responsibilities' => 'responsabilités',
    'knowledge' => 'connaissance',
    'mylatestwork' => 'album',
    'contactdetails' => 'coordonnées',
    'downloadresume' => 'télécharger mon cv',
    'showresume' => 'voir mon cv',
    'myresume' => 'mon cv',
    'myeducation' => 'ma formation',
    'skillsandexpertise' => 'compétences et expertise',
    'superhero' => 'maitrise',
    'preetygood' => 'très bon',
    'good' => 'moyen',
    'newbie' => 'boff',
    'me' => 'nous',
    'name' => 'nom',
    'subject' => 'sujet',
    'message' => 'message',
    'send' => 'envoyer',
    'download' => 'téléchargement',
    'close' => 'fermer',
    'readmore' => 'en savoir plus',
    'getintouch' => 'prendre contact',
    'messagesent' => 'votre message a été envoyé',
    'fieldmissing' => 'opps !! vous ne remplissez pas correctement tous les champs obligatoires',
    'ourcompanyltd' => 'notre société',
    'getstarted' => 'commencer',
    'ourworks' => 'nos œuvres',
    'aboutus' => 'à propos de nous',
    'whatourcostumerhavetosay' => 'Ce que nos clients ont à dire',
    'whywearegood' => 'pourquoi nous sommes bons',
    'welcomeTo' => 'bienvenue chez',

    'basicinfo' => 'Information',
    'published' => 'Publié',
    'technologies' => 'Technologies',
    'client' => 'Client',
    'news' => 'Actualités',
    'phoneNumber' => 'Téléphone',
    'address' => 'Adresse',
];














































