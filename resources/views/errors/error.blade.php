@if($errors->has())
    <div class="row text-center">
        <div class="col-sm-offset-2 col-sm-8 alert alert-danger alert-dismissable" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
{{--@if(Session::has('state'))--}}
    {{--<div class="row text-center">--}}
        {{--<div class=" col-sm-offset-2 col-sm-8 alert alert-{{ Session::get('type') }} alert-dismissable" role="alert">--}}
            {{--<p>{{ Session::get('state') }}</p>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}