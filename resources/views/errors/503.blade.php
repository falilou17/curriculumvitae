<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <head>
        <title>Coming Soon 2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="{{ asset('front/maintenance/images/icons/favicon.ico') }}"/>

        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/vendor/bootstrap/css/bootstrap.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/vendor/animate/animate.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/vendor/select2/select2.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/css/util.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('front/maintenance/css/main.css') }}">

    </head>
    <body>

        <div class="simpleslide100">
            <div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('front/maintenance/images/bg01.jpeg') }})"></div>
            <div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('front/maintenance/images/bg02.jpeg') }})"></div>
            <div class="simpleslide100-item bg-img1" style="background-image: url({{ asset('front/maintenance/images/bg03.jpeg') }})"></div>
        </div>
        <div class="size1 overlay1">

            <div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
                <h3 class="l1-txt1 txt-center p-b-25">
                    Bientôt disponible
                </h3>
                <p class="m2-txt1 txt-center p-b-48">
                    Notre site web est en construction !
{{--                    Our website is under construction, follow us for update now!--}}
                </p>
                <div class="flex-w flex-c-m cd100 p-b-33">
                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                        <span class="l2-txt1 p-b-9 days">10</span>
                        <span class="s2-txt1">Jours</span>
                    </div>
                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                        <span class="l2-txt1 p-b-9 hours">17</span>
                        <span class="s2-txt1">Heures</span>
                    </div>
                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                        <span class="l2-txt1 p-b-9 minutes">50</span>
                        <span class="s2-txt1">Minutes</span>
                    </div>
                    <div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
                        <span class="l2-txt1 p-b-9 seconds">39</span>
                        <span class="s2-txt1">Secondes</span>
                    </div>
                </div>
{{--                <form class="w-full flex-w flex-c-m validate-form">--}}
{{--                    <div class="wrap-input100 validate-input where1"--}}
{{--                         data-validate="Valid email is required: ex@abc.xyz">--}}
{{--                        <input class="input100 placeholder0 s2-txt2" type="text" name="email"--}}
{{--                               placeholder="Enter Email Address">--}}
{{--                        <span class="focus-input100"></span>--}}
{{--                    </div>--}}
{{--                    <button class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1">--}}
{{--                        Subscribe--}}
{{--                    </button>--}}
{{--                </form>--}}
            </div>
        </div>

        <script src="{{ asset('front/maintenance/vendor/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('front/maintenance/vendor/bootstrap/js/popper.js') }}" type="text/javascript"></script>
        <script src="{{ asset('front/maintenance/vendor/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('front/maintenance/vendor/select2/select2.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('front/maintenance/vendor/countdowntime/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('front/maintenance/vendor/countdowntime/moment-timezone.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('front/maintenance/vendor/countdowntime/moment-timezone-with-data.min.js') }}"
                type="text/javascript"></script>
        <script src="{{ asset('front/maintenance/vendor/countdowntime/countdowntime.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            $('.cd100').countdown100({
                /*Set Endtime here*/
                /*Endtime must be > current time*/
                endtimeYear: 2019,
                endtimeMonth: 11,
                endtimeDate: 10,
                endtimeHours: 0,
                endtimeMinutes: 0,
                endtimeSeconds: 0,
                timeZone: "Africa/Abidjan"
                // ex:  timeZone: "America/New_York"
                //go to " http://momentjs.com/timezone/ " to get timezone
            });
        </script>

        <script src="{{ asset('front/maintenance/vendor/tilt/tilt.jquery.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            $('.js-tilt').tilt({
                scale: 1.1
            });
        </script>

        <script src="{{ asset('front/maintenance/js/main.js') }}" type="text/javascript"></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"
                type="text/javascript"></script>
        <script type="text/javascript">
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-23581568-13');
        </script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js"
                data-cf-settings="b935f1207e92cd6d9a57394b-|49" defer=""></script>
    </body>
</html>