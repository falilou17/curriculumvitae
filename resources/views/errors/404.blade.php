<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CV | 404 Error</title>

    <link href="{{ asset('adminStuff/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('adminStuff/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">


<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold">Page Not Found</h3>
    <div class="error-desc">
        Sorry, but the page you are looking for has not been found. Try checking the URL for error, then hit the
        refresh button on your browser or try found something else in our app.
    </div>
    <hr>
    <a href="{{ route('index') }}" class="btn btn-lg btn-primary" role="button">Go to Home</a>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('adminStuff/js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('adminStuff/js/bootstrap.min.js') }}"></script>

</body>
</html>