<!-- FOOTER START -->
<footer class="footer background2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-scroll">
                    <a class="btn-custom" href="#home"><i class="icon-Arrow-Up"></i></a>
                </div>
                <!-- //.page-scroll -->

                @if(!empty($socials))
                    <ul class="list-inline social-icons">
                        @foreach($socials as $social)
                            <li>
                                <div class="item background3">
                                    <a href="{{ $social->link }}" target="_blank"><i class="fa {{ $social->icon }}"></i></a>
                                </div>
                                <!-- //.item -->
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <!-- //.col-md-12 -->
        </div>
        <!-- //.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="copyright text-center">
                    <p>&copy; {{ date("Y") }}. All rights reserved.</p>
                </div>
                <!-- //.copyright -->
            </div>
            <!-- //.col-md-12 -->
        </div>
        <!-- //.row -->
    </div>
    <!-- //.container -->
</footer>
<!-- //FOOTER END -->