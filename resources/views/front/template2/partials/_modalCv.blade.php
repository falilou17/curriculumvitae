@if(!empty($profile->cv))
    <div class="modal fade" id="modalCv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <img class="col-sm-offset-1 col-sm-10 img-responsive" src="{{ asset('img/cv/'.$profile->cv) }}" alt="cv">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ ucfirst(trans('messages.close')) }}</button>
                    <a class="btn btn-custom" href="{{ asset('img/cv/'.$profile->cv) }}" download="CV-{{ $profile->firstName }}-{{ $profile->lastName }}.jpg">
                        <i class="fa fa-download"></i> {{ ucfirst(trans('messages.downloadresume')) }}
                    </a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif