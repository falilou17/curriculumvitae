<section class="section" id="profile">
    <div class="container">
        <div class="row">

            <div class="col-md-3">
                <header class="section-header section-header-line">
                    <h3 class="section-title general-color">{{ ucfirst(trans('messages.aboutus')) }}</h3>
                    <p class="text-color">{{ $profile->descBrief }}</p>
                </header>

                <p class="text-justify text-color">{{ $profile->descLong }}</p>
            </div>

            <div class="col-md-9">
                <div class="section-image">
                    <img src="{{ asset('img/'.$profile->profilePicture) }}" alt="">
                </div>
            </div>

        </div>
    </div>
</section>