<!-- SKILLS SECTION START -->
<section id="skills" class="section background1">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.skillsandexpertise')) }}</h2>

                    <p class="section-subtitle">{{ $proverbs->skill }}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <!-- CIRCLE CHART START -->
            <div class="circle-chart">
                <div class="row">
                    @foreach($skills as $skill)
                        <div class="col-sm-4 col-md-2">
                            <div class="item">
                                <div class="circle">
                                    <span class="item-progress" data-percent="{{ $skill->pourcentage }}"></span>
                                </div>
                                <!-- //.circle -->

                                <span class="percent">{{ $skill->pourcentage }}%</span>

                                <h4 class="text-center">{{ $skill->name }}</h4>
                            </div>
                            <!-- //.item -->
                        </div>
                    @endforeach
                    <!-- //.col-md-2 -->
                </div>
                <!-- //.row -->
            </div>
            <!-- //CIRCLE CHART END -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //SKILLS SECTION END -->