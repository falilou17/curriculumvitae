<!--|===================================================================
| Review
|=======================================================================|-->
<section id="review" class="section">
    <div class="container">
        <div class="section-header section-header-line text-center">
            <h2 class="section-title general-color">{{ ucfirst(trans('messages.whatourcostumerhavetosay')) }}...</h2>
            <p class="text-color">{{ $proverbs->reference }}</p>
        </div>

        <div class="row">
            @foreach($references as $reference)
                <div class="col-md-4">
                    <!--|Review|-->
                    <div class="review-wrap">
                        <div class="review">
                            <figure class="review-meta">
                                <img class="avatar" src="{{ asset('img/reference/'.$reference->picture) }}" alt="">
                                <figcaption class="info">
                                    <h6 class="name general-color">{{ $reference->fullName }}</h6>
                                    <span class="meta text-color"><em>{{ $reference->profession }}</em> {{ $reference->enterprise ? '- '.$reference->enterprise : '' }}</span>
                                </figcaption>
                            </figure>

                            <div class="text text-1" style="background-color: {{ $designs->title }}">
                                <p class="text-color">{{ $reference->description }}</p>
                            </div>
                        </div>
                    </div> <!--|End Review|-->
                </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="clients">
            @foreach($references as $reference)
                <a class="client" href="javascript:void(0)"><img src="{{ asset('img/reference/'.$reference->picture) }}" alt=""></a>
            @endforeach
        </div>
    </div>

</section> <!--|End Review|-->