<!--|===================================================================
| Works
|=======================================================================|-->
<section id="works" class="section section-pb-0">
    <div class="container">
        <header class="section-header section-header-line text-center">
            <h2 class="section-title general-color">{{ ucfirst(trans('messages.ourworks')) }}</h2>
            <p class="text-color">{{ $proverbs->portfolio }}</p>
        </header>
    </div>

{{--    <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter">--}}
{{--        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">--}}
{{--            All <div class="cbp-filter-counter"></div>--}}
{{--        </div>--}}
{{--        <div data-filter=".print" class="cbp-filter-item">--}}
{{--            Print <div class="cbp-filter-counter"></div>--}}
{{--        </div>--}}
{{--        <div data-filter=".web-design" class="cbp-filter-item">--}}
{{--            Web Design <div class="cbp-filter-counter"></div>--}}
{{--        </div>--}}
{{--        <div data-filter=".graphic" class="cbp-filter-item">--}}
{{--            Graphic <div class="cbp-filter-counter"></div>--}}
{{--        </div>--}}
{{--        <div data-filter=".motion" class="cbp-filter-item">--}}
{{--            Motion <div class="cbp-filter-counter"></div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat" style="margin-bottom: 50px;">
        @foreach($portfolios as $portfolio)
            <div class="cbp-item col-md-3">
                <a href="{{ count($portfolio->medias) ? asset('img/portfolio/'.$portfolio->medias[0]->path) : '#' }}" class="cbp-caption cbp-lightbox"
                   data-title="{{ mb_strimwidth($portfolio->name, 0, 100, "...") }}<br>
                   {{ ucfirst(trans('messages.basicinfo')) }}: {{ $portfolio->description }}<br>
                   {{ ucfirst(trans('messages.published')) }}: {{ date_format(date_create($portfolio->datePublish), 'd F Y') }}<br>
                   {{ ucfirst(trans('messages.technologies')) }}: {{ $portfolio->description }}<br>
                   @if(!empty($portfolio->client))
                        {{ ucfirst(trans('messages.client')) }}: {{ $portfolio->client }}<br>
                    @endif
                           ">
                    <div class="cbp-caption-defaultWrap text-center" style="width: 250px;overflow: visible;">
                        @if(count($portfolio->medias))
                            <img style="width: 100%;height: auto;" class="portfolio-img" src="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" alt="">
                        @endif
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">{{ mb_strimwidth($portfolio->name, 0, 100, "...") }}</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
{{--            <div class="item col-sm-6 col-md-3">--}}
{{--                <div class="project-wrapper">--}}
{{--                    <div class="project-link">--}}
{{--                        <a href="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" class="zoom" title="{{ $portfolio->name }}">--}}
{{--                            <i class="icon-Full-Screen"></i>--}}
{{--                        </a>--}}
{{--                        <a href="{{ route('portfolio-show',[$portfolio->id]) }}" class="external-link">--}}
{{--                            <i class="icon-Link"></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <!-- //.project-link -->--}}
{{--                    <div class="project-title">--}}
{{--                        <h4>{{ mb_strimwidth($portfolio->name, 0, 100, "...") }}</h4>--}}
{{--                    </div>--}}
{{--                    <!-- //.project-title -->--}}

{{--                    <img src="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" style="height: 320px" alt="" class="img-responsive"/>--}}
{{--                </div>--}}
{{--                <!-- //.project-wrapper -->--}}
{{--            </div>--}}
            <!-- //.item -->

{{--        <div class="cbp-item web-design graphic">--}}
{{--            <a href="{{ asset('front/template2/images/portfolio/portfolio-1a.jpg') }}" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non nisl libero.">--}}
{{--                <div class="cbp-caption-defaultWrap">--}}
{{--                    <img src="{{ asset('front/template2/images/portfolio/portfolio-1.jpg') }}" alt="">--}}
{{--                </div>--}}
{{--                <div class="cbp-caption-activeWrap">--}}
{{--                    <div class="cbp-l-caption-alignCenter">--}}
{{--                        <div class="cbp-l-caption-body">--}}
{{--                            <div class="cbp-l-caption-title">Bolt UI</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <div class="cbp-item print motion">--}}
{{--            <a href="{{ asset('front/template2/images/portfolio/portfolio-2a.jpg') }}" class="cbp-caption cbp-lightbox" data-title="World Clock<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non nisl libero.">--}}
{{--                <div class="cbp-caption-defaultWrap">--}}
{{--                    <img src="{{ asset('front/template2/images/portfolio/portfolio-2.jpg') }}" alt="">--}}
{{--                </div>--}}
{{--                <div class="cbp-caption-activeWrap">--}}
{{--                    <div class="cbp-l-caption-alignCenter">--}}
{{--                        <div class="cbp-l-caption-body">--}}
{{--                            <div class="cbp-l-caption-title">World Clock</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <div class="cbp-item print motion">--}}
{{--            <a href="{{ asset('front/template2/images/portfolio/portfolio-3a.jpg') }}" class="cbp-caption cbp-lightbox" data-title="WhereTO App<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non nisl libero.">--}}
{{--                <div class="cbp-caption-defaultWrap">--}}
{{--                    <img src="{{ asset('front/template2/images/portfolio/portfolio-3.jpg') }}" alt="">--}}
{{--                </div>--}}
{{--                <div class="cbp-caption-activeWrap">--}}
{{--                    <div class="cbp-l-caption-alignCenter">--}}
{{--                        <div class="cbp-l-caption-body">--}}
{{--                            <div class="cbp-l-caption-title">WhereTO App</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        <div class="cbp-item motion graphic">--}}
{{--            <a href="{{ asset('front/template2/images/portfolio/portfolio-4a.jpg') }}" class="cbp-caption cbp-lightbox" data-title="Seemple* Music<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non nisl libero.">--}}
{{--                <div class="cbp-caption-defaultWrap">--}}
{{--                    <img src="{{ asset('front/template2/images/portfolio/portfolio-4.jpg') }}" alt="">--}}
{{--                </div>--}}
{{--                <div class="cbp-caption-activeWrap">--}}
{{--                    <div class="cbp-l-caption-alignCenter">--}}
{{--                        <div class="cbp-l-caption-body">--}}
{{--                            <div class="cbp-l-caption-title">Seemple* Music</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}

</section> <!--|End Works|-->

