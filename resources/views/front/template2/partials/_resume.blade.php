<!-- RESUME SECTION START -->
<section id="resume" class="section background2">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.myresume')) }}</h2>

                    <p class="section-subtitle">
                        {{ $proverbs->resume}}
                    </p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->
            @if(count($schools) > 0)
                @include('front.template2.partials._school')
            @endif
            @if(count($experiences) > 0)
                @include('front.template2.partials._experience')
            @endif

</div>
<!-- //.section-content -->
</div>
<!-- //.container -->
</section>
<!-- //RESUME SECTION END -->