<section class="section" id="strengths">
    <div class="container">
        <div class="section-header section-header-line text-center">
            <h2 class="section-title general-color">{{ ucfirst(trans('messages.whywearegood')) }}</h2>
            <p class="text-color">{{ $proverbs->strength }}</p>
        </div>

        <div class="skillst">
            <div class="row">
                {{--*/ $cpt=1 /*--}}
                @foreach($strengths as $strength)
                    <div class="col-md-6 col-sm-6">
                        <div class="skillbar" data-percent="{{ $strength->pourcentage }}%">
                            <div class="title general-color">{{ $strength->name }}<span class="count"></span></div>
                            <div class="count-bar-outter">
                                <div class="count-bar count-bar-{{ $cpt % 6 }}"></div>
                            </div>
                        </div>
                    </div>
                    {{--*/ $cpt++ /*--}}
                @endforeach
            </div>
        </div>

    </div>
</section>