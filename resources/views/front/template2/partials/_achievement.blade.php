<div class="statistics" id="milestones">
    <h3 style="color: white" class="general-color">{{ $proverbs->achievement }}</h3>
    <div class="container">
        <div class="row">
            @foreach($achievements as $achievement)
                <div class="col-md-3 col-sm-6">
                    <!--|Statistic Box|-->
                    <div class="statistic-box">
                        <i style="color: white" class="fa-2x fa {{ $achievement->icon }}"></i>
                        <h4 class="title general-color"><span class="countup">{{ $achievement->score }}</span></h4>
                        <span class="tb-meta text-color">{{ $achievement->title }}</span>
                    </div> <!--|End Statistic Box|-->
                </div>
            @endforeach
        </div>
    </div>
</div>