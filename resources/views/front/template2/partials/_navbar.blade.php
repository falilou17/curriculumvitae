<nav class="navbar navbar-default background1">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="ion-drag"></span>
            </button>

            <!--|Logo|-->
            <a class="navbar-brand" href="#intro-section"><img style="width: 40px;" src="{{ asset('img/'.$profile->profilePicture) }}" alt=""></a>
        </div>

        <!--Site Navbar-->
        <div class="collapse scrollspy navbar-collapse" id="primary-menu">
            <ul class="nav navbar-nav navbar-right general-color">
                <li class="page-scroll">
                    <a href="#home">{{ ucfirst(trans('messages.home')) }}</a>
                </li>

                <li class="page-scroll">
                    <a href="#profile">{{ ucfirst(trans('messages.profile')) }}</a>
                </li>

                @if(count($competences) > 0)
                    <li class="page-scroll">
                        <a href="#services">{{ ucfirst(trans('messages.services')) }}</a>
                    </li>
                @endif

                @if(count($strengths) > 0)
                    <li class="page-scroll">
                        <a href="#strengths">{{ ucfirst(trans('messages.strengths')) }}</a>
                    </li>
                @endif

                @if(count($portfolios) > 0)
                    <li class="page-scroll">
                        <a href="#works">{{ ucfirst(trans('messages.portfolio')) }}</a>
                    </li>
                @endif

                @if(count($schools) > 0 || count($experiences) > 0)
                    <li class="page-scroll">
                        <a href="#resume">{{ ucfirst(trans('messages.resume')) }}</a>
                    </li>
                @endif

                @if(count($references) > 0)
                    <li class="page-scroll">
                        <a href="#review">{{ ucfirst(trans('messages.testimonials')) }}</a>
                    </li>
                @endif

                @if(count($skills) > 0)
                    <li class="page-scroll">
                        <a href="#skills">{{ ucfirst(trans('messages.skills')) }}</a>
                    </li>
                @endif

                @if(!empty($profile->email) && !$profile->hideContact)
                    <li>
                        <a href="#contact">{{ ucfirst(trans('messages.contact')) }}</a>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>

    <div id='progress-bar'>
        <div id="progress">
        </div>
    </div>
</nav>