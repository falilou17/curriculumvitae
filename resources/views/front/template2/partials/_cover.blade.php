<!--|===================================================================
| Introduction Section
|=======================================================================|-->
<section id="intro-section" class="intro-section overlay-black">
    <div class="overlay-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-10 block-center">
                    <div class="intro-text">
                        <h1 class="big-title general-color">{{ $profile->firstName." ".strtoupper($profile->lastName) }}</h1>
                        <p class="sub-title text-color">
                            {{ $profile->coverDesc }}
                        </p>
                    </div>
                    <div class="action-area">
                        <a class="btn btn-lg btn-primary" href="#profile">{{ ucfirst(trans('messages.getstarted')) }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!--|End Introduction Section|-->
