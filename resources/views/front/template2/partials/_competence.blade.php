<section id="services" class="section section-bg">
    <div class="container">
        <div class="section-header section-header-line">
            <h2 class="section-title general-color">{{ ucfirst(trans('messages.ourservices')) }}</h2>
            <p class="text-color">{{ $proverbs->competence }}</p>
        </div>

        <div class="row">
            @foreach($competences as $competence)
                <div class="col-md-4 col-sm-4" style="height: 250px;">
                    <!--|Text Box|-->
                    <div class="text-box-sm">
                        <i class="fa {{ $competence->icon }} tb-icon text-box-sm tb-icon-{{ ($competence->id % 6)+1 }}"></i>

                        <div class="tb-text">
                            <h4 class="title general-color">{{ $competence->name }}</h4>
                            <p class="text-color">{{ mb_strimwidth($competence->description, 0, 255, "...") }}</p>
                            @if(strlen($competence->description) > 254)
                                <div class="text-right">
                                    <a class="tb-more read-more" data-title="{{ $competence->name }}" data-content="{{ $competence->description }}">{{ ucfirst(trans('messages.readmore')) }}</a>
                                </div>
                            @endif
                        </div>
                    </div>  <!--|End Text Box|-->
                </div>
            @endforeach
{{--            <div class="col-md-4 col-sm-4">--}}
{{--                <!--|Text Box|-->--}}
{{--                <div class="text-box-sm">--}}
{{--                    <i class="tb-icon tb-icon-1 ion-ios-photos-outline"></i>--}}

{{--                    <div class="tb-text">--}}
{{--                        <h4 class="title">Web Designing</h4>--}}
{{--                        <p>Quisque convallis velit ac quam efficitur consectetur. Nunc sodales nisl sit amet ipsum sodales sollicitudin.</p>--}}
{{--                    </div>--}}
{{--                </div>  <!--|End Text Box|-->--}}
{{--            </div>--}}
        </div>
    </div>
</section>