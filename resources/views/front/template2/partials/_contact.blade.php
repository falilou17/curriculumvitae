<!--|===================================================================
| Footer
|=======================================================================|-->
<div id="footer" class="footer-area section overlay-black">
    <div class="overlay-inner" style="padding: 0">
        <div id="contact" class="contact">
            <div class="container">
                <header class="section-header section-header-line-w text-center">
                    <h2 class="section-title general-color">{{ ucfirst(trans('messages.getintouch')) }}</h2>
                    <p class="text-color">{{ $proverbs->contact }}</p>
                </header>

                <!--|Contact Form|-->
                <form action="{{ route('mail') }}" method="post" class="contact-form">
                {{ csrf_field() }}
                <!--|Action Message|-->
                    <div class="action-message">
                        <p class="alert-success contact-success">{{ ucfirst(trans('messages.messagesent')) }}!</p>
                        <p class="alert-danger contact-error">{{ ucfirst(trans('messages.fieldmissing')) }}.</p>
                    </div> <!--|End Action Message|-->
                <!--|Action Message|-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="entry-field">
                                <input id="name" name="name" placeholder="{{ ucfirst(trans('messages.name')) }}" required type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="entry-field">
                                <input id="email" name="sender" placeholder="Email" required type="email" value="">
                                <input type="hidden" name="receiver" required value="{{ $profile->email }}"
                                       autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="entry-field">
                                <input id="subject" name="subject" placeholder="{{ ucfirst(trans('messages.subject')) }}" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="entry-field">
                        <textarea id="message" rows="8" name="content" placeholder="{{ ucfirst(trans('messages.message')) }}"
                                  required>{{ old('content') }}</textarea>
                    </div>
                    <input type="hidden" value="1" name="ajax">
                    <div class="text-center">
                        <button id="submit" class="btn btn-lg btn-primary" type="submit">{{ ucfirst(trans('messages.send')) }}</button>
                    </div>
                </form> <!--|End Contact Form|-->
            </div>
        </div>
        <footer class="footer background2">
            <div class="footer-inner background2">
                <div class="container">
                    <div class="conct-border background2">
                        <div class="col-sm-12 col-md-6">
                            <i class="tb-icon ion-ribbon-b"></i>
                            <p class="text-justify text-color">{{ $profile->descLong }}</p>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <i class="tb-icon ion-location"></i>
                            <p class="text-color"> {{ ucfirst(trans('messages.ourcompanyltd')) }}
                                <br>
                                    {{ $profile->address }}<br>
                                <br>
                                Tel : {{ $profile->phone }}<br>
                                <br>
                                Mail : {{ $profile->email }}<br>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="footer-content">
                    <div class="social-links">
                        @foreach($socials as $social)
                            <a target="_blank" href="{{ $social->link }}"><i class="fa {{ $social->icon }}"></i></a>
                        @endforeach

                    </div>
                    <p class="copyright general-color">&copy; 2019 {{ $profile->firstName }} {{ $profile->lastName }}. All right
                        reserved.</p>
                </div>
            </div>

        </footer>
    </div>
    <a class="scroll-top" href="#intro-section"><i class="ion-arrow-up-c"></i></a>
</div>