<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $profile->siteTitle }}</title>
        <meta name="description" content="{{ $profile->siteDescription }}">
        <meta name="keywords" content="{{ $profile->siteKeywords }}">

        <!--|Google Font(Fira Sans)|-->
        <link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,200,200italic,400italic,600,600italic,700,700italic,800,800italic,300italic' rel='stylesheet' type='text/css'>

        <!--|Icon Fonts(Ionicons)|-->
        <link href="{{ asset('front/template2/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">

        <!--|Vegas|-->
        <link href="{{ asset('front/template2/css/vegas.min.css') }}" rel="stylesheet" type="text/css">

        <!--|Magnific Popup|-->
        <link href="{{ asset('front/template2/css/magnific-popup.css') }}" rel="stylesheet" type="text/css">

        <!--|Owl Carousel|-->
        <link href="{{ asset('front/template2/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">

        <!-- load css for cubeportfolio -->
        <link rel="stylesheet" type="text/css" href="{{ asset('front/template2/cubeportfolio/css/cubeportfolio.min.css') }}">
        <!-- CSS -->
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('front/template2/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">

        <!--|Site Stylesheet|-->
        <link href="{{ asset('front/template2/css/style.css') }}" rel="stylesheet" type="text/css">

        <!--|Favicon|-->
        <link rel="icon" href="{{ asset('front/template2/images/favicon.png') }}">
        <link href=" {{ asset('front/template1/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet"
              type="text/css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="{{ asset('front/template2/js/html5shiv.min.js') }}"></script>
        <script src="{{ asset('front/template2/js/respond.min.js') }}"></script>
        <![endif]-->

        <style>
            .general-color, .general-color > li > a, .fa, .tb-icon {
                color: {{ $designs->title }}  !important;
            }

            .text-color {
                color: {{ $designs->description }} !important;
            }

            .review-wrap .text-1::after {
                border-bottom: 10px solid {{ $designs->title }};
            }

            .nav > li.page-scroll :hover {
                background-color: {{ $designs->background }}  !important;
            }

            #intro-section {
                background-image: url({{ asset('img/'.$profile->coverPicture) }});
            }

            .background1 {
                background-color: {{ $designs->background }} !important;
            }

            .background2 {
                background-color: {{ $designs->backgroundDiv }}  !important;
            }

            .background3 {
                background-color: {{ $designs->faBackground }}  !important;
            }

            .scroll-top {
                background-color: {{ $designs->element }} !important;
            }

            .scroll-top, .preloader:before, .preloader:after {
                border-color: {{ $designs->element }} !important;
            }

            .btn-primary {
                border: 5px solid {{ $designs->element }} !important;
                color: {{ $designs->element }} !important;
            }

            .btn-primary:hover {
                background-color: {{ $designs->element }} !important;
                border: 5px solid {{ $designs->element }} !important;
                color: {{ $designs->description }} !important;
            }

            #strengths {
                background-image: url("../img/wallpapers/strengths/{{ $wallpaper->strength }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #review {
                background-image: url("../img/wallpapers/references/{{ $wallpaper->reference }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #milestones {
                background-image: url("../img/wallpapers/achievements/{{ $wallpaper->achievement }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #works {
                background-image: url("../img/wallpapers/portfolios/{{ $wallpaper->portfolio }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #contact {
                background-image: url("../img/wallpapers/contacts/{{ $wallpaper->contact }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #services {
                background-image: url("../img/wallpapers/services/{{ $wallpaper->service }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            #profile {
                background-image: url("../img/wallpapers/profiles/{{ $wallpaper->profile }}");
                background-repeat: no-repeat;
                background-size: cover;
            }

            .owl-carousel{
                background-color: transparent;
            }
        </style>
    </head>

    <body>
        <!--|Preloader|-->
        <div class="preloader"></div> <!--|End Preloader|-->

        <div class="wrapper">
            <!--|===================================================================
            | Header
            |=======================================================================|-->
            <header class="header">
                <div class="top-bar">
{{--                    <div class="container">--}}
{{--                        <p><span class="desktop-meta">Support</span> {{ $profile->phone }}</p>--}}
{{--                        <p class="pull-right account-area">--}}
{{--                            <a class="login" style="color: {{ $designs->element }}" href="{{ route('admin-login') }}">Login</a>--}}
{{--                        </p>--}}
{{--                    </div>--}}
                    <select onchange="location='{{ route('index') }}?lang='+$(this).val()">
                        <option value="fr" {{ empty($lang) || $lang == 'fr' ? 'selected' : '' }}>fr</option>
                        <option value="en" {{ !empty($lang) && $lang == 'en' ? 'selected' : '' }}>en</option>
                    </select>
                </div>

                @include('front.template2.partials._navbar')
            </header> <!--|End Header|-->
        </div>

        @include('front.template2.partials._cover')

        {{-- TODO : do we need this ?  --}}
        <!--|===================================================================
        | Services
        |=======================================================================|-->
        {{--<section id="about" class="section">
            <div class="container">
                <header class="section-header section-header-line text-center">
                    <div class="col-md-7 block-center">
                        <h2 class="section-title">What We Do...</h2>
                        <p>Cras fermentum quam neque. Suspendisse potenti. Maecenas pulvinar lacus non sapien euismod lobortis ac id nibh.</p>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <!--|Text Box|-->
                        <div class="text-box">
                            <span class="sr-no">1</span>
                            <i class="tb-icon tb-icon-1 ion-shuffle"></i>
                            <h4 class="tb-title">Landing Strategy</h4>
                            <p class="text-justify">Nullam placerat felis non tempor mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <div class="text-right">
                                <a class="tb-more" href="#">Learn More</a>
                            </div>
                        </div> <!--|End Text Box|-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!--|Text Box|-->
                        <div class="text-box">
                            <span class="sr-no">2</span>
                            <i class="tb-icon tb-icon-2 ion-ios-speedometer"></i>
                            <h4 class="tb-title">Multipurpose</h4>
                            <p class="text-justify">Nullam placerat felis non tempor mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <div class="text-right">
                                <a class="tb-more" href="#">Learn More</a>
                            </div>
                        </div> <!--|End Text Box|-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!--|Text Box|-->
                        <div class="text-box">
                            <span class="sr-no">3</span>
                            <i class="tb-icon tb-icon-3 ion-ios-information"></i>
                            <h4 class="tb-title">Unlimited Lifetime</h4>
                            <p class="text-justify">Nullam placerat felis non tempor mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <div class="text-right">
                                <a class="tb-more" href="#">Learn More</a>
                            </div>
                        </div> <!--|End Text Box|-->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!--|Text Box|-->
                        <div class="text-box">
                            <span class="sr-no">4</span>
                            <i class="tb-icon tb-icon-4 ion-ios-paperplane"></i>
                            <h4 class="tb-title">Creative Marketing</h4>
                            <p class="text-justify">Nullam placerat felis non tempor mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <div class="text-right">
                                <a class="tb-more" href="#">Learn More</a>
                            </div>
                        </div> <!--|End Text Box|-->
                    </div>
                </div>
            </div>
        </section>--}}


        @include('front.template2.partials._profile')


        {{-- TODO : do we need this ?  --}}
        <!--|===================================================================
        | About
        |=======================================================================|-->
        {{--<div id="about">
            <section class="section section-bg-img">
                <div class="container">
                    <header class="section-header section-header-line-w text-center">
                        <div class="row">
                            <div class="col-md-9 block-center">
                                <h2 class="section-title">Watch this Video About Our Work</h2>
                                <p>Maecenas accumsan in ante ullamcorper blandit. Etiam massa tortor, dapibus cursus risus a, aliquam ultrices lacus. Aenean vestibulum porta placerat. Sed congue nibh quis leo efficitur dictum.</p>
                            </div>
                        </div>
                    </header>

                    <div class="row">
                        <div class="col-md-9 block-center">
                            <div class="video-wrap ratio-16-9">
                                <div class="ratio-inner vid-responsive">
                                    <iframe src="https://player.vimeo.com/video/299007286?color=5ac8c5" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="workflow clearfix">
                <div class="working-step">
                    <i class="tb-icon ion-log-in"></i>
                    <h4 class="title">Curabitur</h4>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut auctor ex non scelerisque luctus.</p>
                </div>
                <div class="working-step">
                    <i class="tb-icon ion-ios-pulse"></i>
                    <h4 class="title">Interdum</h4>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut auctor ex non scelerisque luctus.</p>
                </div>
                <div class="working-step">
                    <i class="tb-icon ion-log-out"></i>
                    <h4 class="title">Fringilla</h4>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut auctor ex non scelerisque luctus.</p>
                </div>
            </div>
        </div>--}}
        @if( count($competences) > 0)
            @include('front.template2.partials._competence')
        @endif

        @if( count($strengths) > 0)
            @include('front.template2.partials._strength')
        @endif

    {{-- TODO : add pricing --}}
        <!--|===================================================================
        | Packages
        |=======================================================================|-->
        {{--        <section id="package" class="section section-bg">
            <div class="container">
                <header class="section-header section-header-line text-center">
                    <h2 class="section-title">Our Packages</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </header>

                <div class="row">
                    <div class="col-md-3">
                        <div class="pricing-item pricing-item-1">
                            <h3 class="title">Startup</h3>
                            <p class="sentence">Fusce vitae erat sed nibh porttitor consequat. Cras rutrum in nisi vitae pulvinar.</p>
                            <div class="price price-1"><span class="currency">$</span>12<span class="period">/ month</span></div>
                            <ul class="feature-list">
                                <li>Donec elementum</li>
                                <li>Donec elementum</li>
                            </ul>
                            <ul class="feature-list-2">
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                            </ul>

                            <button class="btn btn-plan-1" aria-label="Get Started">Get Started</button>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="pricing-item pricing-item-2">
                            <h3 class="title">Business</h3>
                            <p class="sentence">Fusce vitae erat sed nibh porttitor consequat. Cras rutrum in nisi vitae pulvinar.</p>
                            <div class="price price-2"><span class="currency">$</span>39<span class="period">/ month</span></div>
                            <ul class="feature-list">
                                <li>Donec elementum</li>
                                <li>Donec elementum</li>
                                <li>Cras non porttitor justo</li>
                            </ul>
                            <ul class="feature-list-2">
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                            </ul>
                            <button class="btn btn-plan-2" aria-label="Get Started">Get Started</button>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="pricing-item pricing-item-3">
                            <h3 class="title">Corporate</h3>
                            <p class="sentence">Fusce vitae erat sed nibh porttitor consequat. Cras rutrum in nisi vitae pulvinar.</p>
                            <div class="price price-3"><span class="currency">$</span>169<span class="period">/ month</span></div>
                            <ul class="feature-list">
                                <li>Donec elementum</li>
                                <li>Donec elementum</li>
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                            </ul>
                            <ul class="feature-list-2">
                                <li>Cras non porttitor justo</li>
                            </ul>
                            <button class="btn btn-plan-3" aria-label="Get Started">Get Started</button>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="pricing-item pricing-item-4">
                            <h3 class="title">Premium</h3>
                            <p class="sentence">Fusce vitae erat sed nibh porttitor consequat. Cras rutrum in nisi vitae pulvinar.</p>
                            <div class="price price-4"><span class="currency">$</span>399<span class="period">/ month</span></div>
                            <ul class="feature-list">
                                <li>Donec elementum</li>
                                <li>Donec elementum</li>
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                                <li>Cras non porttitor justo</li>
                            </ul>
                            <button class="btn btn-plan-4" aria-label="Get Started">Get Started</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}


        @if( count($portfolios) > 0)
            @include('front.template2.partials._portfolio')
        @endif
        @if( count($achievements) > 0)
            @include('front.template2.partials._achievement')
        @endif
        @if( count($references) > 0)
            @include('front.template2.partials._reference')
        @endif

        {{-- TODO : add team --}}
        <!--|===================================================================
        | Team
        |=======================================================================|-->
        {{--<section id="team" class="section section-bg">
            <div class="container">
                <!--|Section Header|-->
                <div class="section-header section-header-line text-center">
                    <h2 class="section-title">Our Team</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div> <!--|End Section Header|-->

                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <!--|Team Member|-->
                        <figure class="team-member">
                            <img class="avatar" src="{{ asset('front/template2/images/1.png') }}" alt="">

                            <figcaption class="tb-overlay">
                                <div class="member-info">
                                    <h4 class="name">Lara Thompson</h4>
                                    <cite>Developer</cite>
                                </div>
                                <div class="member-bio">
                                    <p>Duis porttitor leo at lorem vestibulum, non ultricies elit eleifend. Praesent malesuada lectus sed nunc sagittis, ut iaculis urna mollis. Proin non feugiat elit.</p>
                                </div>
                                <div class="social-links text-right">
                                    <a href="#"><i class="ion-social-twitter-outline"></i></a>
                                    <a href="#"><i class="ion-social-facebook-outline"></i></a>
                                </div>
                            </figcaption>
                        </figure> <!--|End Team Member|-->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <!--|Team Member|-->
                        <figure class="team-member">
                            <img class="avatar" src="{{ asset('front/template2/images/2.png') }}" alt="">

                            <figcaption class="tb-overlay">
                                <div class="member-info">
                                    <h4 class="name">Nicole Kass</h4>
                                    <cite>Programmer</cite>
                                </div>
                                <div class="member-bio">
                                    <p>Duis porttitor leo at lorem vestibulum, non ultricies elit eleifend. Praesent malesuada lectus sed nunc sagittis, ut iaculis urna mollis. Proin non feugiat elit.</p>
                                </div>
                                <div class="social-links text-right">
                                    <a href="#"><i class="ion-social-twitter-outline"></i></a>
                                    <a href="#"><i class="ion-social-facebook-outline"></i></a>
                                </div>
                            </figcaption>
                        </figure> <!--|End Team Member|-->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <!--|Team Member|-->
                        <figure class="team-member">
                            <img class="avatar" src="{{ asset('front/template2/images/3.png') }}" alt="">

                            <figcaption class="tb-overlay">
                                <div class="member-info">
                                    <h4 class="name">Paul Smith</h4>
                                    <cite>Software Engineer</cite>
                                </div>
                                <div class="member-bio">
                                    <p>Duis porttitor leo at lorem vestibulum, non ultricies elit eleifend. Praesent malesuada lectus sed nunc sagittis, ut iaculis urna mollis. Proin non feugiat elit.</p>
                                </div>
                                <div class="social-links text-right">
                                    <a href="#"><i class="ion-social-twitter-outline"></i></a>
                                    <a href="#"><i class="ion-social-facebook-outline"></i></a>
                                </div>
                            </figcaption>
                        </figure> <!--|End Team Member|-->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <!--|Team Member|-->
                        <figure class="team-member">
                            <img class="avatar" src="{{ asset('front/template2/images/4.png') }}" alt="">

                            <figcaption class="tb-overlay">
                                <div class="member-info">
                                    <h4 class="name">Gena Chane</h4>
                                    <cite>Sales Manager</cite>
                                </div>
                                <div class="member-bio">
                                    <p>Duis porttitor leo at lorem vestibulum, non ultricies elit eleifend. Praesent malesuada lectus sed nunc sagittis, ut iaculis urna mollis. Proin non feugiat elit.</p>
                                </div>
                                <div class="social-links text-right">
                                    <a href="#"><i class="ion-social-twitter-outline"></i></a>
                                    <a href="#"><i class="ion-social-facebook-outline"></i></a>
                                </div>
                            </figcaption>
                        </figure> <!--|End Team Member|-->
                    </div>
                </div>
            </div>
        </section>--}}

        {{-- TODO : add Blog --}}
        <!--|===================================================================
        | Blog
        |=======================================================================|-->
        {{--<section id="blog" class="section section-bg">
            <div class="container">
                <header class="section-header section-header-line text-center">
                    <h2 class="section-title">From Our Blog</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </header>

                <div class="row post-area">
                    <div class="col-md-4">
                        <div class="post">
                            <a class="media-container ratio-16-9" href="#">
                                <div class="ratio-inner vid-responsive">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/wIimU5-UEPc"></iframe>
                                </div>
                            </a>
                            <div class="post-desc">
                                <h4 class="post-title"><a href="#">Maecenas ante justo</a></h4>
                                <div class="post-info tb-meta">
                                    <span class="post-date">15 March 2019</span>
                                </div>
                                <div class="entry-conetnt">
                                    <p>Nullam ut nulla id nibh luctus pretium eget sed tellus. Proin tincidunt mollis tortor quis elementum.</p>
                                    <div class="text-right">
                                        <a class="tb-more" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="post">
                            <div class="media-container ratio-16-9">
                                <div class="ratio-inner">
                                    <img src="{{ asset('front/template2/images/posts/img-1.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="post-desc">
                                <h4 class="post-title"><a href="#">Suspendisse non tortor sagittis</a></h4>
                                <div class="post-info tb-meta">
                                    <span class="post-date">15 March 2019</span>
                                </div>
                                <div class="entry-conetnt">
                                    <p>Nullam ut nulla id nibh luctus pretium eget sed tellus. Proin tincidunt mollis tortor quis elementum.</p>
                                    <div class="text-right">
                                        <a class="tb-more" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="post">
                            <div class="media-container ratio-16-9">
                                <div class="ratio-inner post-gallery">
                                    <img src="{{ asset('front/template2/images/posts/img-2.jpg') }}" alt="">
                                    <img src="{{ asset('front/template2/images/posts/img-3.jpg') }}" alt="">
                                    <img src="{{ asset('front/template2/images/posts/img-4.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="post-desc">
                                <h4 class="post-title"><a href="#">Vestibulum convallis</a></h4>
                                <div class="post-info tb-meta">
                                    <span class="post-date">15 March 2019</span>
                                </div>
                                <div class="entry-conetnt">
                                    <p>Nullam ut nulla id nibh luctus pretium eget sed tellus. Proin tincidunt mollis tortor quis elementum.</p>
                                    <div class="text-right">
                                        <a class="tb-more" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}

        @if(!empty($profile->email) && !$profile->hideContact)
            @include('front.template2.partials._contact')
        @endif

        <div class="modal fade" id="modalReadMore" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title general-color" id="modalReadMoreTitle">Modal title</h4>
                    </div>
                    <div class="modal-body text-color" id="modalReadMoreContent">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none;" id="img-container-cache">

        </div>

        <!--|===================================================================
        | Javascripts
        |=======================================================================|-->
        <!--|jQuery|-->
        <script src="{{ asset('front/template2/js/jquery.min.js') }}"></script>
        <!--|Custom|-->
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $('.portfolio-img').each(function () {
                        var parentWidth = $(this).parent().parent().width();
                        var childWidth = $(this).width();
                        $(this).attr('style', 'margin-left: '+((parentWidth-childWidth)/2)+'px;');
                    });
                }, 1500);

                $('.read-more').click(function () {
                    console.log('clicked');
                    $('#modalReadMoreTitle').html($(this).data('title'));
                    $('#modalReadMoreContent').html($(this).data('content'));
                    $('#modalReadMore').modal('show');
                });

                var cpt = 0;
                var maxKeyRaw = '{{ $profile->coverPicture }}';
                if (maxKeyRaw.split('-').length > 1) {
                    var maxKey = parseInt(maxKeyRaw.split('-')[0]);
                    if(maxKey > 0) {
                        for (var i = 0; i <= maxKey; i++) {
                            $('#img-container-cache').append('<img src="' + window.location.origin + '/img/' + i + '-coverPicture.png"/>');
                        }
                        setInterval(function () {
                            $('#intro-section').css('background-image', 'url(' + window.location.origin + '/img/' + cpt + '-coverPicture.png)');
                            cpt = cpt < maxKey ? cpt + 1 : 0;
                        }, 5000);
                    }
                }
            });
        </script>
        <!--|Waypoints|-->
        <script src="{{ asset('front/template2/js/waypoints.min.js') }}"></script>
        <!--|Vegas|-->
        <script src="{{ asset('front/template2/js/vegas.min.js') }}"></script>
        <!--|Fitvids|-->
        <script src="{{ asset('front/template2/js/jquery.fitvids.js') }}"></script>
        <!--|Counterup|-->
        <script src="{{ asset('front/template2/js/jquery.counterup.min.js') }}"></script>
        <!--|Appear|-->
        <script src="{{ asset('front/template2/js/jquery.appear.js') }}"></script>
        <!--|Mixitup|-->
        <script src="{{ asset('front/template2/js/jquery.mixitup.min.js') }}"></script>
        <!--|Magnific Popup|-->
        <script src="{{ asset('front/template2/js/jquery.magnific-popup.min.js') }}"></script>
        <!--|Owl Carousel|-->
        <script src="{{ asset('front/template2/js/owl.carousel.min.js') }}"></script>
        <!--|Validate|-->
        <script src="{{ asset('front/template2/js/jquery.validate.min.js') }}"></script>
        <!--|Ajaxchimp|-->
        <script src="{{ asset('front/template2/js/jquery.ajaxchimp.min.js') }}"></script>
        <!--|Form|-->
        <script src="{{ asset('front/template2/js/jquery.form.js') }}"></script>
        <!--|Bootstrap|-->
        <script src="{{ asset('front/template2/bootstrap/js/bootstrap.min.js') }}"></script>
        <!--|Init|-->
        <script src="{{ asset('front/template2/js/init.js') }}"></script>
        <!-- load cubeportfolio -->
        <script type="text/javascript" src="{{ asset('front/template2/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>

        <!-- init cubeportfolio -->
        <script type="text/javascript" src="{{ asset('front/template2/js/main.js') }}"></script>
    </body>

</html>