<section id="contact" class="contact">
    <div class="" data-aos="fade-up">

        <div class="section-title">
            <h2>{{ ucfirst(trans('messages.contact')) }}</strong></h2>
            <p>{{ $proverbs->contact }}</p>
        </div>
        <!-- ======= Contact Section ======= -->
        <div class="map-section">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4417.1156886393055!2d-17.47082819663967!3d14.747894131056835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDQ0JzUzLjAiTiAxN8KwMjgnMTAuNCJX!5e0!3m2!1sfr!2ssn!4v1615065528168!5m2!1sfr!2ssn" style="border:0; width: 100%; height: 400px;" frameborder="0" allowfullscreen="" loading="lazy"></iframe>
        </div>

        <section id="contact" class="contact">
            <div class="container-fluid">
                <div class="row justify-content-center aos-init aos-animate" data-aos="fade-up">
                    <div class="col-lg-10">
                        <div class="info-wrap">
                            <div class="row">
                                <div class="col-lg-4 info">
                                    <i class="icofont-google-map"></i>
                                    <h4>{{ ucfirst(trans('messages.address')) }}:</h4>
                                    <p>{!! $profile->address !!}</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-envelope"></i>
                                    <h4>Email:</h4>
                                    <p>{{ $profile->email }}</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-phone"></i>
                                    <h4>{{ ucfirst(trans('messages.phoneNumber')) }}:</h4>
                                    <p>{!! $profile->phone !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact Section -->
    </div>
</section>
<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>{{ $profile->lastName }}</span></strong>. All Rights Reserved
            </div>
        </div>
        <div class="social-links text-center text-md-right pt-3 pt-md-0">
            @foreach($socials as $social)
                <a target="_blank" href="{{ $social->link }}"><i class="fa {{ $social->icon }}"></i></a>
            @endforeach
        </div>
    </div>
</footer>
<!-- End Footer -->