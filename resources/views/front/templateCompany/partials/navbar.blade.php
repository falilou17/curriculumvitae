<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <h1 class="logo mr-auto"><a href="/"><span>GNCS</span>SARL</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="page-scroll">
                    <a href="#header">{{ ucfirst(trans('messages.home')) }}</a>
                </li>

                <li class="page-scroll">
                    <a href="#about-us">{{ ucfirst(trans('messages.profile')) }}</a>
                </li>

                @if(count($competences) > 0)
                    <li class="page-scroll">
                        <a href="#services">{{ ucfirst(trans('messages.services')) }}</a>
                    </li>
                @endif

                @if(count($portfolios) > 0)
                    <li class="page-scroll">
                        <a href="#portfolio">{{ ucfirst(trans('messages.portfolio')) }}</a>
                    </li>
                @endif

                @if(count($references) > 0)
                    <li class="page-scroll">
                        <a href="#testimonials">{{ ucfirst(trans('messages.testimonials')) }}</a>
                    </li>
                @endif

                @if(!$profile->hideContact)
                    <li>
                        <a href="#contact">{{ ucfirst(trans('messages.contact')) }}</a>
                    </li>
                @endif
            </ul>
        </nav><!-- .nav-menu -->

        <div class="header-social-links">
            @foreach($socials as $social)
                <a target="_blank" class="twitter" href="{{ $social->link }}"><i class="fa {{ $social->icon }}"></i></a>
            @endforeach
        </div>

    </div>
</header><!-- End Header -->
