@extends('front.templateCompany.master')

@section('navbar')@endsection
@section('header')@endsection

@section('content')
    <!-- ======= Main Section ======= -->
    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <a class="btn btn-sm" href="{{ route('index') }}"><i class="fa fa-arrow-circle-left"></i> {{ ucfirst(trans('messages.home')) }}</a>
                    <ol>
                        <li><a href="{{ route('index') }}">{{ ucfirst(trans('messages.home')) }}</a></li>
                        <li>Details {{ $portfolio->name }}</li>
                    </ol>
                </div>
            </div>
        </section><!-- End Breadcrumbs -->

        <!-- ======= Portfolio Details Section ======= -->
        <section id="portfolio-details" class="portfolio-details">
            <div class="container" data-aos="fade-up">
                <div class="portfolio-details-container">

                    <div class="owl-carousel portfolio-details-carousel">
                        @foreach($portfolio->medias as $image)
                            <img src="{{ asset('img/portfolio/'.$image->path) }}" class="img-fluid" alt="">
                        @endforeach
                    </div>

                    <div class="portfolio-info">
                        <h3>Project information</h3>
                        <ul>
                            <li><strong>Category</strong>: Web design</li>
                            <li><strong>{{ ucfirst(trans('messages.technologies')) }}</strong>: {{ $portfolio->technologies }}</li>
                            @if(!empty($portfolio->client))
                                <li><strong>{{ ucfirst(trans('messages.client')) }}</strong>:{{ $portfolio->client }}</li>
                            @endif
                            @if(!empty($portfolio->datePublish))
                                <li><strong>{{ ucfirst(trans('messages.published')) }}</strong>: {{ date_format(date_create($portfolio->datePublish), 'd F Y') }}</li>
                            @endif
                        </ul>
                    </div>

                </div>

                <div class="portfolio-description">
                    <h2>{{ $portfolio->name }}</h2>
                    <p>
                        {{ $portfolio->description }}
                    </p>
                    @if(!empty($portfolio->testimonial))
                        <h5><strong>Retour client</strong>:</h5>
                        <p>
                            {{ $portfolio->testimonial }}
                        </p>
                    @endif
                </div>

            </div>
        </section><!-- End Portfolio Details Section -->

    </main>
    <!-- End #main -->
@endsection

@section('footer')@endsection