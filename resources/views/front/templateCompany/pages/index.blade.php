@extends('front.templateCompany.master')

@section('navbar')
    @include('front.templateCompany.partials.navbar')
@endsection

@section('header')
    @include('front.templateCompany.partials.header')
@endsection

@section('content')
    <!-- ======= Main Section ======= -->
    <main id="main">
        <!-- ======= About Us Section ======= -->
        <section id="about-us" class="about-us">
            <div class="container" data-aos="fade-up">

                <div class="section-title">
                    <h2>{{ ucfirst(trans('messages.aboutus')) }}</strong></h2>
                </div>

                <div class="row content">
                    <div class="col-lg-6" data-aos="fade-right">
                        {!! $profile->descBrief !!}
                    </div>
                    <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                        {!! $profile->descLong !!}
                    </div>
                </div>

            </div>
        </section><!-- End About Us Section -->

        @if(count($competences) > 0)
            <!-- ======= Services Section ======= -->
            <section id="services" class="services section-bg">
                <div class="container" data-aos="fade-up">

                    <div class="section-title">
                        <h2>{{ ucfirst(trans('messages.services')) }}</strong></h2>
                        <p>{{ $proverbs->competence }}</p>
                    </div>

                    <div class="row">
                        @foreach($competences as $key => $competence)
                            <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="{{ ($key+1) * 100 }}" style="margin-bottom: 30px;">
                                <div class="icon-box iconbox-blue">
                                    <div class="icon">
                                        <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke="none" stroke-width="0" fill="{{ $designs->element }}" d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174"></path>
                                        </svg>
                                        <i class="fa {{ $competence->icon }}"></i>
                                    </div>
                                    <h4><a href="">{{ $competence->name }}</a></h4>
                                    <p>{{ $competence->description }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </section>
            <!-- End Services Section -->
        @endif

        @if(count($portfolios) > 0)
            <!-- ======= Portfolio Section ======= -->
            <section id="portfolio" class="portfolio">
                <div class="container" data-aos="fade-up">

                    <div class="section-title">
                        <h2>{{ ucfirst(trans('messages.portfolio')) }}</strong></h2>
                        <p>{{ $proverbs->portfolio }}</p>
                    </div>

                    <div class="row portfolio-container" data-aos="fade-up">
                        @foreach($portfolios as $portfolio)
                            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                                <img src="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>{{ $portfolio->name }}</h4>
                                    <p>{{ mb_strimwidth($portfolio->description, 0, 250, "...") }}</p>
                                    <a href="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" data-gall="portfolioGallery" class="venobox preview-link" title="{{ $portfolio->name }}"><i class="bx bx-plus"></i></a>
                                    <a href="{{ route('portfolio-show', $portfolio->id) }}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </section>
            <!-- End Portfolio Section -->
        @endif

        @if(count($references) > 0)
            <!-- ======= Testimonials Section ======= -->
            <section id="testimonials" class="testimonials section-bg">
                <div class="container" data-aos="fade-up">

                    <div class="section-title">
                        <h2>{{ ucfirst(trans('messages.testimonials')) }}</strong></h2>
                        <p>{{ $proverbs->reference }}</p>
                    </div>

                    <div class="row">
                        @foreach($references as $reference)
                            <div class="col-lg-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                <div class="testimonial-item">
                                    <img src="{{ asset('img/reference/'.$reference->picture) }}" max-width="100" max-height="100" class="testimonial-img" alt="">
                                    <h3>{{ $reference->fullName }}</h3>
                                    <h4>{{ $reference->profession }} {{ $reference->enterprise ? '- '.$reference->enterprise : '' }}</h4>
                                    <p>
                                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                        {{ $reference->description }}
                                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
            <!-- End Our Testimonials Section -->
        @endif

    </main>
    <!-- End #main -->
@endsection

@section('footer')
    @include('front.templateCompany.partials.footer')
@endsection