<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>{{ $profile->siteTitle }}</title>
        <meta content="{{ $profile->siteDescription }}" name="description">
        <meta content="{{ $profile->siteKeywords }}" name="keywords">

        <!-- Favicons -->
        <link href="{{ asset('front/templateCompany/img/favicon.png') }}" rel="icon">
        <link href="{{ asset('front/templateCompany/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{ asset('front/templateCompany/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateColorLib/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('front/templateCompany/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/venobox/venobox.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/aos/aos.css') }}" rel="stylesheet">
        <link href="{{ asset('front/templateCompany/vendor/remixicon/remixicon.css') }}" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="{{ asset('front/templateCompany/css/style.css') }}" rel="stylesheet">

        <!-- =======================================================
        * Template Name: Company - v2.2.1
        * Template URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
        <style>
            #header .logo a span, .nav-menu a:hover, .nav-menu .active > a, .nav-menu li:hover > a, .nav-menu .drop-down ul a:hover, .nav-menu .drop-down ul .active > a, .nav-menu .drop-down ul li:hover > a, .mobile-nav-toggle i, .mobile-nav a:hover, .mobile-nav .active > a, .mobile-nav li:hover > a, .header-social-links a:hover, .portfolio .portfolio-item .portfolio-info .preview-link:hover, .portfolio .portfolio-item .portfolio-info .details-link:hover, .about-us .content ul i, .team .member .social a:hover, .features .icon-box h3 a:hover, .pricing h4, .pricing ul i, .faq .faq-list a.collapsed:hover, .contact .info i, .blog .entry .entry-title a:hover, .blog .entry .entry-meta a:hover, .blog .entry .entry-footer a:hover, .blog .blog-comments .comment h5 a:hover, .blog .sidebar .categories ul a:hover, .blog .sidebar .recent-posts h4 a:hover, #footer .footer-top .footer-links ul i, .credits a {
                color: {{ $designs->element }} !important;
            }
            .back-to-top, #hero .btn-get-started:hover, #hero .carousel-indicators li.active, .section-title h2::after, .breadcrumbs, .portfolio #portfolio-flters li:hover, .portfolio #portfolio-flters li.filter-active, .skills .progress-bar, .pricing .btn-buy, .pricing .featured h3, .pricing .advanced, .contact .info:hover i, .contact .php-email-form button[type="submit"], .portfolio-details .portfolio-details-carousel .owl-dot.active, .blog .entry .entry-content .read-more a, .blog .blog-comments .reply-form .btn-primary, .blog .blog-comments .reply-form .btn-primary:hover, .blog .blog-pagination li.active, .blog .blog-pagination li:hover, .blog .sidebar .search-form form button, .blog .sidebar .tags ul a:hover, #footer .footer-newsletter form input[type="submit"], #footer .social-links a:hover, #footer .social-links a:hover{
                background-color: {{ $designs->element }} !important;
            }
            .nav-menu .drop-down ul, #hero .carousel-content, #hero .btn-get-started, .contact .info i, .contact .php-email-form input:focus, .contact .php-email-form textarea:focus, .blog .blog-pagination li.active, .blog .blog-pagination li:hover, .blog .sidebar .tags ul a:hover {
                border-color: {{ $designs->element }} !important;
            }
            #header, #about-us, #portfolio, #contact, .contact .info, .services .icon-box, .testimonial-item {
                background-color: {{ $designs->background }} !important;
            }
            #services, #testimonials{
                background-color: {{ $designs->backgroundDiv }} !important;
            }
            #footer{
                background-color: {{ $designs->faBackground }} !important;
            }
            body, p {
                color: {{ $designs->description }} !important;
            }
            .section-title, .services .icon-box h4 a, .testimonial-item h3, .testimonial-item h4, .contact .info h4, .copyright {
                color: {{ $designs->title }} !important;
            }
        </style>
    </head>

    <body>

        @yield('navbar')

        @yield('header')

        @yield('content')

        @yield('footer')

        <a href="#" class="back-to-top"><i class="fa fa-arrow-up"></i></a>

        <!-- Vendor JS Files -->
        <script src="{{ asset('front/templateCompany/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/php-email-form/validate.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/jquery-sticky/jquery.sticky.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/venobox/venobox.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('front/templateCompany/vendor/aos/aos.js') }}"></script>

        <!-- Template Main JS File -->
        <script src="{{ asset('front/templateCompany/js/main.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function (){
                $('.btn-get-started').click(function(){
                    console.log('ok');
                    $('html,body').animate({
                        scrollTop: $("#main").offset().top-50
                    }, 'slow');
                });
            });
        </script>

    </body>

</html>