<section id="profile" class="section background1">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">

                <!-- PROFILE PICTURE START -->
                <div class="col-md-4 col-sm-12 col-xs-12 match-height pp-wrapper">
                    <div class="profile-picture style-two">
                        <img src="{{ asset('img/'.$profile->profilePicture) }}" style="max-height: 500px; width: 100%" alt="profile picture" class="img-responsive"/>

                        @if(!empty($profile->job))
                            <h4 class="title background2">{{ $profile->job }}</h4>
                        @endif
                    </div>
                </div>
                <!-- //PROFILE PICTURE END -->

                <div class="col-md-8 col-sm-12 col-xs-12 match-height">
                    <!-- PROFILE TEXT START -->
                    <div class="profile-text padding-left-yes">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="section-title">{{ $profile->firstName.' '.$profile->lastName }}</h2>

                                <p>
                                    {!! $profile->descBrief !!}
                                </p>

                                <span class="divider"></span>
                                <p class="text-color">
                                    {!! $profile->descLong !!}
                                </p>
                            </div>
                        </div>
                    </div>

                    <!-- CONTACT DETAILS START -->
                    <div class="contact-details padding-left-yes">
                        <div class="row">
                            <div class="col-sm-6 col-md-5">
                                @if(!empty($profile->address) || !empty($profile->phone) || !empty($profile->email) || !empty($profile->webSite))
                                    <h4>{{ ucfirst(trans('messages.contactdetails')) }}</h4>
                                @endif

                                <ul class="list-unstyled text-color">
                                    <li>{{ $profile->address }}</li>
                                    <li>{{ $profile->phone }}</li>
                                    <li>{{ $profile->email }}</li>
                                    <li>{{ $profile->webSite }}</li>
                                </ul>
                            </div>
                                @if(!empty($profile->cv))
                                    <a class="btn btn-lg btn-custom" data-toggle="modal" data-target="#modalCv">
                                        <i class="fa fa-eye"></i> {{ ucfirst(trans('messages.showresume')) }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>