<!-- REFERENCES SECTION START -->
<section id="references" class="section bg-image-yes">
    <div class="container-fluid section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.testimonials')) }}</h2>

                    <p class="section-subtitle">{{$proverbs->reference}}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="flexslider references">
                        <div class="slides">
                            @foreach($references as $reference)
                            <div class="item">
                                <div class="animated" data-animation-effect="flipInX">
                                    <div class="profile hidden-xs">
                                        <img src="{{ asset('img/reference/'.$reference->picture) }}" alt="" class="img-responsive img-circle img-thumbnail"/>
                                    </div>

                                    <div class="content text-color">
                                        <h3>{{$reference->entreprise}}</h3>

                                        <p>
                                            "{{$reference->description}}"
                                        </p>

                                        <p class="source general-color">
                                            {{$reference->fullName}}
                                            <br>
                                            <span class="text-color">{{$reference->profession}}</span>
                                            <span class="text-color">{{$reference->enterprise}}</span>
                                        </p>
                                    </div>
                                    <!-- //.content -->
                                </div>
                                <!-- //.animated -->
                            </div>
                            @endforeach
                            <!-- //.item -->
                        </div>
                        <!-- //.slides -->

                        <div class="flexslider-controls">
                            <ul class="flex-control-nav">
                                @foreach($references as $reference)
                                    <li>
                                        <img src="{{ asset('img/reference/'.$reference->picture) }}" alt="" class="img-responsive"/>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- //.flexslider-controls -->

                        <ul class="flex-direction-nav-custom list-inline hidden-xs">
                            <li>
                                <a class="flex-prev" href="#">
                                    <i class="icon-Left-3"></i>
                                </a>
                            </li>

                            <li>
                                <a class="flex-next" href="#">
                                    <i class="icon-Right-3"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- //.flexslider -->
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //REFERENCES SECTION END -->
