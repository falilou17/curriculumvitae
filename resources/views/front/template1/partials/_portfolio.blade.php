<!-- PORTFOLIO SECTION START -->
<section id="portfolio" class="section background2">
    <div class="container-fluid section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.mylatestwork')) }}</h2>

                    <p class="section-subtitle"> {{ $proverbs->portfolio }}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->

            <!-- PORTFOLIO START -->
            <div class="portfolio">
                <div class="row">
                    @foreach($portfolios as $portfolio)
                        <div class="item col-sm-6 col-md-3">
                            <div class="project-wrapper">
                                <div class="project-link">
                                    <a href="{{ count($portfolio->medias) ? asset('img/portfolio/'.$portfolio->medias[0]->path) : '#' }}" class="zoom" title="{{ $portfolio->name }}">
                                        <i class="icon-Full-Screen"></i>
                                    </a>
                                    <a href="{{ route('portfolio-show',[$portfolio->id]) }}?lang={{ $lang ?? 'fr' }}" class="external-link">
                                        <i class="icon-Link"></i>
                                    </a>
                                </div>
                                <!-- //.project-link -->
                                <div class="project-title">
                                    <h4>{{ mb_strimwidth($portfolio->name, 0, 100, "...") }}</h4>
                                </div>
                                <!-- //.project-title -->
                                @if(count($portfolio->medias))
                                    <img src="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" alt="" class="img-responsive"/>
                                @endif
                            </div>
                            <!-- //.project-wrapper -->
                        </div>
                        <!-- //.item -->
                    @endforeach
                </div>
                <!-- //.row -->
            </div>
            <!-- PORTFOLIO END -->
        </div>
        <!-- //.section-content -->
    </div>
</section>
<!-- //PORTFOLIO SECTION END -->
