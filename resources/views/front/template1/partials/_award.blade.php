<!-- ACCOLADES SECTION START -->
<section id="accolades" class="section background1">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.accolades')) }}</h2>

                    <p class="section-subtitle">{{ $proverbs->award }}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <!-- ACCOLADES START -->
                    <div class="accolades">
                        @foreach($awards as $award)
                            <div class="item">
                                @if(!empty($award->picture))
                                    <img src="{{ asset('img/icons/awards/'.$award->picture) }}" style="border-radius: 50px; float: left; position: relative" alt="icon">
                                @else
                                    <i class="fa {{ $award->icon }}"></i>
                                @endif

                                <div class="content">
                                    <h3>{{ $award->title }}</h3>

                                    <p class="text-color">{{ $award->description }}</p>
                                </div>
                                <!-- //.content -->
                            </div>
                            <!-- //.item -->
                        @endforeach
                    </div>
                    <!-- //ACCOLADES END -->
                    <div class="bookmark text-color text-center">
                        <i class="icon-Bookmark"></i>
                    </div>
                    <!-- //.bookmark -->
                </div>
                <!-- //.col-md-6 -->
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //ACCOLADES SECTION END -->
