<!-- CONTACT SECTION START -->
<section id="contact" class="section background1">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.contactme')) }}</h2>

                    <p class="section-subtitle">
                        {{$proverbs->contact}}
                    </p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <!-- CONTACT FORM START -->
                    <form action="{{ route('mail') }}" method="post" name="contact-form" id="contact-form" class="contact-form validate element-line" role="form">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <input type="text" name="name" id="name" class="form-control required" value="" placeholder="{{ ucfirst(trans('messages.name')) }}">
                                    </div>
                                    <!-- //.input-group -->
                                </div>
                                <!-- //.form-group -->
                            </div>
                            <!-- //.col-md-6 -->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <input type="email" name="sender" required id="email" class="form-control required email" value="" placeholder="Email">
                                        <input type="hidden" name="receiver" required value="{{ $profile->email }}"autocomplete="off">
                                    </div>
                                    <!-- //.input-group -->
                                </div>
                                <!-- //.form-group -->
                            </div>
                            <!-- //.col-md-6 -->

                        </div>
                        <!-- //.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <input type="text" required name="object" id="subject" class="form-control required" placeholder="{{ ucfirst(trans('messages.subject')) }}" value="">
                                    </div>
                                    <!-- //.input-group -->
                                </div>
                                <!-- //.form-group -->
                            </div>
                            <!-- //.col-md-12 -->
                        </div>
                        <!-- //.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <textarea name="content" required id="message" class="form-control required" placeholder="{{ ucfirst(trans('messages.message')) }}"></textarea>
                                    </div>
                                    <!-- //.input-group -->
                                </div>
                                <!-- //.form-group -->
                            </div>
                            <!-- //.col-md-12 -->
                        </div>
                        <!-- //.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button id="submit" class="text-color element-color" type="submit">{{ ucfirst(trans('messages.send')) }}</button>
                                </div>
                                <!-- //.form-group -->
                            </div>
                            <!-- //.col-md-12 -->
                        </div>
                        <!-- //.row -->
                    </form>
                    <!-- //CONTACT FORM END -->

                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //CONTACT SECTION END -->
