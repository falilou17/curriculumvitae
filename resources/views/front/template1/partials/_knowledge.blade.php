
<!-- KNOWLEDGE SECTION START -->
<section id="knowledge" class="section background2">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.knowledge')) }}</h2>

                    <p class="section-subtitle">{{ $proverbs->knowledge}}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <!-- BAR CHART START -->
            <div class="row">
                @foreach($knowledges as $knowledge)
                <div class="col-sm-6 col-md-6">
                    <div class="bar-chart">
                        <div class="item">
                            <h4>{{ $knowledge->name}}</h4>

                            <div class="bar">
                                <span class="percent">{{ $knowledge->pourcentage}}</span>
                                <span class="item-progress" data-percent="{{ $knowledge->pourcentage}}"></span>
                            </div>
                            <!-- //.bar -->
                        </div>
                        <!-- //.item -->
                    </div>
                    <!-- //.bar-chart -->
                </div>
                <!-- //.col-md-6 -->
                @endforeach
            </div>
            <!-- //BAR CHART END -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //KNOWLEDGE SECTION END -->