<!-- MILESTONES SECTION START -->
<section id="milestones" class="section bg-image-yes">
    <div class="container-fluid section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.stats')) }}</h2>

                    <p class="section-subtitle"> {{ $proverbs->achievement }}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <!-- MILESTONES START -->
            <div class="milestones">
                <div class="row">
                    @foreach($achievements as $achievement)
                        <div class="col-sm-6 col-md-3 item">
                            <div class="circle">
                                @if(!empty($achievement->picture))
                                    <img src="{{ asset('img/icons/achievements/'.$achievement->picture) }}" style="margin-top: 20px; border-radius: 50px;" alt="icon">
                                @else
                                    <i class="fa {{ $achievement->icon }}"></i>
                                @endif
                            </div>
                            <!-- //.circle -->
                            <span class="number" data-from="0" data-to="{{ $achievement->score }}" data-refresh-interval="100">
                                {{ $achievement->score }}
                            </span>
                            <h4>{{ $achievement->title }}</h4>
                        </div>
                        <!-- //.item -->
                    @endforeach
                </div>
                <!-- //.row -->
            </div>
            <!-- //MILESTONES END -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //MILESTONES SECTION END -->