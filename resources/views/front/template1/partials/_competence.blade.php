<!-- SERVICES SECTION START -->
<section id="services" class="section background1">
    <div class="container section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.services')) }}</h2>

                    <p class="section-subtitle">
                        {{ $proverbs->competence }}
                    </p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <!-- SERVICES START -->
            <div class="services">
                <div class="row">
                    @foreach($competences as $competence)
                        <div class="col-sm-4 col-md-4 item top match-height background2">
                            <div class="inner-content">
                                @if(!empty($competence->picture))
                                    <img src="{{ asset('img/icons/competences/'.$competence->picture) }}" style="border-radius: 50px;" alt="icon">
                                @elseif(!empty($competence->icon))
                                    <i class="fa {{ $competence->icon }}"></i>
                                @endif

                                <h4>{{ $competence->name }}</h4>

                                <p class="text-color">
                                    {{ $competence->description }}
                                </p>
                            </div>
                            <!-- //.inner-content -->
                        </div>
                    @endforeach
                </div>
                <!-- //.row -->
            </div>
            <!-- //SERVICES END -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //SERVICES SECTION END -->