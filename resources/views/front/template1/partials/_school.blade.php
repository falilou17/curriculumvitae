<!-- EDUCATION START -->
<div class="row education">
    <div class="col-md-10 col-md-offset-1">
        <h3>
            {{ ucfirst(trans('messages.myeducation')) }}
            <br>
            {{ $schools->min('dateStart') }} - {{ $schools->max('dateEnd') }}
        </h3>
        <div class="panel-group resume" id="education">
            @foreach($schools as $school)
                <div class="resume-item">
                    <div class="resume-year">
                        <span class="resume-year">{{ $school->dateStart }} - {{ $school->dateEnd }}</span>
                    </div>
                    <!-- //.resume-year -->

                    <div class="resume-btn background3">
                        <a href="#education{{$school->id}}" data-toggle="collapse" data-parent="#education"></a>
                    </div>
                    <!-- //.resume-btn -->

                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4 class="resume-title">{{ $school->diploma }} - {{ $school->name }}</h4>
                            </div>
                            <!-- //.panel-title -->
                        </div>
                        <!-- //.panel-heading -->

                        <div id="education{{$school->id}}" class="panel-collapse collapse in">
                            <div class="panel-body text-color">
                                <p>
                                    {{ $school->description }}
                                </p>
                            </div>
                            <!-- //.panel-body -->
                        </div>
                        <!-- //.panel-collapse -->
                    </div>
                    <!-- //.panel -->
                </div>
                <!-- //.resume-item -->

            @endforeach
        </div>
        <!-- //.panel-group -->
    </div>
    <!-- //.col-md-10 -->
</div>
<!-- //EDUCATION END -->