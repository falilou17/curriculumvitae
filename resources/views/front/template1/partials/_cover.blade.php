<header id="home" class="hero">
    <div class="hero-body">
        <div class="hero-text general-color text-center">
            <h1>
                @if(!empty($profile->firstName) && !empty($profile->lastName))
                    I am
                @endif
                 {{ $profile->firstName." ".strtoupper($profile->lastName) }} <span class="blinker"></span>
            </h1>

            <h1 class="small general-color">{{ $profile->job }}</h1>

            <div>{!! $profile->coverDesc !!}</div>

            <div class="page-scroll">
                <a href="#profile" class="btn btn-lg btn-custom">
                    {{ trans('messages.knowmebetter') }}
                </a>
            </div>
        </div>
    </div>
</header>
