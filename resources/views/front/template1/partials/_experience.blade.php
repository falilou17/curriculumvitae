<!-- WORK EXPERIENCE START -->
<div class="row work-experience">
    <div class="col-md-10 col-md-offset-1">
        <h3>
            {{ ucfirst(trans('messages.workexperience')) }}
            <br>
            {{ $experiences->min('dateStart') }} - {{ $experiences->max('dateEnd') }}
        </h3>
        <div class="panel-group resume" id="work">
            @foreach($experiences as $experience)
                <div class="resume-item">
                    <div class="resume-year">
                        <span class="resume-year"> {{ $experience->dateStart }} - {{ $experience->dateEnd }}</span>
                    </div>
                    <!-- //.resume-year -->

                    <div class="resume-btn background3">
                        <a href="#work{{ $experience->id }}" data-toggle="collapse" data-parent="#work"></a>
                    </div>
                    <!-- //.resume-btn -->

                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4 class="resume-title">{{ $experience->profession}} - {{ $experience->name}}</h4>
                            </div>
                            <!-- //.panel-title -->
                        </div>
                        <!-- //.panel-heading -->

                        <div id="work{{ $experience->id }}" class="panel-collapse collapse in">
                            <div class="panel-body text-color">
                                <p>
                                    @if(!empty($experience->description))
                                        {{ ucfirst(trans('messages.responsibilities')) }}:
                                    @endif
                                    <br>
                                    {{ $experience->description }}
                                </p>
                            </div>
                            <!-- //.panel-body -->
                        </div>
                        <!-- //.panel-collapse -->
                    </div>
                    <!-- //.panel -->
                </div>
                <!-- //.resume-item -->
            @endforeach
        </div>
        <!-- //.panel-group -->
    </div>
    <!-- //.col-md-10 -->
</div>
<!-- //WORK EXPERIENCE END -->