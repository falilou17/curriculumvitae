<!-- STRENGTHS SECTION START -->
<section id="strengths" class="section bg-image-yes">
    <div class="container-fluid section-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="section-title">{{ ucfirst(trans('messages.mystrengths')) }}</h2>

                    <p class="section-subtitle">{{ $proverbs->strength }}</p>

                    <span class="divider center"></span>
                </div>
                <!-- //.col-md-8 -->
            </div>
            <!-- //.row -->

            <div class="row">
                <div class="col-md-12">

                    <!-- COLUMN CHART START -->
                    <div class="column-chart">
                        <div class="legend legend-left hidden-xs">
                            <h3 class="legend-title">{{ ucfirst(trans('messages.me')) }}</h3>
                        </div>
                        <!-- //.legend -->

                        <div class="legend legend-right hidden-xs">
                            <div class="item">
                                <h4>{{ ucfirst(trans('messages.superhero')) }}</h4>
                            </div>
                            <!-- //.item -->

                            <div class="item">
                                <h4>{{ ucfirst(trans('messages.preetygood')) }}</h4>
                            </div>
                            <!-- //.item -->

                            <div class="item">
                                <h4>{{ ucfirst(trans('messages.good')) }}</h4>
                            </div>
                            <!-- //.item -->

                            <div class="item">
                                <h4>{{ ucfirst(trans('messages.newbie')) }}</h4>
                            </div>
                            <!-- //.item -->
                        </div>
                        <!-- //.legend -->

                        <div class="chart clearfix">
                            @foreach($strengths as $strength)
                                <div class="item">
                                    <div class="bar">
                                        <span class="percent">{{ $strength->pourcentage }}</span>

                                        <div class="item-progress" data-percent="{{ $strength->pourcentage }}">
                                            <span class="title">{{ $strength->name }}</span>
                                        </div>
                                        <!-- //.item-progress -->
                                    </div>
                                    <!-- //.bar -->
                                </div>
                                <!-- //.item -->
                            @endforeach
                        </div>
                        <!-- //.chart -->
                    </div>
                    <!-- //COLUMN CHART END -->

                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container -->
</section>
<!-- //STRENGTHS SECTION END -->