<nav id="navigation" class="navbar navbar-fixed-top center-menu background2" role="navigation">
    <div class="container navbar-container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle background1" data-toggle="collapse" data-target=".berg-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand general-color visible-xs visible-sm" href="#page-top"></a>
        </div>
        <!-- //.navbar-header -->

        <div class="navbar-collapse collapse berg-collapse background2">
            <ul class="nav navbar-nav general-color">
                <li class="page-scroll">
                    <a href="#home">{{ ucfirst(trans('messages.home')) }}</a>
                </li>

                <li class="page-scroll">
                    <a href="#profile">{{ ucfirst(trans('messages.profile')) }}</a>
                </li>

                @if(count($portfolios) > 0)
                    <li class="page-scroll">
                        <a href="#portfolio">{{ ucfirst(trans('messages.portfolio')) }}</a>
                    </li>
                @endif

                @if(count($competences) > 0)
                    <li class="page-scroll">
                        <a href="#services">{{ ucfirst(trans('messages.services')) }}</a>
                    </li>
                @endif

                @if(count($schools) > 0 || count($experiences) > 0)
                    <li class="page-scroll">
                        <a href="#resume">{{ ucfirst(trans('messages.resume')) }}</a>
                    </li>
                @endif

                @if(count($references) > 0)
                    <li class="page-scroll">
                        <a href="#references">{{ ucfirst(trans('messages.testimonials')) }}</a>
                    </li>
                @endif

                @if(count($skills) > 0)
                    <li class="page-scroll">
                        <a href="#skills">{{ ucfirst(trans('messages.skills')) }}</a>
                    </li>
                @endif
                @if(!empty($profile->email) && !$profile->hideContact)
                    <li class="page-scroll">
                        <a href="#contact">{{ ucfirst(trans('messages.contact')) }}</a>
                    </li>
                @endif
            </ul>
        </div>
        <!-- //.navbar-collapse -->
    </div>
    <!-- //.navbar-container -->
</nav>