<!DOCTYPE html>
<html>
<head>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <meta charset="utf-8">

    <!-- Site Title -->
    <title>{{ $profile->siteTitle }}</title>

    <meta name="description" content="{{ $profile->siteDescription }}">
    <meta name="keywords" content="{{ $profile->siteKeywords }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap Core CSS -->
    <link href=" {{ asset('front/template1/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Font icons CSS -->
    <link href=" {{ asset('front/template1/plugins/icons-mind/style.css') }}" rel="stylesheet" type="text/css">
    <link href=" {{ asset('front/template1/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css">

    <!-- Plugins CSS -->
    <link href=" {{ asset('front/template1/plugins/jpreloader/css/jpreloader.css') }}" rel="stylesheet" type="text/css">
    <link href=" {{ asset('front/template1/plugins/animate-css/animate.min.css') }}" rel="stylesheet" type="text/css">
    <link href=" {{ asset('front/template1/plugins/magnificPopup/magnific-popup.css') }}" rel="stylesheet" type="text/css">
    <link href=" {{ asset('front/template1/plugins/flexSlider/flexslider.css') }}" rel="stylesheet" type="text/css">

    <!-- Main CSS -->
    <link href=" {{ asset('front/template1/css/berg.css') }}" rel="stylesheet" type="text/css">
    <style>
        .general-color, .general-color > li > a {
            color: {{ $designs->title }};
        }

        .nav > li.page-scroll :hover {
            background-color: {{ $designs->background }}  !important;
        }

        header.hero {
            background-image: url({{ asset('img/'.$profile->coverPicture) }});
        }

        .text-color {
            color: {{ $designs->description }};
        }

        .background1 {
            background-color: {{ $designs->background }};
        }

        .background2 {
            background-color: {{ $designs->backgroundDiv }};
        }

        .background3 {
            background-color: {{ $designs->faBackground }};
        }

        .element-color, #jpreBar, .btn-custom, span.divider, .resume-year, .column-chart > .chart > .item > .bar > .item-progress, #references .flexslider.references > .slides > .item .profile:before, .circle-chart .item > .percent, .bar-chart > .item > .bar > .percent, .accolades > .item > i, .milestones .item > .circle, .portfolio .item > .project-wrapper > .project-link > a.external-link, .navbar.navbar-fixed-top .navbar-toggle {
            background-color: {{ $designs->element }};
        }

        #strengths {
            background-image: url("../img/wallpapers/strengths/{{ $wallpaper->strength }}");
        }

        #references {
            background-image: url("../img/wallpapers/references/{{ $wallpaper->reference }}");
        }

        #milestones {
            background-image: url("../img/wallpapers/achievements/{{ $wallpaper->achievement }}");
        }
    </style>
</head>
<body class="general-color">

@include('front.template1.partials._cover')
@include('front.template1.partials._navbar')
@include('front.template1.partials._profile')
@if(count($portfolios) > 0)
    @include('front.template1.partials._portfolio')
@endif
@if( count($competences) > 0)
    @include('front.template1.partials._competence')
@endif
@if( count($strengths) > 0)
    @include('front.template1.partials._strength')
@endif
@if( count($schools) > 0 || count($experiences) > 0)
    @include('front.template1.partials._resume')
@endif
@if( count($references) > 0)
    @include('front.template1.partials._reference')
@endif
@if( count($skills) > 0)
    @include('front.template1.partials._skills')
@endif
@if( count($knowledges) > 0)
    @include('front.template1.partials._knowledge')
@endif
@if( count($awards) > 0)
    @include('front.template1.partials._award')
@endif
@if( count($achievements) > 0)
    @include('front.template1.partials._achievement')
@endif
@if(!empty($profile->email) && !$profile->hideContact)
    @include('front.template1.partials._contact')
@endif

@include('front.template1.partials._footer')
@include('front.template1.partials._modalCv')
        <!-- Plugins JS -->
<script src=" {{ asset('front/template1/plugins/jquery.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/jpreloader/js/jpreloader.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/detectmobilebrowser/detectmobilebrowser.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/debouncer/debouncer.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/easing/jquery.easing.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/sticky/jquery.sticky.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/inview/jquery.inview.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/matchHeight/jquery.matchHeight-min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/magnificPopup/jquery.magnific-popup.min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/flexSlider/jquery.flexslider-min.js') }}"></script>
<script src=" {{ asset('front/template1/plugins/countTo/jquery.countTo.js') }}"></script>

<!-- Main JS -->
<script src=" {{ asset('front/template1/js/main.js') }}"></script>

<!-- Animation JS (Optional) -->
<script src=" {{ asset('front/template1/js/animation.js') }}"></script>
<script>
    function LightenDarkenColor(col, amt) {
        var usePound = false;
        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }
        var num = parseInt(col, 16);
        var r = (num >> 16) + amt;
        if (r > 255) r = 255;
        else if (r < 0) r = 0;
        var b = ((num >> 8) & 0x00FF) + amt;
        if (b > 255) b = 255;
        else if (b < 0) b = 0;
        var g = (num & 0x0000FF) + amt;
        if (g > 255) g = 255;
        else if (g < 0) g = 0;
        return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
    }
    $(document).ready(function () {
        $(".btn-custom").css('box-shadow', "3px 4px 0" + (LightenDarkenColor("{{ $designs->element }}", 50)).toString());
        $(".dark-bg").css('background-color', (LightenDarkenColor("{{ $designs->background }}", -8)).toString());
        $(".btn-custom").mouseenter(function () {
            $(this).css('background-color', (LightenDarkenColor("{{ $designs->element }}", -15)).toString());
        });
        $(".btn-custom").mouseleave(function () {
            $(this).css('background-color', (LightenDarkenColor("{{ $designs->element }}", 0)).toString());
        });3
    });
</script>

</body>
</html>
