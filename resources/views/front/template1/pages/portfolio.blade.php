<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <!-- Site Title -->
    <title>{{ $profile->siteTitle }}</title>

    <meta name="description" content="{{ $profile->siteDescription }}">
    <meta name="keywords"
          content="{{ $profile->siteKeywords }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('front/template1/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Font icons CSS -->
    <link href="{{ asset('front/template1/plugins/icons-mind/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/template1/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Plugins CSS -->
    <link href="{{ asset('front/template1/plugins/jpreloader/css/jpreloader.css') }}" rel="stylesheet" type="text/css">

    <!-- Main CSS -->
    <link href="{{ asset('front/template1/css/berg.css') }}" rel="stylesheet" type="text/css">

    <!-- Single Portfolio CSS -->
    <link href="{{ asset('front/template1/css/single-portfolio-1.css') }}" rel="stylesheet" type="text/css">

    <!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 8]>
    <script src="plugins/modernizr.min.js"></script>
    <![endif]-->
    <style>
        .list-unstyled > li > p{
            color: {{ $designs->description }}  !important;
        }
        .text-color{
            color: {{ $designs->title }}  !important;
        }
        .background1{
            background-color: {{ $designs->background }};
        }
        .background2{
            background-color: {{ $designs->backgroundDiv }};
        }
    </style>
</head>

<body class="background2">
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- SINGLE PORTFOLIO SECTION START -->
<section id="single-portfolio" class="section">
    <div class="container-fluid section-wrapper project-description-wrapper">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="project-description background1">
                        <div class="project-nav clearfix">
                            <div class="project-nav-link pull-right">
                                <a href="{{ route('index') }}?lang={{ $lang ?? 'fr' }}"><i class="icon-Close"></i></a>
                            </div>
                            <!-- //.project-nav-link -->
                        </div>
                        <!-- //.project-nav -->

                        <div class="project-details text-center">
                            <h3 class="project-title text-color">
                                {{ $portfolio->name }}
                            </h3>

                            <ul class="list-unstyled">
                                <li>

                                    <h5 class="text-color">{{ ucfirst(trans('messages.basicinfo')) }}:</h5>
                                    <p>{{ $portfolio->description }}</p>
                                </li>

                                <li>
                                    <h5 class="text-color">{{ ucfirst(trans('messages.published')) }}:</h5>
                                    <p>{{ date_format(date_create($portfolio->datePublish), 'd F Y') }}</p>
                                </li>

                                <li>
                                    <h5 class="text-color">{{ ucfirst(trans('messages.technologies')) }}:</h5>
                                    <p>{{ $portfolio->technologies }}</p>
                                </li>
                                @if(!empty($portfolio->client))
                                <li>
                                    <h5 class="text-color">{{ ucfirst(trans('messages.client')) }}:</h5>
                                    <p>{{ $portfolio->client }}</p>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <!-- //.project-details -->
                    </div>
                    <!-- //.project-description -->
                </div>
                <!-- //.col-md-12 -->
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container-fluid -->

    <div class="container-fluid section-wrapper showcase-wrapper">
        <div class="section-content background2">
            <div class="row">
                @foreach($medias as $media)
                    <div class="col-md-12 showcase">
                        <div class="showcase-media">
                            <img src="{{ asset('img/portfolio/'.$media->path) }}" alt="portfolio-img" class="img-responsive img-thumbnail"/>
                        </div>
                        <!-- //.showcase-media -->
                    </div>
                    <!-- //.col-md-12 -->
                @endforeach
            </div>
            <!-- //.row -->
        </div>
        <!-- //.section-content -->
    </div>
    <!-- //.container-fluid -->
</section>
<!-- //SINGLE PORTFOLIO SECTION END -->
<!-- Plugins JS -->
<script src="{{ asset('front/template1/plugins/jquery.min.js') }}"></script>
<script src="{{ asset('front/template1/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/template1/plugins/jpreloader/js/jpreloader.min.js') }}"></script>
<script src="{{ asset('front/template1/plugins/detectmobilebrowser/detectmobilebrowser.js') }}"></script>
<script src="{{ asset('front/template1/plugins/debouncer/debouncer.js') }}"></script>
<script src="{{ asset('front/template1/plugins/easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('front/template1/plugins/inview/jquery.inview.min.js') }}"></script>

<!-- Main JS -->
<script src="{{ asset('front/template1/js/main.js') }}"></script>
</body>
</html>