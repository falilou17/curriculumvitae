<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>{{ $profile->siteTitle }}</title>
        <meta name="description" content="{{ $profile->siteDescription }}">
        <meta name="keywords" content="{{ $profile->siteKeywords }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="{{ asset('front/templateColorLib/images/favicon.png') }}" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700,700i,900|Montserrat:400,700|PT+Serif" rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/clear.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/common.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/font-awesome.min.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/carouFredSel.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/prettyPhoto.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/sm-clean.css') }}' />
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/style.css') }}' />
        <!--[if lt IE 9]>
            <script src="{{ asset('front/templateColorLib/js/html5.js') }}"></script>
        <![endif]-->
        <style>
            .intro-page{
                background-image: url("{{ asset('img/'.$profile->coverPicture) }}");
            }
            #services {
                background-image: url("../img/wallpapers/services/{{ $wallpaper->service }}");
            }
            #portfolio {
                background-image: url("../img/wallpapers/portfolios/{{ $wallpaper->portfolio }}");
            }
            #news {
                background-image: url("../img/wallpapers/references/{{ $wallpaper->reference }}");
            }
            #skills {
                background-image: url("../img/wallpapers/strengths/{{ $wallpaper->strength }}");
            }
            #contact {
                background-image: url("../img/wallpapers/contacts/{{ $wallpaper->contact }}");
            }
        </style>
    </head>

    <body>
        <!--===================================================================
        | Loader
        |=======================================================================-->
        <table class="doc-loader">
            <tr>
                <td>
                    <img src="{{ asset('img/'.$profile->profilePicture) }}" alt="Loading..." />
                </td>
            </tr>
        </table>

        <?php $section_number = 1 ?>

        @include('front.templateColorLib.partials._navbar')

        @include('front.templateColorLib.partials._cover')

        @if(count($competences))
            @include('front.templateColorLib.partials._competence')
            <?php $section_number++ ?>
        @endif

        @if( count($portfolios) > 0)
            @include('front.templateColorLib.partials._portfolio')
            <?php $section_number++ ?>
        @endif

        @if( count($references) > 0)
            @include('front.templateColorLib.partials._reference')
            <?php $section_number++ ?>
        @endif

        @if( count($skills) > 0)
            @include('front.templateColorLib.partials._skills')
            <?php $section_number++ ?>
        @endif

        @if(!empty($profile->email) && !$profile->hideContact)
            @include('front.templateColorLib.partials._contact')
        @endif

        @include('front.templateColorLib.partials._footer')


        <!--===================================================================
        | Javascripts
        |=======================================================================-->
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.sticky-kit.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.smartmenus.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.sticky.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.dryMenu.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/isotope.pkgd.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.carouFredSel-6.2.0-packed.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.mousewheel.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.touchSwipe.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.easing.1.3.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/imagesloaded.pkgd.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.prettyPhoto.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/main.js') }}'></script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="aacc674bdf7b7b75785b34cc-|49" defer=""></script></body>
    </body>

</html>