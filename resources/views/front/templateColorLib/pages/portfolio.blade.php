<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>{{ $profile->siteTitle }}</title>
        <meta name="description" content="{{ $profile->siteDescription }}">
        <meta name="keywords" content="{{ $profile->siteKeywords }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="{{ asset('front/templateColorLib/images/favicon.png') }}"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700,700i,900|Montserrat:400,700|PT+Serif"
              rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/clear.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/common.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/font-awesome.min.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/carouFredSel.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/prettyPhoto.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/css/sm-clean.css') }}'/>
        <link rel="stylesheet" type="text/css" href='{{ asset('front/templateColorLib/style.css') }}'/>
    <!--[if lt IE 9]>
            <script src="{{ asset('front/templateColorLib/js/html5.js') }}"></script>
        <![endif]-->
    </head>

    <body class="single-portfolio">
        <!--===================================================================
        | Loader
        |=======================================================================-->
        <table class="doc-loader">
            <tr>
                <td>
                    <img src="{{ asset('front/templateColorLib/images/ajax-document-loader.gif') }}" alt="Loading..."/>
                </td>
            </tr>
        </table>

{{--        @include('front.templateColorLib.partials._navbar')--}}

        <article id="portfolio-2" class="section portfolio">
            <div class="center-relative content-1170">
                <div class="entry-content">
                    <div class="content-wrap relative">
                        <a class="absolute x-close" href="{{ route('index') }}">
                            <img src="../front/templateColorLib/images/icon_x.svg" alt="Close">
                        </a>
                        <script type="text/javascript">
                            var aboutImage_speed = "500";
                            var aboutImage_auto = "false";
                            var aboutImage_hover = "true";
                        </script>
                        <div class="image-slider-wrapper relative img aboutImage">
                            <a id="aboutImage_next" class="image_slider_next" href="#"></a>
                            <div class="caroufredsel_wrapper">
                                <ul id="aboutImage" class="image-slider slides">
                                    @foreach($medias as $media)
                                        <li><img src="{{ asset('img/portfolio/'.$media->path) }}"
                                                 alt=""></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <br>
                        <br>
                        <br>
                        <div class="one_half text-right">
                            <p>
                                <span style="color: #adadad;">{{ ucfirst(trans('messages.technologies')) }}:</span> {{ $portfolio->technologies }}<br>
                                @if(!empty($portfolio->client))
                                    <span style="color: #adadad;">{{ ucfirst(trans('messages.client')) }}:</span> {{ $portfolio->client }}<br>
                                @endif
                                <span style="color: #adadad;">{{ ucfirst(trans('messages.published')) }}:</span> {{ date_format(date_create($portfolio->datePublish), 'd F Y') }}
                            </p>
                        </div>
                        <div class="one_half last ">
                            <h1>{{ $portfolio->name }}</h1>
                            <p>
                                {{ $portfolio->description }}
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </article>

        <!--===================================================================
        | Footer
        |=======================================================================-->
        <footer>
            <div class="footer content-1170 center-relative">
                <ul>
                    <li class="copyright-footer">
                        © {{ (new DateTime())->format('Y') }} All rights reserved.
                    </li>
                    <li class="social-footer">
                        @foreach($socials as $social)
                            <a target="_blank" href="{{ $social->link }}"><span class="fa {{ $social->icon }}"></span></a>
                        @endforeach
                    </li>
                </ul>
            </div>
        </footer>

        <!--===================================================================
        | Javascripts
        |=======================================================================-->
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.sticky-kit.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.smartmenus.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.sticky.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.dryMenu.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/isotope.pkgd.js') }}'></script>
        <script type="text/javascript"
                src='{{ asset('front/templateColorLib/js/jquery.carouFredSel-6.2.0-packed.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.mousewheel.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.touchSwipe.min.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.easing.1.3.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/imagesloaded.pkgd.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/jquery.prettyPhoto.js') }}'></script>
        <script type="text/javascript" src='{{ asset('front/templateColorLib/js/main.js') }}'></script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"
                data-cf-settings="aacc674bdf7b7b75785b34cc-|49" defer=""></script>
    </body>
    </body>

</html>