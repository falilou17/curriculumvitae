<!--===================================================================
| Services
|=======================================================================-->
<div id="services" class="section">
    <div class="block content-1170 center-relative">
        <div class="section-title-holder left">
            <div class="section-num">
                <span>{{ sprintf('%02d', $section_number) }}</span>
            </div>
            <h2 class="entry-title">{{ ucfirst(trans('messages.ourservices')) }}</h2>
        </div>
        <div class="section-content-holder right">
            <div class="content-wrapper">
                <script type="text/javascript">
                    var slider1_speed = "500";
                    var slider1_auto = "false";
                    var slider1_hover = "true";
                </script>
                <div class="image-slider-wrapper relative service slider1">
                    <a id="slider1_next" class="image_slider_next" href="#"></a>
                    <ul id="slider1" class="image-slider slides">
                        <li>
                            @foreach($competences as $competence)
                                <div class="service-holder">
                                    <i class="fa fa-2x {{ $competence->icon }} tb-icon text-box-sm tb-icon-{{ ($competence->id % 6)+1 }}"></i>
                                    {{--                                    <img src="demo-images/icon_01.png" alt="">--}}
                                    <div class="service-content-holder">
                                        <div class="service-title">{{ $competence->name }}</div>
                                        <div class="service-content">
                                            {{ $competence->description }}
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class='clear'></div>
    </div>
</div>