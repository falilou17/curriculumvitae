<!--|===================================================================
| Contact
|=======================================================================|-->
<div id="contact" class="section">
    <div class="block content-1170 center-relative">
        <div class="section-title-holder left">
            <div class="section-num">
                <span>{{ sprintf('%02d', $section_number) }}</span>
            </div>
            <h2 class="entry-title">{{ ucfirst(trans('messages.contact')) }}</h2>
        </div>
        <div class="section-content-holder right">
            <div class="content-wrapper">
                <div class="one_half ">
                    <p>{{ $proverbs->contact }}</p>
                </div>
                <div class="one_half last">
                    <div class="contact-form">
                        <form action="{{ route('mail') }}" method="post" class="contact-form">
                            {{ csrf_field() }}
                            <div class="action-message">
                                <p class="alert-success contact-success">{{ ucfirst(trans('messages.messagesent')) }}!</p>
                                <p class="alert-danger contact-error">{{ ucfirst(trans('messages.fieldmissing')) }}.</p>
                            </div>

                            <p><input id="name" type="text" required name="name" placeholder="email"></p>
                            <p><input id="email" type="hidden" required name="email" value="{{ $profile->email }}"></p>
                            <p><input id="subject" type="text" required name="subject" placeholder="{{ ucfirst(trans('messages.subject')) }}"></p>
                            <p><textarea id="message" name="content" required placeholder="{{ ucfirst(trans('messages.message')) }}"></textarea></p>
                            <input type="hidden" value="1" name="ajax">
                            <p><input type="submit" value="{{ ucfirst(trans('messages.send')) }}"></p>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>