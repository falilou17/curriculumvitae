<div class="menu-wrapper center-relative">
    <nav id="header-main-menu">
        <div class="mob-menu">MENU</div>
        <ul class="main-menu sm sm-clean">
            <li><a href="#home">{{ ucfirst(trans('messages.home')) }}</a></li>
            @if(count($competences) > 0)
                <li><a href="#services">{{ ucfirst(trans('messages.services')) }}</a></li>
            @endif
            @if(count($portfolios) > 0)
                <li><a href="#portfolio">{{ ucfirst(trans('messages.portfolio')) }}</a></li>
            @endif
            @if(count($references) > 0)
                <li><a href="#news">{{ ucfirst(trans('messages.news')) }}</a></li>
            @endif
            @if(count($skills) > 0)
                <li><a href="#skills">{{ ucfirst(trans('messages.skills')) }}</a></li>
            @endif
            @if(!empty($profile->email) && !$profile->hideContact)
                <li><a href="#contact">{{ ucfirst(trans('messages.contact')) }}</a></li>
            @endif
        </ul>
    </nav>
</div>