<!--===================================================================
| PORTOFOLIO
|=======================================================================-->
<div id="portfolio" class="section">
    <div class="block content-1170 center-relative">
        <div class="section-title-holder right">
            <div class="section-num">
                <span>{{ sprintf('%02d', $section_number) }}</span>
            </div>
            <h2 class="entry-title">{{ ucfirst(trans('messages.ourworks')) }}</h2>
        </div>
        <div class="section-content-holder portfolio-holder left">
            <div class="grid" id="portfolio-grid">
                <div class="grid-sizer"></div>
                @foreach($portfolios as $portfolio)
                    <div class="grid-item element-item p_one">
                        <a href="{{ route('portfolio-show', [$portfolio->id]) }}">
                            <img src="{{ asset('img/portfolio/'.$portfolio->medias[0]->path) }}" alt="">
                            <div class="portfolio-text-holder">
                                <div class="portfolio-text-wrapper">
                                    <p class="portfolio-type">
                                        <img src="front/templateColorLib/images/icon_post.svg" alt="">
                                    </p>
                                    <p class="portfolio-text">{{ mb_strimwidth($portfolio->name, 0, 100, "...") }}</p>
                                    <p class="portfolio-sec-text">{{ $portfolio->description }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="clear"></div>
                <div class="block portfolio-load-more-holder">
                    <a target="_self" class="more-posts">{{ ucfirst(trans('messages.readmore')) }}</a>
                    <img src="front/templateColorLib/images/icon_infinity.svg" alt="Load more">
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>