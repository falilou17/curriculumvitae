<!--|===================================================================
| Testimonials
|=======================================================================|-->
<div id="news" class="section">
    <div class="block content-1170 center-relative">
        <div class="section-title-holder right">
            <div class="section-num">
                <span>{{ sprintf('%02d', $section_number) }}</span>
            </div>
            <h2 class="entry-title">{{ ucfirst(trans('messages.news')) }}</h2>
        </div>
        <div class="section-content-holder left">
            <div class="content-wrapper">
                <div class="blog-holder block center-relative">
                    @foreach($references as $k => $reference)
                        <article class="relative blog-item-holder center-relative">
                            <div class="num">{{ $k+1 }}</div>
                            <div class="info">
                                <div class="author vcard ">{{ $reference->fullName }}</div>
                                <div class="cat-links">
                                    <ul>
                                        <li><a href="#">{{ $reference->profession }} {{ $reference->enterprise ? '- '.$reference->enterprise : '' }}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <h3 class="entry-title">
                                <a href="#">{{ $reference->description }}</a>
                            </h3>
                            <div class="clear"></div>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>