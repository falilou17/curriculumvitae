<!--===================================================================
| Footer
|=======================================================================-->
<footer>
    <div class="footer content-1170 center-relative">
        <ul>
            <li class="social-footer">
                @foreach($socials as $social)
                    <a target="_blank" href="{{ $social->link }}"><span class="fa {{ $social->icon }}"></span></a>
                @endforeach
            </li>
            @if(!empty($profile->address))
                <li>
                    <u><b>{{ ucfirst(trans('messages.address')) }}</b></u>: {{ $profile->address }}
                </li>
            @endif
            @if(!empty($profile->phone))
                <li>
                    <u><b>{{ ucfirst(trans('messages.phoneNumber')) }}</b></u>: {{ $profile->phone }}
                </li>
            @endif
            <li class="copyright-footer">
                © {{ (new DateTime())->format('Y') }} All rights reserved.
            </li>
        </ul>
    </div>
</footer>