<!--|===================================================================
| Introduction Section
|=======================================================================|-->
<div id="home" class="section intro-page">
    <div class="block content-1170 center-relative center-text">
        <img class="top-logo" src="{{ asset('img/'.$profile->profilePicture) }}" alt="Boxus" />
        <br>
        <h1 class="big-title">{{ $profile->firstName." ".strtoupper($profile->lastName) }}</h1>
        <p class="title-desc">{{ $profile->coverDesc }}</p>
    </div>
</div>
