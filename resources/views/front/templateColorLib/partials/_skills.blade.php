<!--===================================================================
| SKILLS
|=======================================================================-->
<div id="skills" class="section">
    <div class="block content-1170 center-relative">
        <div class="section-title-holder right">
            <div class="section-num">
                <span>{{ sprintf('%02d', $section_number) }}</span>
            </div>
            <h2 class="entry-title">{{ ucfirst(trans('messages.skills')) }}</h2>
        </div>
        <div class="section-content-holder left">
            <div class="content-wrapper">
                <p>{{ $proverbs->skill }}</p>
                <br>
                @foreach($skills as $key => $skill)
                    <div class="progress_bar ">
                        <div class="progress_bar_field_holder" style="width:{{ $skill->pourcentage }}%;">
                            <div class="progress_bar_title progress_bar_color_{{ $key % 5 }}">{{ $skill->name }}</div>
                            <div class="progress_bar_percent_text progress_bar_color_{{ $key % 5 }}">{{ $skill->pourcentage }}%</div>
                            <div class="progress_bar_field_perecent progress_bar_color_{{ $key % 5 }}"></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>