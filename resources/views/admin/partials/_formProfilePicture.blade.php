<!-- Form for Profile Picture -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Picture </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" class="form-horizontal" action="{{ route('admin-profile-picture-save') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group col-sm-6 row">
                <div class="col-sm-12">
                    <img class="col-sm-offset-2 col-sm-8" id="previewPP" src="{{ asset('img/'.$myProfile->profilePicture) }}" alt="PP" class="img-rounded">
                </div>
                <label class="col-sm-2 control-label">Profile Picture</label>
                <div class="col-sm-10">
                    <input class="btn btn-white" name="profilePicture" id="inputPP" type="file" accept="image/*">
                </div>
            </div>
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewCP" src="{{ asset('img/'.$myProfile->coverPicture) }}" alt="PP" style="max-height:300px;" class="img-rounded">
                <label class="col-sm-2 control-label">Cover Picture</label>
                <div class="col-sm-10">
                    <input class="btn btn-white" name="coverPicture[]" id="inputCP" type="file" accept="image/*" multiple>
                </div>
            </div>
            @include('errors/error')

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button onClick="resetPreview()" class="btn btn-lg btn-danger" type="reset">Cancel</button>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>