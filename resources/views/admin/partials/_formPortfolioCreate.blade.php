<!-- Form for Profile Portfolio -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Portfolio </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-portfolio-store') }}" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input required name="name" type="text" class="form-control" value="{{ old('name') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" required name="description" rows="6">{{ old('description') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Technologies</label>
                <div class="col-sm-10">
                    <textarea class="form-control" required name="technologies" rows="6">{{ old('technologies') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Client</label>
                <div class="col-sm-10"><input name="client" type="text" class="form-control" value="{{ old('client') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Testimonial</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="testimonial" rows="5">{{ old('testimonial') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Publish Date</label>
                <div class="col-sm-10"><input type="text" name="datePublish" class="form-control datepicker" value="{{ old('datePublish') }}"></div>
            </div>

            <div>
                <label class="col-sm-offset-2 col-sm-4 control-label"><u>Pictures</u></label>
                <a class="btn btn-primary" onclick="addImg()" role="button">Add</a>
                <span id="limitImg" style="color: red;"></span>
                <div class="table-responsive col-sm-offset-2 col-sm-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Path </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="lstImg">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-10">
                                            <input class="btn btn-white" id="addImgButton" required name="media[]" accept="image/*" type="file">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p>At least this one</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @include('errors/error')

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-portfolios') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>