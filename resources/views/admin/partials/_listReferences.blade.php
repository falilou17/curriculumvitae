<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Testimonials</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-reference-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Enterprise </th>
                                <th>Full Name </th>
                                <th>Profession</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($references))
                                @foreach($references as $reference)
                                    <tr>
                                        <td>{{ mb_strimwidth($reference->enterprise, 0, 35, "...") }}</td>
                                        <td>{{ mb_strimwidth($reference->fullName, 0, 35, "...") }}</td>
                                        <td>{{ mb_strimwidth($reference->profession, 0, 35, "...") }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-reference-edit', $reference->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-reference-destroy', $reference->id) }}" role="button">Delete</a>
                                            @if($reference->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-reference-approve', $reference->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-reference-disapprove', $reference->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $references->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>