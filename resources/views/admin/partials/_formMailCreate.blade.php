@section('css')
    <link href="{{ asset('adminStuff/css/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/css/summernote-bs3.css') }}" rel="stylesheet">
@endsection

<div class="col-lg-9 animated fadeInRight">
    <div class="mail-box-header">
        <div class="pull-right tooltip-demo">
            <a href="{{ route('admin-mails','Inbox') }}" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Discard</a>
        </div>
        <h2>
            Compose mail
        </h2>
    </div>
    <form class="form-horizontal" method="post" action="{{ route('admin-mail-send') }}">
        {{ csrf_field() }}
        <div class="mail-box">
            <div class="mail-body">
                <div class="form-group"><label class="col-sm-2 control-label">To:</label>
                    <div class="col-sm-10"><input type="email" required class="form-control" name="receiver" value="{{ old('receiver') }}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Subject:</label>
                    <div class="col-sm-10"><input type="text" required class="form-control" name="object" value="{{ old('object') }}"></div>
                </div>
            </div>
            <div class="mail-text h-200">
                <textarea name="content" class="summernote">
                    {{ old('content') }}
                </textarea>
                <div class="clearfix"></div>
            </div>
            <div class="mail-body text-right tooltip-demo">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-reply"></i> Send</button>
                <a href="{{ route('admin-mails','Inbox') }}" class="btn btn-danger btn-sm pull-left"><i class="fa fa-times"></i> Discard</a>
            </div>
            <div class="clearfix"></div>
            @include('errors/error')
        </div>
    </form>
</div>

@section('script')
    <script src="{{ asset('adminStuff/js/summernote.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote();
        });
        var edit = function() {
            $('.click2edit').summernote({focus: true});
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };
    </script>
@endsection