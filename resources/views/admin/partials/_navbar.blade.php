@inject('Helper', 'App\Services\BladeData')
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a target="_blank" href="{{ route('index') }}">
                    <i class="fa fa-circle-o"></i> My Site
                </a>
            </li>
            <li>
                <a href="{{ route('admin-home') }}">
                    <i class="fa fa-home"></i> Home
                </a>
            </li>
            <li>
                <a href="{{ route('admin-edit') }}">
                    <i class="fa fa-edit"></i> Edit LogIn
                </a>
            </li>
            <li>
                <a href="{{ route('admin-logout') }}">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>