<div class="col-lg-9 animated fadeInRight">
    <div class="mail-box-header">
        <div class="pull-right tooltip-demo">
            <a href="mail_compose.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Reply</a>
            <a href="{{ route('admin-mail-destroy',$mail->id) }}" class="btn btn-white btn-sm"><i class="fa fa-trash-o"></i> </a>
        </div>
        <h2>
            View Message
        </h2>
        <div class="mail-tools tooltip-demo m-t-md">
            <h3>
                <span class="font-noraml">Subject: </span>{{ $mail->object }}
            </h3>
            <h5>
                <span class="pull-right font-noraml">{{ date_format($mail->created_at, 'H:m d M Y') }}</span>
                @if($mail->sender != 'me')
                    <span class="font-noraml">From: </span>{{ $mail->sender }}
                @else
                    <span class="font-noraml">To: </span>{{ $mail->receiver }}
                @endif
            </h5>
        </div>
    </div>
    <div class="mail-box">
        <div class="mail-body">
            <p>
               {!! $mail->content !!}
            </p>
        </div>
        <div class="mail-body text-right tooltip-demo">
            <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i> Reply</a>
            <a class="btn btn-sm btn-white" href="{{ route('admin-mail-destroy',$mail->id) }}"><i class="fa fa-trash-o"></i> Remove</a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>