<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Strengths (Only the first five will be displayed)</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-strength-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Pourcentage </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($strengths))
                                @foreach($strengths as $strength)
                                    <tr>
                                        <td>{{ $strength->name }}</td>
                                        <td>{{ $strength->pourcentage }}%</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-strength-edit', $strength->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-strength-destroy', $strength->id) }}" role="button">Delete</a>
                                            @if($strength->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-strength-approve', $strength->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-strength-disapprove', $strength->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $strengths->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>