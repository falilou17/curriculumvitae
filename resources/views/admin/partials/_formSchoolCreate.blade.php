<!-- Form for Profile School -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>School </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-school-store') }}" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input name="name" type="text" class="form-control" value="{{ old('name') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Diploma</label>
                <div class="col-sm-10"><input name="diploma" type="text" class="form-control" value="{{ old('diploma') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Date</label>
                <div class="col-sm-10">
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" name="dateStart" required class="input-sm form-control" value="{{ old('dateStart') }}">
                        <span class="input-group-addon">to</span>
                        <input type="text" name="dateEnd" required class="input-sm form-control" value="{{ old('dateEnd') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="6">{{ old('description') }}</textarea>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-schools') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>