<!-- Form for Profile Portfolio -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Portfolio </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-portfolio-update', $portfolio->id) }}" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input required name="name" type="text" class="form-control" value="{{ $portfolio->name }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" required name="description" rows="6">{{ $portfolio->description }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Technologies</label>
                <div class="col-sm-10">
                    <textarea class="form-control" required name="technologies" rows="6">{{ $portfolio->technologies }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Client</label>
                <div class="col-sm-10"><input name="client" type="text" class="form-control" value="{{ $portfolio->client }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Testimonial</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="testimonial" rows="5">{{ $portfolio->testimonial }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Publish Date</label>
                <div class="col-sm-10"><input type="text" name="datePublish" value="{{ $portfolio->datePublish }}" class="form-control datepicker"></div>
            </div>
            <div class="col-sm-12">
                <label class="col-sm-offset-2 col-sm-4 control-label"><u>Pictures</u></label>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="jumbotron">
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <ul class="list-group ImgDraggable">
                                    @for($i=0; $i<5; $i++)
                                        <li class="col-sm-2">
                                            @if(count($medias) > $i)
                                                <img style="border: 1px outset #c80a48; width: 100%; height: 100px" id="previewPortf{{ $i }}" src="{{ asset('img/portfolio/'.$medias[$i]->path) }}" alt="PP" class="img-rounded">
                                                <div>
                                                    <input class="btn" id="addImgButton{{ $i }}"  name="media[]" accept="image/*" type="file">
                                                    <input type="hidden" name="FNMedia[]" value="{{ $medias[$i]->path }}">
                                                    <p class="text-center"><a id="AddBtnPortf{{ $i }}" class="btn btn-success col-sm-12" role="button">Change</a></p>
                                                    <p class="text-center"><a href="{{ route('admin-portfolio-destroy-picture', $medias[$i]->id) }}" class="btn btn-danger col-sm-12" role="button">Delete</a></p>
                                                </div>
                                            @else
                                                <img style="border: 1px outset #c80a48; width: 100%; height: 100px" id="previewPortf{{ $i }}" src="{{ asset('img/portfolio/portfolio.png') }}" alt="PP" class="img-rounded">
                                                <div>
                                                    <input class="btn" id="addImgButton{{ $i }}"  name="media[]" accept="image/*" type="file">
                                                    <input type="hidden" name="FNMedia[]" value="null">
                                                    <p class="text-center"><a id="AddBtnPortf{{  $i }}" class="btn btn-primary col-sm-12" role="button">Add</a></p>
                                                </div>
                                            @endif
                                        </li>
                                    @endfor
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('errors/error')

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-portfolios') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>