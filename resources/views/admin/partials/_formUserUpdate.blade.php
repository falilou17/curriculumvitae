<!-- Form for Award -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>User </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-user-update', $user->id) }}" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Login</label>
                <div class="col-sm-10"><input type="text" required name="login" class="form-control" value="{{ $user->login }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Domain</label>
                <div class="col-sm-10"><input type="text" name="domain" class="form-control" value="{{ $user->domain }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10"><input type="password" name="password" class="form-control"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Confirm Password</label>
                <div class="col-sm-10"><input type="password" name="cpassword" class="form-control"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Admin</label>
                <div class="col-sm-10"><input type="checkbox" name="isAdmin" {{ $user->isAdmin == 1 ? 'checked' : '' }} class="form-control"></div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-users') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>