<!-- Form for Profile Contact -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Contact </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" class="form-horizontal" action="{{ route('admin-profile-contact-save') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10"><input name="email" type="email" class="form-control"value="{{ !empty($myProfile->email) ? $myProfile->email : old('email') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Web Site</label>
                <div class="col-sm-10"><input name="webSite" type="text" class="form-control" value="{{ !empty($myProfile->webSite) ? $myProfile->webSite : old('webSite') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Phone number</label>
                <div class="col-sm-10"><input name="phone" type="text" class="form-control" value="{{ !empty($myProfile->phone) ? $myProfile->phone : old('phone') }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="address" rows="2">{{ !empty($myProfile->address) ? $myProfile->address : old('address') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Cacher formulaire de contact</label>
                <div class="col-sm-2">
                    <label for="hideContact1">Oui</label>
                    <input class="form-control" type="radio" id="hideContact1" name="hideContact" value="1" {{ !empty($myProfile->hideContact) ? 'checked' : '' }}>
                </div>
                <div class="col-sm-2">
                    <label for="hideContact2">Non</label>
                    <input class="form-control" type="radio" id="hideContact2" name="hideContact" value="0" {{ empty($myProfile->hideContact) ? 'checked' : '' }}>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-lg btn-danger" type="reset">Cancel</button>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>