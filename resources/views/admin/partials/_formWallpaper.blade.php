<!-- Form for Profile Picture -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Picture </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" class="form-horizontal" action="{{ route('admin-wallpapers-update') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewStrength" src="{{ asset('img/wallpapers/strengths/'.$wallpaper->strength) }}" alt="PP" style="height:240px;" class="img-rounded">
                <label class="col-sm-3 text-center control-label">{{ $template->id != 3 ? 'Strength' : 'Skill' }} Wallpaper</label>
                <div class="col-sm-9">
                    <input class="btn btn-white" name="strength" id="inputStrength" type="file" accept="image/*">
                </div>
            </div>
            @if($template->id != 3)
                <div class="form-group col-sm-6 row">
                    <img class="col-sm-12" id="previewAchievement" src="{{ asset('img/wallpapers/achievements/'.$wallpaper->achievement) }}" alt="PP" style="height:240px;" class="img-rounded">
                    <label class="col-sm-3 text-center control-label">Stat Wallpaper</label>
                    <div class="col-sm-9">
                        <input class="btn btn-white" name="achievement" id="inputAchievement" type="file" accept="image/*">
                    </div>
                </div>
            @endif
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewReference" src="{{ asset('img/wallpapers/references/'.$wallpaper->reference) }}" alt="PP" style="height:240px;" class="img-rounded">
                <label class="col-sm-3 text-center control-label">Testimonial Wallpaper</label>
                <div class="col-sm-9">
                    <input class="btn btn-white" name="reference" id="inputReference" type="file" accept="image/*">
                </div>
            </div>
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewPortfolio" src="{{ asset('img/wallpapers/portfolios/'.$wallpaper->portfolio) }}" alt="PP" style="height:240px;" class="img-rounded">
                <label class="col-sm-3 text-center control-label">Portfolio Wallpaper</label>
                <div class="col-sm-9">
                    <input class="btn btn-white" name="portfolio" id="inputPortfolio" type="file" accept="image/*">
                </div>
            </div>
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewContact" src="{{ asset('img/wallpapers/contacts/'.$wallpaper->contact) }}" alt="PP" style="height:240px;" class="img-rounded">
                <label class="col-sm-3 text-center control-label">Contact Wallpaper</label>
                <div class="col-sm-9">
                    <input class="btn btn-white" name="contact" id="inputContact" type="file" accept="image/*">
                </div>
            </div>
            <div class="form-group col-sm-6 row">
                <img class="col-sm-12" id="previewService" src="{{ asset('img/wallpapers/services/'.$wallpaper->service) }}" alt="PP" style="height:240px;" class="img-rounded">
                <label class="col-sm-3 text-center control-label">Service Wallpaper</label>
                <div class="col-sm-9">
                    <input class="btn btn-white" name="service" id="inputService" type="file" accept="image/*">
                </div>
            </div>
            @if($template->id != 3)
                <div class="form-group col-sm-6 row">
                    <img class="col-sm-12" id="previewProfile" src="{{ asset('img/wallpapers/profiles/'.$wallpaper->profile) }}" alt="PP" style="height:240px;" class="img-rounded">
                    <label class="col-sm-3 text-center control-label">Profile Wallpaper</label>
                    <div class="col-sm-9">
                        <input class="btn btn-white" name="profile" id="inputProfile" type="file" accept="image/*">
                    </div>
                </div>
            @endif
            @include('errors/error')

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button onClick="resetPreview()" class="btn btn-lg btn-danger" type="reset">Cancel</button>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>