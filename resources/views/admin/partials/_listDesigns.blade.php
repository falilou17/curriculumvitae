<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Designs</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-design-create') }}" role="button">New</a>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 12%">Background 1</th>
                            <th style="width: 12%">Background 2</th>
                            <th style="width: 12%">Background 3</th>
                            <th style="width: 12%">Title</th>
                            <th style="width: 12%">Description</th>
                            <th style="width: 12%">Element</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($designs))
                                @foreach($designs as $design)
                                    <tr>
                                        <td style="background-color: {{ $design->background }}"></td>
                                        <td style="background-color: {{ $design->backgroundDiv }}"></td>
                                        <td style="background-color: {{ $design->faBackground }}"></td>
                                        <td style="background-color: {{ $design->title }}"></td>
                                        <td style="background-color: {{ $design->description }}"></td>
                                        <td style="background-color: {{ $design->element }}"></td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-design-edit', $design->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-design-destroy', $design->id) }}" role="button">Delete</a>
                                            @if($design->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-design-approve', $design->id) }}" role="button">Active</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $designs->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>