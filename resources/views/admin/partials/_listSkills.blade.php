<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Skills</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-skill-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Pourcentage </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($skills))
                                @foreach($skills as $skill)
                                    <tr>
                                        <td>{{ $skill->name }}</td>
                                        <td>{{ $skill->pourcentage }}%</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-skill-edit', $skill->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-skill-destroy', $skill->id) }}" role="button">Delete</a>
                                            @if($skill->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-skill-approve', $skill->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-skill-disapprove', $skill->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $skills->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>