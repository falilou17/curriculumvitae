<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Links (Social Network)</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-social-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title </th>
                                <th>Icon </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($socials))
                                @foreach($socials as $social)
                                    <tr>
                                        <td>{{ mb_strimwidth($social->title, 0, 70, "...") }}</td>
                                        <td><i class="fa {{ $social->icon }}"></i></td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-social-edit', $social->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-social-destroy', $social->id) }}" role="button">Delete</a>
                                            @if($social->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-social-approve', $social->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-social-disapprove', $social->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $socials->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>