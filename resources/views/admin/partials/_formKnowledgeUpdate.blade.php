<!-- Form for Knowledge -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Knowledge </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-knowledge-update', $knowledge->id) }}" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input type="text" name="name" class="form-control" value="{{ $knowledge->name }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Pourcentage</label>
                <div class="col-sm-10"><input type="number" name="pourcentage" min="0" max="100" class="form-control" value="{{ $knowledge->pourcentage }}"></div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-knowledges') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>