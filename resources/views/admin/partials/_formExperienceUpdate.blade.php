<!-- Form for Profile Experience -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Experience </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-experience-update', $experience->id) }}" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input type="text" name="name" class="form-control" value="{{ $experience->name }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Profession</label>
                <div class="col-sm-10"><input type="text" name="profession" class="form-control" value="{{ $experience->profession }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Date</label>
                <div class="col-sm-10">
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" name="dateStart" required class="input-sm form-control" value="{{ $experience->dateStart }}" />
                        <span class="input-group-addon">to</span>
                        <input type="text" name="dateEnd" required class="input-sm form-control" value="{{ $experience->dateEnd }}"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="6">{{ $experience->description }}</textarea>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-experiences') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>