<div class="col-lg-9 animated fadeInRight">
    <div class="mail-box-header">

        <form method="get" action="http://webapplayers.com/inspinia_admin-v2.3/index.html" class="pull-right mail-search">
            <div class="input-group">
                <input type="text" class="form-control input-sm" name="search" placeholder="Search email">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Search
                    </button>
                </div>
            </div>
        </form>
        <h2>
            {{ $home.' '.'('. count($mails) .')' }}
        </h2>
        <div class="mail-tools tooltip-demo m-t-md">
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>
        </div>
    </div>
    <div class="mail-box">
        <table class="table table-hover table-mail">
            <tbody>
            @foreach($mails as $mail)
                <tr class="{{ $mail->read == 1 ? 'read' : 'unread' }}">
                    <td class="check-mail">
                        @if($home == 'Inbox')
                            @if($mail->read == 1)
                                <a href="{{ route('admin-mail-unread',$mail->id) }}"><i class="fa fa-eye-slash"></i></a>
                            @else
                                <a href="{{ route('admin-mail-read',$mail->id) }}"><i class="fa fa-eye"></i></a>
                            @endif
                        @endif
                    </td>
                    <td class="mail-contact"><a href="{{ route('admin-mail-show',$mail->id) }}"><strong>{{ mb_strimwidth($mail->object, 0, 30, '...') }}</strong></a></td>
                    <td class="mail-subject" style="width: 70%"><a href="{{ route('admin-mail-show',$mail->id) }}">{!! mb_strimwidth($mail->content, 0, 400, '') !!}</a></td>
                    <td class=""><a href="{{ route('admin-mail-destroy',$mail->id) }}"><i class="fa fa-trash-o"></i></a></td>
                    <td class="text-right mail-date">{{ date_format($mail->created_at, 'd M y') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {!! $mails->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>