<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Experiences</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-experience-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Profession </th>
                                <th>Date Start</th>
                                <th>Date End </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($experiences))
                                @foreach($experiences as $experience)
                                    <tr>
                                        <td>{{ mb_strimwidth($experience->name, 0, 35, "...") }}</td>
                                        <td>{{ mb_strimwidth($experience->profession, 0, 20, "...") }}</td>
                                        <td>{{ $experience->dateStart }}</td>
                                        <td>{{ $experience->dateEnd }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-experience-edit', $experience->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-experience-destroy', $experience->id) }}" role="button">Delete</a>
                                            @if($experience->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-experience-approve', $experience->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-experience-disapprove', $experience->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $experiences->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>