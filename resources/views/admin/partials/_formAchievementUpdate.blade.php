<!-- Form for Achievement -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Stat </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-achievement-update', $achievement->id) }}" class="form-horizontal"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10"><input required name="title" type="text" class="form-control"
                                              value="{{ $achievement->title }}"></div>
            </div>
            @if(empty($achievement->picture))
                <div class="form-group">
                    <label class="col-sm-2 control-label">Icon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input data-placement="bottomRight" readonly name="icon" class="form-control icp icp-auto"
                                   value="{{ $achievement->icon }}" type="text"/>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="col-sm-2 control-label">Score</label>
                <div class="col-sm-10"><input type="number" required name="score" min="0" max="2000000000"
                                              class="form-control" value="{{ $achievement->score }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Picture</label>
                <div class="col-sm-10">
                    <img class="col-sm-offset-2 col-sm-8" style="width: 70px" id="previewIcon"
                         src="{{ !empty($achievement->picture) ? asset('img/icons/achievements/'.$achievement->picture) : asset('img/icons/picture-icon.png') }}"
                         alt="PP" class="img-rounded">
                    <input class="btn btn-white" name="picture" id="inputIcon" type="file" accept="image/*">
                </div>
            </div>
            @if(!empty($achievement->picture))
                <div class="form-group">
                    <label class="col-sm-2 control-label">Supprimer la photo<input type="checkbox" name="delete_picture"></label>
                </div>
            @endif
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-achievements') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>