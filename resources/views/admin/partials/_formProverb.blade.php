<!-- Form for Proverb -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Proverb </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-proverb-update') }}" class="form-horizontal">
            {{ csrf_field() }}
            @if($template->id != 3)
                <div class="form-group">
                    <label class="col-sm-2 control-label">Portfolio</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="portfolio" rows="2">{{ !empty($proverb->portfolio) ? $proverb->portfolio : old('portfolio')  }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Service</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="competence" rows="2">{{ !empty($proverb->competence) ? $proverb->competence : old('competence')  }}</textarea>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Strength</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="strength" rows="2">{{ !empty($proverb->strength) ? $proverb->strength : old('strength') }}</textarea>
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Resume</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="resume" rows="2">{{ !empty($proverb->resume) ? $proverb->resume : old('resume') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Testimonial</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="reference" rows="2">{{ !empty($proverb->reference) ? $proverb->reference : old('reference') }}</textarea>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="col-sm-2 control-label">Skill</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="skill" rows="2">{{ !empty($proverb->skill) ? $proverb->skill : old('skill') }}</textarea>
                </div>
            </div>
            @if($template->id != 3)
                <div class="form-group">
                    <label class="col-sm-2 control-label">Knowledge</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="knowledge" rows="2">{{ !empty($proverb->knowledge) ? $proverb->knowledge : old('knowledge') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Award</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="award" rows="2">{{ !empty($proverb->award) ? $proverb->award : old('award') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Achievement</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="achievement" rows="2">{{ !empty($proverb->achievement) ? $proverb->achievement : old('achievement') }}</textarea>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="col-sm-2 control-label">Contact</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="contact" rows="2">{{ !empty($proverb->contact) ? $proverb->contact : old('contact') }}</textarea>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-lg btn-danger" type="reset">Cancel</button>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>