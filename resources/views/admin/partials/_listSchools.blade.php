<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Schools</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-school-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Diploma </th>
                                <th>Date Start</th>
                                <th>Date End </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($schools))
                                @foreach($schools as $school)
                                    <tr>
                                        <td>{{ mb_strimwidth($school->name, 0, 35, "...") }}</td>
                                        <td>{{ mb_strimwidth($school->diploma, 0, 20, "...") }}</td>
                                        <td>{{ $school->dateStart }}</td>
                                        <td>{{ $school->dateEnd }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-school-edit', $school->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-school-destroy', $school->id) }}" role="button">Delete</a>
                                            @if($school->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-school-approve', $school->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-school-disapprove', $school->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $schools->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>