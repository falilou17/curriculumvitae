<!-- Form for Social -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Social Network </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-social-update', $social->id) }}" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10"><input type="text" required name="title" class="form-control" value="{{ $social->title }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10"><input type="text" required name="link" class="form-control" value="{{ $social->link }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Icon</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <input data-placement="bottomRight" required name="icon" class="form-control icp icp-auto" value="{{ $social->icon }}" type="text" />
                        <span class="input-group-addon"></span>
                    </div>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-socials') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>