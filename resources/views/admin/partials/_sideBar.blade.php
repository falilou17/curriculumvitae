@inject('Helper', 'App\Services\BladeData')
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a href="{{ route('admin-home') }}">
                        <img alt="image" class="img-circle center-block" width="50px" height="50px" src="{{ asset('img/'.$Helper->getAuthUser()->profilePicture) }}" />
                    </a>
                    <span class="clear text-center">
                        <span class="block m-t-xs">
                            <strong class="font-bold text-white">{{ $Helper->getAuthUser()->firstName.' '.$Helper->getAuthUser()->lastName }}</strong>
                        </span>
                        <span class="text-muted text-xs block">{{ $Helper->getAuthUser()->job }}</span>
                    </span>
                </div>
                <div class="logo-element">
                    CV
                </div>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-user"></i> <span class="nav-label">Profile</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="active"><a href="{{ route('admin-profile-me') }}"><i class="fa fa-github-alt"></i>Me</a></li>
                    <li class="active"><a href="{{ route('admin-profile-myjob') }}"><i class="fa fa-briefcase"></i>My Job</a></li>
                    <li class="active"><a href="{{ route('admin-profile-contact') }}"><i class="fa fa-phone"></i>Contact</a></li>
                    <li class="active"><a href="{{ route('admin-profile-picture') }}"><i class="fa fa-picture-o"></i>Pictures</a></li>
                    <li class="active"><a href="{{ route('admin-profile-setting') }}"><i class="fa fa-gears"></i>Settings</a></li>
                </ul>
            </li>
            @if(Auth::user()->profile->template->showMenu('award'))
                <li>
                    <a href="#">
                        <i class="fa fa-gift"></i> <span class="nav-label">Award</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-awards') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-award-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('design'))
                <li>
                    <a href="#">
                        <i class="fa fa-adjust"></i> <span class="nav-label">Design</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-designs') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-design-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('experience'))
                <li>
                    <a href="#">
                        <i class="fa fa-bank"></i> <span class="nav-label">Experience</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-experiences') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-experience-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('knowledge'))
                <li>
                    <a href="#">
                        <i class="fa fa-stack-overflow"></i> <span class="nav-label">Knowledge</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-knowledges') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-knowledge-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('portfolio'))
                <li>
                    <a href="#">
                        <i class="fa fa-book"></i> <span class="nav-label">Portfolio</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-portfolios') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-portfolio-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            <li>
                <a href="{{ route('admin-proverbs') }}">
                    <i class="fa fa-align-center"></i> <span class="nav-label">Proverb</span>
                </a>
            </li>
            @if(Auth::user()->profile->template->showMenu('school'))
                <li>
                    <a href="#">
                        <i class="fa fa-mortar-board"></i> <span class="nav-label">School</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-schools') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-school-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('service'))
                <li>
                    <a href="#">
                        <i class="fa fa-unlink"></i> <span class="nav-label">Service</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-competences') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-competence-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('skill'))
                <li>
                    <a href="#">
                        <i class="fa fa-bolt"></i> <span class="nav-label">Skill</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-skills') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-skill-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('social'))
                <li>
                    <a href="#">
                        <i class="fa fa-facebook"></i> <span class="nav-label">Social</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-socials') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-social-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('achievement'))
                <li>
                    <a href="#">
                        <i class="fa fa-calendar"></i> <span class="nav-label">Stat</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-achievements') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-achievement-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('strength'))
                <li>
                    <a href="#">
                        <i class="fa fa-star"></i> <span class="nav-label">Strength</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-strengths') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-strength-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('reference'))
                <li>
                    <a href="#">
                        <i class="fa fa-users"></i> <span class="nav-label">Testimonial</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="active"><a href="{{ route('admin-references') }}"><i class="fa fa-align-justify"></i>List</a></li>
                        <li class="active"><a href="{{ route('admin-reference-create') }}"><i class="fa fa-plus"></i>New</a></li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->profile->template->showMenu('wallpaper'))
                <li>
                    <a href="{{ route('admin-wallpapers') }}">
                        <i class="fa fa-picture-o"></i> <span class="nav-label">Wallpaper</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>