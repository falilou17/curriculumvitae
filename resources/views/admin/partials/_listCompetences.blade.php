<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Services</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-competence-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Icon </th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($competences))
                                @foreach($competences as $competence)
                                    <tr>
                                        <td>{{ mb_strimwidth($competence->name, 0, 20, "...") }}</td>
                                        @if(!empty($competence->picture))
                                            <td><img src="{{ asset('img/icons/competences/'.$competence->picture) }}" style="border-radius: 50px ;width:30px;" alt="icon"></td>
                                        @else
                                            <td><i class="fa {{ $competence->icon }}"></i></td>
                                        @endif
                                        <td >{{ mb_strimwidth($competence->description, 0, 80, "...") }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-competence-edit', $competence->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-competence-destroy', $competence->id) }}" role="button">Delete</a>
                                            @if($competence->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-competence-approve', $competence->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-competence-disapprove', $competence->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $competences->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>