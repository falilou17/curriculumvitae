<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Knowledges</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-knowledge-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Pourcentage </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($knowledges))
                                @foreach($knowledges as $knowledge)
                                    <tr>
                                        <td>{{ mb_strimwidth($knowledge->name, 0, 80, "...") }}</td>
                                        <td>{{ $knowledge->pourcentage }}%</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-knowledge-edit', $knowledge->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-knowledge-destroy', $knowledge->id) }}" role="button">Delete</a>
                                            @if($knowledge->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-knowledge-approve', $knowledge->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-knowledge-disapprove', $knowledge->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $knowledges->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>