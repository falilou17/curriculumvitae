<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Users</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-user-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Login</th>
                                <th>Admin</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($users))
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ mb_strimwidth($user->login, 0, 30, "...") }}</td>
                                        <td> @if($user->isAdmin == 1)
                                                <i class="fa fa-check"></i>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-user-edit', $user->id) }}" role="button">Edit</a>
                                            @if(!$user->isActive)
                                                <a class="btn btn-primary" href="{{ route('admin-user-activate', $user->id) }}" role="button">Activate</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-user-deactivate', $user->id) }}" role="button">Deactivate</a>
                                            @endif
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-user-destroy', $user->id) }}" role="button">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $users->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>