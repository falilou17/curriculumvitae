<!-- Form for Reference -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Testimonial </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-reference-update', $reference->id) }}" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Enterprise</label>
                <div class="col-sm-10"><input name="enterprise" required type="text" class="form-control" value="{{ $reference->enterprise }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Full Name</label>
                <div class="col-sm-10"><input name="fullName" required type="text" class="form-control" value="{{ $reference->fullName }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Profession</label>
                <div class="col-sm-10"><input name="profession" required type="text" class="form-control" value="{{ $reference->profession }}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" required rows="2">{{ $reference->description }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-6">
                    <img class="col-sm-8" id="previewRP" src="{{ asset('img/reference/'.$reference->picture) }}" alt="PP" style="max-height:300px;" class="img-rounded">
                    <input class="btn btn-white col-sm-8" name="picture" id="inputRP" type="file" accept="image/*">
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-lg btn-danger" onclick="resetPreview()" type="reset">Cancel</button>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>