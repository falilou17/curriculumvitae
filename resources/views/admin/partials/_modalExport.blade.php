<!-- Modal -->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('export-excel-empty') }}" id="ExportForm">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="section-col">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="profile"> Profile
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="portfolio"> Portfolios
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="competence"> Services
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="strength"> Strengths
                                </label>
                            </div>
                        </div>
                        <div class="section-col">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="school"> Schools
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="experience"> Experiences
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="reference"> Testimonials
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="skill"> Skills
                                </label>
                            </div>
                        </div>
                        <div class="section-col">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="knowledge"> Knowledges
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="award"> Awards
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="achievement"> Stats
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="social"> Socials
                                </label>
                            </div>
                        </div>
                        <div class="section-col">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="design"> Designs
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="proverb"> Proverbs
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary col-sm-4 pull-right" id="GetDocument">Get Document</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>