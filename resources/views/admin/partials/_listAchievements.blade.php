<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Stats (Only the first four will be displayed)</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-achievement-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title </th>
                                <th>Icon </th>
                                <th>Score</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($achievements))
                                @foreach($achievements as $achievement)
                                    <tr>
                                        <td>{{ mb_strimwidth($achievement->title, 0, 60, "...") }}</td>
                                        @if(!empty($achievement->picture))
                                            <td><img src="{{ asset('img/icons/achievements/'.$achievement->picture) }}" style="border-radius: 50px ;width:30px;" alt="icon"></td>
                                        @else
                                            <td><i class="fa {{ $achievement->icon }}"></i></td>
                                        @endif
                                        <td>{{ $achievement->score }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-achievement-edit', $achievement->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-achievement-destroy', $achievement->id) }}" role="button">Delete</a>
                                            @if($achievement->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-achievement-approve', $achievement->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-achievement-disapprove', $achievement->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $achievements->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>