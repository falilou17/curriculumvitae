<!-- Form for Design -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Design </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-design-store') }}" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group colPicker colPickerForBackground">
                <label class="col-sm-5 control-label">Background 1</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" id="colPickerForBackground" required name="background" value="#0d0c0d" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group colPicker colPickerForBackgroundDiv">
                <label class="col-sm-5 control-label">Background 2</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" id="colPickerForBackgroundDiv" required name="backgroundDiv" value="#171717" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group colPicker colPickerForBackgroundFa">
                <label class="col-sm-5 control-label">Background 3</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" required id="colPickerForFaBackground" name="faBackground" value="#2e2e2e" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group colPicker colPickerForTitle">
                <label class="col-sm-5 control-label">Global Title</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" required id="colPickerForTitle" name="title" value="#ffffff" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group colPicker colPickerForDescription">
                <label class="col-sm-5 control-label">Description</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" required id="colPickerForDescription" name="description" value="#a1a1a1" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group colPicker colPickerForElement">
                <label class="col-sm-5 control-label">Element</label>
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" required id="colPickerForElement" name="element" value="#c80a48" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
            </div>
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-designs') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>