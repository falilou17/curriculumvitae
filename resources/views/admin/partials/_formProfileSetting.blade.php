<!-- Form for Profile Me -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Setting</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        @if(!empty($myProfile))
            <form method="post" class="form-horizontal" action="{{ route('admin-profile-setting-save') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Template</label>
                    <div class="col-sm-10">
                        <select name="template_id" id="template_id" class="form-control">
                            @foreach($templates as $template)
                                <option {{ !empty($myProfile->template_id) ? ($myProfile->template_id == $template->id ? 'selected' : '') :  '' }} value="{{ $template->id }}"> {{ $template->title }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Site Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="siteTitle" class="form-control" value="{{ !empty($myProfile->siteTitle) ? $myProfile->siteTitle : old('siteTitle') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Site Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="siteDescription" rows="6">{{ !empty($myProfile->siteDescription) ? $myProfile->siteDescription : old('siteDescription') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Site Keywords</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="siteKeywords" rows="6">{{ !empty($myProfile->siteKeywords) ? $myProfile->siteKeywords : old('siteKeywords') }}</textarea>
                    </div>
                </div>
                @include('errors/error')

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-lg btn-danger" type="reset">Cancel</button>
                        <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>