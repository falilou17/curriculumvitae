<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Portfolios</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-portfolio-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Technologies </th>
                                <th>Client</th>
                                <th>Date of publication</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($portfolios))
                                @foreach($portfolios as $portfolio)
                                    <tr>
                                        <td>{{ mb_strimwidth($portfolio->name, 0, 15, "...") }}</td>
                                        <td>{{ mb_strimwidth($portfolio->technologies, 0, 30, "...") }}</td>
                                        <td>{{ mb_strimwidth($portfolio->client, 0, 30, "...") }}</td>
                                        <td>{{ $portfolio->datePublish }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-portfolio-edit', $portfolio->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-portfolio-destroy', $portfolio->id) }}" role="button">Delete</a>
                                            @if($portfolio->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-portfolio-approve', $portfolio->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-portfolio-disapprove', $portfolio->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $portfolios->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>