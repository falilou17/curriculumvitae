<!-- Form for Competence -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Service </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form method="post" action="{{ route('admin-competence-update', $competence->id) }}" class="form-horizontal"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10"><input name="name" required type="text" class="form-control"
                                              value="{{ $competence->name }}"></div>
            </div>
            @if(empty($award->picture))
                <div class="form-group">
                    <label class="col-sm-2 control-label">Icon</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input data-placement="bottomRight" name="icon" readonly required
                                   class="form-control icp icp-auto" value="{{ $competence->icon }}" type="text"/>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" required
                              rows="4">{{ $competence->description }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Picture</label>
                <div class="col-sm-10">
                    <img class="col-sm-offset-2 col-sm-8" style="width: 70px" id="previewIcon"
                         src="{{ !empty($competence->picture) ? asset('img/icons/competences/'.$competence->picture) : asset('img/icons/picture-icon.png') }}"
                         alt="PP" class="img-rounded">
                    <input class="btn btn-white" name="picture" id="inputIcon" type="file" accept="image/*">
                </div>
            </div>
            @if(!empty($competence->picture))
                <div class="form-group">
                    <label class="col-sm-2 control-label">Supprimer la photo<input type="checkbox" name="delete_picture"></label>
                </div>
            @endif
            @include('errors/error')

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <a class="btn btn-lg btn-danger" role="button" href="{{ route('admin-competences') }}">Cancel</a>
                    <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>