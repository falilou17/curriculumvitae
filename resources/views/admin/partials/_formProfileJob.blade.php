<!-- Form for Profile Job -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Job </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        @if(!empty($myProfile))
            <form method="post" class="form-horizontal" action="{{ route('admin-profile-myjob-save') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Job</label>
                    <div class="col-sm-10"><input type="text" name="job" class="form-control" value="{{ !empty($myProfile->job) ? $myProfile->job : old('job') }}"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        @if(!empty($myProfile->cv))
                            <img class="col-sm-offset-2 col-sm-4" id="previewCV" src="{{ asset('img/cv/'.$myProfile->cv) }}" alt="PP" style="max-height:300px;" class="img-rounded">
                        @else
                            <img class="col-sm-offset-2 col-sm-4" id="previewCV" src="{{ asset('img/cv/cv.jpg') }}" alt="PP" style="max-height:300px;" class="img-rounded">
                        @endif
                    </div>
                    <label class="col-sm-2 control-label">Cv</label>
                    <div class="col-sm-10">
                        <input class="btn btn-white" id="inputCV" name="cv" accept="image/*" type="file">
                        <p class="help-block">Copy of your CV</p>
                    </div>
                </div>
                @include('errors/error')

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-lg btn-danger" onclick="resetPreview()" type="reset">Cancel</button>
                        <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                      @if(!empty($myProfile->cv))
                            <a class="btn btn-lg btn-danger col-sm-offset-1" href="{{ route('admin-profile-myjob-deletecv') }}" role="button">Delete</a>
                      @endif
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>