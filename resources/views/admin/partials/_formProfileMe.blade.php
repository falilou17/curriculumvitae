<!-- Form for Profile Me -->
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Me</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        @if(!empty($myProfile))
            <form method="post" class="form-horizontal" action="{{ route('admin-profile-me-save') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10"><input type="text" name="firstName" class="form-control" value="{{ !empty($myProfile->firstName) ? $myProfile->firstName : old('firstName') }}"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10"><input type="text" name="lastName" class="form-control" value="{{ !empty($myProfile->lastName) ? $myProfile->lastName : old('lastName') }}"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Cover Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control tiny-input" name="coverDesc" rows="2">{{ !empty($myProfile->coverDesc) ? $myProfile->coverDesc : old('coverDesc') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Brief Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control tiny-input" name="descBrief" maxlength="200" rows="2">{{ !empty($myProfile->descBrief) ? $myProfile->descBrief : old('descBrief') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Long Description</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control tiny-input" name="descLong" rows="6">{{ !empty($myProfile->descLong) ? $myProfile->descLong : old('descLong') }}</textarea>
                    </div>
                </div>
                @include('errors/error')

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-lg btn-danger" type="reset">Cancel</button>
                        <button class="btn btn-lg btn-primary col-sm-6 pull-right" type="submit">Save changes</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>