<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>List Awards</h5>
            </div>
            <div class="ibox-content">
                @include('errors/error')
                <div class="table-responsive">
                    <a class="btn btn-primary center-block" href="{{ route('admin-award-create') }}" role="button">New</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title </th>
                                <th>Icon </th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($awards))
                                @foreach($awards as $award)
                                    <tr>
                                        <td>{{ mb_strimwidth($award->title, 0, 30, "...") }}</td>
                                        @if(!empty($award->picture))
                                            <td><img src="{{ asset('img/icons/awards/'.$award->picture) }}" style="border-radius: 50px ;width:30px;" alt="icon"></td>
                                        @else
                                            <td><i class="fa {{ $award->icon }}"></i></td>
                                        @endif
                                        <td>{{ mb_strimwidth($award->description, 0, 60, "...") }}</td>
                                        <td>
                                            <a class="btn btn-warning" href="{{ route('admin-award-edit', $award->id) }}" role="button">Edit</a>
                                            <a class="btn btn-danger pull-right deleteBtn" href="{{ route('admin-award-destroy', $award->id) }}" role="button">Delete</a>
                                            @if($award->status == 0)
                                                <a class="btn btn-primary" href="{{ route('admin-award-approve', $award->id) }}" role="button">Active</a>
                                            @else
                                                <a class="btn btn-danger" href="{{ route('admin-award-disapprove', $award->id) }}" role="button">Desactive</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {!! $awards->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>