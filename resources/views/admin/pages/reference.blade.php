@extends('admin/master')

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listReferences')
    @elseif(!empty($reference))
        @include('admin.partials._formReferenceUpdate')
    @else
        @include('admin.partials._formReferenceCreate')
    @endif
@endsection

@section('script')
    <script type="text/javascript">
        function resetPreview(){
            @if(!empty($reference->picture))
                $('#previewRP').attr('src', "{{ asset('img/reference/'.$reference->picture) }}");
            @else
                $('#previewRP').attr('src', "{{ asset('img/reference/reference.png') }}");
            @endif
        }
        $(document).ready(function() {
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            @if(Session::has('state'))
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif

                $("#inputRP").change(function () {
                readURL(this, '#previewRP');
            });
        });
    </script>
@endsection