@extends('admin/master')

@section('css')
    @if(Auth::user()->isAdmin)
        <link href="{{ asset('adminStuff/css/home2.css') }}" rel="stylesheet">
    @else
        <link href="{{ asset('adminStuff/css/home.css') }}" rel="stylesheet">
    @endif
@endsection

@section('content')
    <div class="row text-center">
        @if(Auth::user()->isAdmin)
            <div class="upper-section">
                <div class="row">
                    <a href="{{ route('admin-users') }}">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="widget style1 lazur-bg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9">
                                        <h2 class="font-bold">Manage user</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endif
        <div class="center-section">
            <div class="row">
                <div class="col-sm-3">
                    <a href="#" class="btn btn-warning btn-lg" role="button" id="getData" data-toggle="modal"
                       data-target="#myModalExport"> Export Excel </a>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="btn btn-primary btn-lg" role="button" id="getEmptyTemplate" data-toggle="modal"
                       data-target="#myModalExport"> Get Empty Template </a>
                </div>
                <div class="col-sm-5">
                    <form method="post" action="{{ route('import-excel') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="importExcel">Load Excel File</label>
                            <input required type="file" name="importExcel" id="importExcel"
                                   accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <button type="submit" class="btn btn-success btn-lg"> Import Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('admin/partials/_modalExport')
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#getEmptyTemplate').click(function () {
                $('#ExportForm').attr('action', '{{ route('export-excel-empty') }}');
            });
            $('#getData').click(function () {
                $('#ExportForm').attr('action', '{{ route('export-excel') }}');
            });
            @if(Session::has('state'))
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif
            @if($errors->has())
                setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('{{ $errors->all() }}', 'State');
            }, 0);
            @endif
        });
    </script>
@endsection