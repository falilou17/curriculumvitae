@inject('Helper', 'App\Services\BladeData')
@extends('admin/master')

@section('css')
    <!-- Color Picker -->
    <link href="{{ asset('adminStuff/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }
        .colorpicker-2x .colorpicker-hue{
            width: 30px;
            height: 200px;
        }
        .colorpicker-2x .colorpicker-alpha {
            width: 0px;
            height: 0px;
        }
        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div{
            height: 30px;
        }
    </style>
@endsection

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listDesigns')
    @elseif(!empty($design))
        @include('admin.partials._formDesignUpdate')
        <div class="row">
            <div class="col-sm-12">
                <iframe id="PreviewPage" src="{{ url() }}" class="col-sm-12" height="700" >
                    <p>Votre navigateur ne supporte pas l'élément iframe</p>
                </iframe>
            </div>
        </div>
    @else
        @include('admin.partials._formDesignCreate')
        <div class="row">
            <div class="col-sm-12">
                <iframe id="PreviewPage" src="{{ route('index') }}" class="col-sm-12" height="700" >
                    <p>Votre navigateur ne supporte pas l'élément de previsualisation</p>
                </iframe>
            </div>
        </div>
    @endif
@endsection

@section('script')
        <!-- Color picker -->
    <script src="{{ asset('adminStuff/js/bootstrap-colorpicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(Session::has('state'))
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
                $('.colPicker').colorpicker({
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            });
            $('#PreviewPage').load(function(){
                InitIframe();
            });
            $('.colPickerForBackground').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find(".background1").css("background-color" , event.color.toHex());
                $('#PreviewPage').contents().find(".dark-bg").css('background-color', (LightenDarkenColor($('#colPickerForBackground').val().toString(), -8)).toString());
            });
            $('.colPickerForBackgroundDiv').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find(".background2").css("background-color" , event.color.toHex());
            });
            $('.colPickerForBackgroundFa').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find(".background3").css("background-color" , event.color.toHex());
            });
            $('.colPickerForTitle').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find(".general-color, .general-color > li > a").css("color" , event.color.toHex());
            });
            $('.colPickerForDescription').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find(".text-color").css("color" , event.color.toHex());
            });
            $('.colPickerForElement').colorpicker().on('changeColor.colorpicker', function(event){
                $('#PreviewPage').contents().find("#jpreBar, .btn-custom, span.divider, .resume-year,.column-chart > .chart > .item > .bar > .item-progress, #references .flexslider.references > .slides > .item .profile:before, .circle-chart .item > .percent, .bar-chart > .item > .bar > .percent, .accolades > .item > i, .milestones .item > .circle").css("background-color" , event.color.toHex());
                $('#PreviewPage').contents().find(".btn-custom").css('box-shadow', "3px 4px 0"+(LightenDarkenColor($('#colPickerForElement').val().toString(), 50)).toString());
            });
        });
        function LightenDarkenColor(col, amt) {
            var usePound = false;
            if (col[0] == "#") {
                col = col.slice(1);
                usePound = true;
            }
            var num = parseInt(col,16);
            var r = (num >> 16) + amt;
            if (r > 255) r = 255;
            else if  (r < 0) r = 0;
            var b = ((num >> 8) & 0x00FF) + amt;
            if (b > 255) b = 255;
            else if  (b < 0) b = 0;
            var g = (num & 0x0000FF) + amt;
            if (g > 255) g = 255;
            else if (g < 0) g = 0;
            return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
        }
        function InitIframe(){
            $('#PreviewPage').contents().find(".background1").css("background-color" , $('#colPickerForBackground').val());
            $('#PreviewPage').contents().find(".background2").css("background-color" , $('#colPickerForBackgroundDiv').val());
            $('#PreviewPage').contents().find(".background3").css("background-color" , $('#colPickerForBackgroundFa').val());
            $('#PreviewPage').contents().find(".general-color, .general-color > li > a").css("color" , $('#colPickerForTitle').val());
            $('#PreviewPage').contents().find(".text-color").css("color" , $('#colPickerForDescription').val());
            $('#PreviewPage').contents().find("#jpreBar, .btn-custom, span.divider, .resume-year,.column-chart > .chart > .item > .bar > .item-progress, #references .flexslider.references > .slides > .item .profile:before, .circle-chart .item > .percent, .bar-chart > .item > .bar > .percent, .accolades > .item > i, .milestones .item > .circle").css("background-color" , $('#colPickerForElement').val());
            $('#PreviewPage').contents().find(".btn-custom").css('box-shadow', "3px 4px 0"+(LightenDarkenColor($('#colPickerForElement').val().toString(), 50)).toString());
            $('#PreviewPage').contents().find(".dark-bg").css('background-color', (LightenDarkenColor($('#colPickerForBackground').val().toString(), -8)).toString());
        };

    </script>
@endsection