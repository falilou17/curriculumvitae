@extends('admin/master')

@section('css')
        <!-- Icon Picker -->
    <link href="{{ asset('adminStuff/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listAchievements')
    @elseif(!empty($achievement))
        @include('admin.partials._formAchievementUpdate')
    @else
        @include('admin.partials._formAchievementCreate')
    @endif
@endsection

@section('script')
        <!-- Icon Picker -->
    <script src="{{ asset('adminStuff/js/fontawesome-iconpicker.min.js') }}"></script>
    <script>
        $(function() {
            $('.icp-auto').iconpicker();
        });
        function resetPreview(){
            @if(!empty($achievement->picture))
                $('#previewIcon').attr('src', "{{ asset('img/icons/achievements/'.$achievement->picture) }}");
            @else
                $('#previewIcon').attr('src', "{{ asset('img/icons/picture-icon.png') }}");
            @endif
        }
        $(document).ready(function() {
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            @if(Session::has('state'))
                setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
            @endif

            $("#inputIcon").change(function () {
                readURL(this, '#previewIcon');
            });
        });
    </script>
@endsection