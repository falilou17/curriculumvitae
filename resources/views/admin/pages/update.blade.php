<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin Panel</title>

    <link href="{{ asset('adminStuff/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('adminStuff/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">
<div id="wrapper">
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-bold">CV Admin Panel</h2>
                <p>
                    Edit Admin LogIn Curriculum Vitae.
                </p>
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" method="post" action="{{ route('admin-update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="cpassword" placeholder="Confirm Password" required>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Update</button>
                        <button type="reset" class="btn btn-warning block full-width m-b">Cancel</button>
                    </form>
                        <a href="{{ route('admin-home') }}" class="btn btn-danger block full-width m-b">Back</a>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                @include('errors/error')
                Copyright Official
            </div>
            <div class="col-md-6 text-right">
                <small>© 2016</small>
            </div>
        </div>
    </div>
</div>

</body>
</html>

@if(Session::has('state'))
    @section('script')
        <script type="text/javascript">
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
        </script>
    @endsection
@endif