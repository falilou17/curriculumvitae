@extends('admin/master')

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listSocials')
    @elseif(!empty($social))
        @include('admin.partials._formSocialUpdate')
    @else
        @include('admin.partials._formSocialCreate')
    @endif
@endsection

@section('script')
        <!-- Icon Picker -->
    <script src="{{ asset('adminStuff/js/fontawesome-iconpicker.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
        });
        $(function() {
            $('.icp-auto').iconpicker();
        });
        @if(Session::has('state'))
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
        @endif
    </script>
@endsection