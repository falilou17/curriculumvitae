<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin Panel</title>

    <link href="{{ asset('adminStuff/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('adminStuff/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">
<div id="wrapper">
    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Admin Panel</h2>
                <p>
                    Log In to edit all parameters of your website.
                </p>
                @include('errors/error')
                @if(Session::has('state'))
                    <div class="row text-center">
                        <div class="col-sm-offset-2 col-sm-8 alert alert-danger alert-dismissable" role="alert">
                            <p>{{ Session::get('state') }}</p>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" method="post" action="{{ route('admin-login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" name="login" placeholder="Username" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Connect</button>
                    </form>
                        <a href="{{ url() }}" class="btn btn-danger block full-width m-b">Back</a>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12 text-center">
                <small>&copy; {{ date("Y") }}. All rights reserved.</small>
            </div>
        </div>
    </div>
</div>

</body>
</html>

@if(Session::has('state'))
    @section('script')
        <script type="text/javascript">
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
        </script>
    @endsection
@endif