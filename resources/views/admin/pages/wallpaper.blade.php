@extends('admin/master')

@section('content')
    @if(!empty($wallpaper))
        @include('admin/partials/_formWallpaper')
    @endif
@endsection
@section('script')
    <script type="text/javascript">
        function resetPreview(){
            $('#previewStrength').attr('src', "{{ asset('img/wallpapers/strengths/'.$wallpaper->strength) }}");
            $('#previewAchievement').attr('src', "{{ asset('img/wallpapers/achievements/'.$wallpaper->achievement) }}");
            $('#previewReference').attr('src', "{{ asset('img/wallpapers/references/'.$wallpaper->reference) }}");
            $('#previewPortfolio').attr('src', "{{ asset('img/wallpapers/portfolios/'.$wallpaper->portfolio) }}");
            $('#previewContact').attr('src', "{{ asset('img/wallpapers/contacts/'.$wallpaper->contact) }}");
            $('#previewService').attr('src', "{{ asset('img/wallpapers/services/'.$wallpaper->service) }}");
            $('#previewProfile').attr('src', "{{ asset('img/wallpapers/profiles/'.$wallpaper->profile) }}");
        }
        $(document).ready(function() {
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            @if(Session::has('state'))
                setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
            @endif

            $("#inputStrength").change(function () {
                readURL(this, '#previewStrength');
            });

            $("#inputAchievement").change(function () {
                readURL(this, '#previewAchievement');
            });

            $("#inputReference").change(function () {
                readURL(this, '#previewReference');
            });

            $("#inputPortfolio").change(function () {
                readURL(this, '#previewPortfolio');
            });

            $("#inputContact").change(function () {
                readURL(this, '#previewContact');
            });

            $("#inputService").change(function () {
                readURL(this, '#previewService');
            });

            $("#inputProfile").change(function () {
                readURL(this, '#previewProfile');
            });
        });
    </script>
@endsection