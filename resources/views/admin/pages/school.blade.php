@extends('admin/master')

@section('css')
    <link href="{{ asset('adminStuff/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listSchools')
    @elseif(!empty($school))
        @include('admin.partials._formSchoolUpdate')
    @else
        @include('admin.partials._formSchoolCreate')
    @endif
@endsection

@section('script')
    <script src="{{ asset('adminStuff/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
            $('.input-daterange').datepicker({
                format: "yyyy",
                startView: 2,
                minViewMode: 2,
                orientation: "bottom auto",
                autoclose: true
            });
            @if(Session::has('state'))
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif
        });
    </script>
@endsection