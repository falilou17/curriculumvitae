@extends('admin/master-mail')

@section('mail-content')
    @if(!empty($home))
        @include('admin/partials/_listMails')
    @elseif(!empty($mail))
        @include('admin.partials._mailShow')
    @else
        @include('admin.partials._formMailCreate')
    @endif
@endsection

@section('script')
    <script type="text/javascript">
        @if(Session::has('state'))
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
        @endif
    </script>
@endsection