@extends('admin/master')

@section('css')
    <link href="{{ asset('adminStuff/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }
        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }
        .list-group
        {
            list-style-type: none;
        }
        ul.ImgDraggable > li.col-sm-2
        {
            padding-top: 20px;
            border: 1px solid rgba(0, 0, 0, 0.32);
            background-color: rgba(21, 21, 21, 0.2);
        }
        ul.ImgDraggable > li.placeholder {
            position: relative;
        }
        ul.ImgDraggable > li.placeholder:before{
            position: absolute;
        }
        .ImgDraggable input[type='file'] {
            display: none;
        }
    </style>
@endsection

@section('content')
    @if(!empty($home))
        @include('admin/partials/_listPortfolios')
    @elseif(!empty($portfolio))
        @include('admin.partials._formPortfolioUpdate')
    @else
        @include('admin.partials._formPortfolioCreate')
    @endif
@endsection

@section('script')
    <script src="{{ asset('adminStuff/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('adminStuff/js/jquery-sortable.min.js') }}"></script>
    <script>
        var cpt = 1;
        function uniqId() {
            return Math.round(new Date().getTime() + (Math.random() * 100));
        }
        function addImg(){
            if (cpt < 5){
                var id = uniqId();
                $("#lstImg").append('<tr id="'+id+'"><td><div class="form-group"><div class="col-sm-10"><input class="btn btn-white addImgButton" name="media[]" accept="image/*" type="file"></div></div></td><td><a class="btn btn-danger delImgButton" onclick="delImg('+id+')" role="button">Delete</a></td></tr>');
                cpt++;
            }
            else
                $("#limitImg").html("Limit reach !!!");
        }
        function delImg(id){
            $("#"+id).remove();
            $("#limitImg").html("");
            cpt--;
        }
        $(document).ready(function(){
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
            $('.datepicker').datepicker({
                format: "mm/dd/yyyy",
                startView: 2,
                todayBtn: "linked"
            });
            $(function  () {
                $("ul.ImgDraggable").sortable();
            });
            @if(Session::has('state'))
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif


            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            @for($i=0; $i<5; $i++)
                $("#AddBtnPortf{{ $i }}").click(function(){
                    $("#addImgButton{{ $i }}").trigger('click');
                });
                $("#addImgButton{{ $i }}").change(function () {
                    readURL(this, '#previewPortf{{ $i }}');
                });
            @endfor
        });
    </script>
@endsection