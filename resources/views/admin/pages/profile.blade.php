@extends('admin/master')

@section('content')
    @if(!empty($page))
        @if($page == "Me")
            @include('admin/partials/_formProfileMe')
        @elseif($page == "Contact")
            @include('admin/partials/_formProfileContact')
        @elseif($page == "Job")
            @include('admin/partials/_formProfileJob')
        @elseif($page == "Picture")
            @include('admin/partials/_formProfilePicture')
        @elseif($page == "Setting")
            @include('admin/partials/_formProfileSetting')
        @endif
    @endif
@endsection

@section('script')
    <script type="text/javascript">
        function resetPreview(){
            @if(!empty($myProfile))
                @if(!empty($myProfile->cv))
                    $('#previewCV').attr('src', "{{ asset('img/cv/'.$myProfile->cv) }}");
                @else
                    $('#previewCV').attr('src', "{{ asset('img/cv/cv.jpg') }}");
                @endif
            @else
                $('#previewPP').attr('src', "{{ asset('img/'.$myProfile->profilePicture) }}");
                $('#previewCP').attr('src', "{{ asset('img/'.$myProfile->coverPicture) }}");
            @endif
        }
        $(document).ready(function() {
            $('.deleteBtn').click(function(e) {
                link = $(this).attr('href');
                e.preventDefault();
                swal({
                    title: "Are you sure ?",
                    text: "You won't be able to revert this !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ed5565',
                    cancelButtonColor: '#1ab394',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false
                }).then(function(isConfirm) {
                    if (isConfirm === true) {
                        window.location.href = link;
                    }
                });
            });
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            @if(Session::has('state'))
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
                }, 0);
            @endif

            $("#inputCV").change(function () {
                readURL(this, '#previewCV');
            });

            $("#inputPP").change(function () {
                readURL(this, '#previewPP');
            });

            $("#inputCP").change(function () {
                readURL(this, '#previewCP');
            });
        });
    </script>
@endsection