@extends('admin/master')

@section('content')
    @if(!empty($proverb))
        @include('admin/partials/_formProverb')
    @endif
@endsection
@if(Session::has('state'))
    @section('script')
        <script type="text/javascript">
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.{{ Session::get('type') }}('{{ Session::get('state') }}', 'State');
            }, 0);
        </script>
    @endsection
@endif