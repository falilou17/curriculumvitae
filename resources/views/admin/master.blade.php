<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin Panel</title>

    <link href="{{ asset('adminStuff/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Toastr style -->
    <link href="{{ asset('adminStuff/css/toastr.min.css') }}" rel="stylesheet">

    <!-- Sweet Alert style -->
    <link href="{{ asset('adminStuff/css/sweetalert2.min.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('adminStuff/css/jquery.gritter.css') }}" rel="stylesheet">

    <link href="{{ asset('adminStuff/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('adminStuff/css/style.css') }}" rel="stylesheet">

    <!-- Icon Picker -->
    <link href="{{ asset('adminStuff/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet">

    @yield('css')
</head>

<body>
<div id="wrapper">
    @include('admin/partials/_sideBar')
    <div id="page-wrapper" class="gray-bg dashbard-1">
        @include('admin/partials/_navbar')
        @yield('content')
    </div>
    @yield('modal')
</div>

<!-- Mainly scripts -->
<script src="{{ asset('adminStuff/js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('adminStuff/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminStuff/js/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('adminStuff/js/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('adminStuff/js/inspinia.js') }}"></script>
<script src="{{ asset('adminStuff/js/pace.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('adminStuff/js/toastr.min.js') }}"></script>

<!-- Sweet Alert -->
<script src="{{ asset('adminStuff/js/sweetalert2.min.js') }}"></script>

<script src="{{ asset('adminStuff/js/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function (){
            tinymce.init({
                selector: '.tiny-input',
                height : 480,
                plugins: 'lists advlist anchor autolink autosave code codesample colorpicker contextmenu directionality emoticons fullscreen help hr image imagetools insertdatetime link lists media preview print save searchreplace spellchecker table template textcolor toc visualblocks visualchars wordcount',
                toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat | charmap  | preview save restoredraft print | insertfile image link codesample | fullscreen',
                autosave_ask_before_unload: true,
                autosave_interval: '30s',
                autosave_prefix: '{path}{query}-{id}-',
                autosave_restore_when_empty: false,
                autosave_retention: '2m',
            });
        })
    </script>

@yield('script')
</body>
</html>
