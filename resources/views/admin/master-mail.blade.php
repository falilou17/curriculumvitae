@extends('admin/master')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="{{ route('admin-mail-create') }}">Compose Mail</a>
                            <div class="space-25"></div>
                            <h5>Folders</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="{{ route('admin-mails','Inbox') }}"> <i class="fa fa-inbox "></i> Inbox </a></li>
                                <li><a href="{{ route('admin-mails','Send') }}"> <i class="fa fa-envelope-o"></i> Send Mail</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('mail-content')
        </div>
    </div>
@endsection